﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services.Protocols;
using Scandic.MembershipCards.NameServiceProxy;
using Scandic.MembershipCards.Entity;

namespace Scandic.MembershipCards.Utils
{
    public class ServiceHelper
    {
        public MembershipDetails GetMembershipDetails(string membershipNo, string lastName, string primaryLanguageID)
        {
            MembershipDetails membDetails = null;
            string membershipType = WebUtil.GetTextFromXML("root/configEntry/SCANDIC_LOYALTY_MEMBERSHIPTYPE");
            NameLookupRequest nameLookupReq = GetNameLookUpRequest(membershipNo, lastName, membershipType, false);
            NameLookupResponse nameLookupResponse = null;
            FetchGuestCardListRequest guestCardListRequest = null;
            FetchGuestCardListResponse guestCardListResponse = null;

            try
            {
                NameService nameSvc = GetNameService(primaryLanguageID);
                nameLookupResponse = nameSvc.NameLookup(nameLookupReq);

                string nameID = string.Empty;
                if (nameLookupResponse != null && nameLookupResponse.Result != null && nameLookupResponse.Result.resultStatusFlag != ResultStatusFlag.FAIL
                    && nameLookupResponse.Profiles.Length > 0)
                    nameID = nameLookupResponse.Profiles[0].ProfileIDs[0].Value;
                else
                {
                    nameLookupReq = GetNameLookUpRequest(membershipNo, lastName, membershipType, true);
                    nameLookupResponse = nameSvc.NameLookup(nameLookupReq);
                    if (nameLookupResponse != null && nameLookupResponse.Result != null && nameLookupResponse.Result.resultStatusFlag != ResultStatusFlag.FAIL
                        && nameLookupResponse.Profiles.Length > 0)
                    nameID = nameLookupResponse.Profiles[0].ProfileIDs[0].Value;
                }
                if (!string.IsNullOrEmpty(nameID))
                {
                    guestCardListRequest = GetFetchGuestcardListRequest(nameID);
                    guestCardListResponse = nameSvc.FetchGuestCardList(guestCardListRequest);
                }
                if (guestCardListResponse != null && guestCardListResponse.Result != null && guestCardListResponse.Result.resultStatusFlag != ResultStatusFlag.FAIL)
                {
                    NameMembership[] guestCardList = guestCardListResponse.GuestCardList;
                    for (int count = 0; count < guestCardList.Length; count++)
                    {
                        NameMembership nameMembership = guestCardList[count];
                        if (string.Equals(nameMembership.membershipType, MembershipConstants.SCANDIC_LOYALTY_MEMBERSHIPTYPE, StringComparison.InvariantCultureIgnoreCase))
                        {
                            membDetails = new MembershipDetails();
                            membDetails.Name = nameMembership.memberName;
                            membDetails.MembershipLevel = nameMembership.membershipLevel;
                            membDetails.MembershipNumber = nameMembership.membershipNumber;
                            membDetails.ExpiryDate = nameMembership.expirationDate;
                            break;
                        }
                    }
                }
            }
            catch (SoapException soapException)
            {
                throw soapException;
            }
            finally
            {
                guestCardListRequest = null;
            }
            return membDetails;
        }

        /// <summary>
        /// Create FetchGuestCardListRequest
        /// </summary>
        /// <param name="nameId">NameID</param>
        /// <returns>FetchGuestCardListRequest</returns>
        public static FetchGuestCardListRequest GetFetchGuestcardListRequest(string nameId)
        {
            FetchGuestCardListRequest reqFetchGuestcardList = new FetchGuestCardListRequest();
            UniqueID uniqueId = new UniqueID();

            uniqueId.Value = nameId;
            uniqueId.type = UniqueIDType.INTERNAL;
            reqFetchGuestcardList.NameID = uniqueId;

            return reqFetchGuestcardList;
        }

        /// <summary>
        /// Create FetchGuestCardListRequest
        /// </summary>
        /// <param name="nameId">NameID</param>
        /// <returns>FetchGuestCardListRequest</returns>
        public static NameLookupRequest GetNameLookUpRequest(string membershipNo, string lastName, string membershipType, bool IsAlternateName)
        {
            NameLookupRequest nameRequest = new NameLookupRequest();
            NameLookupCriteriaMembership nameLkpCriteria = new NameLookupCriteriaMembership();
            if (IsAlternateName)
            {
                nameLkpCriteria.NativeName = new NativeName();
                nameLkpCriteria.NativeName.lastName = lastName;
            }
            else
            {
                nameLkpCriteria.LastName = lastName;
            }
            nameLkpCriteria.MembershipNumber = membershipNo;
            nameLkpCriteria.MembershipType = membershipType;
            nameRequest.NameLookupCriteria = new NameLookupInput();
            nameRequest.NameLookupCriteria.Item = nameLkpCriteria;
            return nameRequest;
        }


        /// <summary>
        /// Get the Name Service Proxy
        /// </summary>
        /// <param name="primaryLangaugeID">Primary LanguageID</param>
        /// <returns>NameService</returns>
        public static NameService GetNameService(params string[] primaryLangaugeID)
        {
            NameService service = new NameService();
            service.OGHeaderValue = GetNameHeader();
            if (primaryLangaugeID != null && primaryLangaugeID.Length > 0)
            {
                service.OGHeaderValue.primaryLangID = primaryLangaugeID[0];
            }

            service.Url = MembershipConstants.OWS_NAME_SERVICE;

            return service;
        }

        /// <summary>
        /// GetNameHeader
        /// </summary>
        /// <returns>NameHeader</returns>
        private static OGHeader GetNameHeader()
        {
            OGHeader soapHeader = new OGHeader();

            soapHeader.transactionID = GetTransactionID();
            soapHeader.timeStamp = DateTime.Now;
            soapHeader.timeStampSpecified = true;

            soapHeader.Origin = new EndPoint();
            soapHeader.Origin.entityID = MembershipConstants.OWS_ORIGIN_ENTITY_ID;
            soapHeader.Origin.systemType = MembershipConstants.OWS_ORIGIN_SYSTEM_TYPE;

            soapHeader.Destination = new EndPoint();
            soapHeader.Destination.entityID = MembershipConstants.OWS_DESTINATION_ENTITY_ID;
            soapHeader.Destination.systemType = MembershipConstants.OWS_DESTINATION_SYSTEM_TYPE;

            return soapHeader;
        }

        private static string GetTransactionID()
        {
            byte[] tID = new Byte[2];
            Random rnd = new Random(~unchecked((int)DateTime.Now.Ticks));
            rnd.NextBytes(tID);
            return tID[0].ToString() + tID[1].ToString();
        }
    }
}
