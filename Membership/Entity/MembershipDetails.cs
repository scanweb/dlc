﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scandic.MembershipCards.Entity
{
    public class MembershipDetails
    {
        /// <summary>
        /// Gets or sets Member Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets Membership Number
        /// </summary>
        public string MembershipNumber { get; set; }

        /// <summary>
        /// Gets or sets Membership Level
        /// </summary>
        public string MembershipLevel { get; set; }

        /// <summary>
        /// Gets or sets Membership Expiry date
        /// </summary>
        public DateTime ExpiryDate { get; set; }

        /// <summary>
        /// Gets or sets Name Id from users profile
        /// </summary>
        public string NameId { get; set; }
    }
    
}
