﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GenerateKey.aspx.cs" Inherits="Scandic.MembershipCards.GenerateKey" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Scandic</title>
    <link rel="stylesheet" type="text/css" href="/MembershipCard/AddToHome/addtohomescreen.css">
    <link rel="stylesheet" type="text/css" href="/MembershipCard/Public/Styles/MembershipDetails.css">
    <link rel="stylesheet" type="text/css" href="/MembershipCard/Public/Styles/GenerateKey.css">
</head>
<body>
    <div id="container">
        <header class="topHeader">
		<h2 class="logo"></h2>
		<h2 class="subTitle"></h2>		
		<div class="line"></div>
		<h4 class="personal-greeting" runat="server" ID="welcomeMember">Please enter membership no and last name to generate the key.</h4>
		<p id="headerText" runat="server">
		</p>
	</header>
        <form id="generateKeyForm" runat="server" action="" method="post">
        <section class='main'>	
		
        <p class='memno'>Membership Number
            <input type="text" maxlength="20" tabindex="10" id="txtLoyaltyUsername" name="txtLoyaltyUsername" />
        </p>
        <p class='memno'>Last Name
            <input type="text" tabindex="15" maxlength="40" id="txtLoyaltyLastname" name="txtLoyaltyLastname" />
        </p>
        <asp:Button ID="btnGenerateKey" tabindex="20" OnClick="btnGenerateKey_Click" Text="Submit" class='memno' BackColor="#87B33A" runat="server" />
		<input id="btnReset" class='memno' type="button" tabindex="25" onclick="resetFields()" value="Reset" />
		<div id="resultsDiv" class='memno message' runat="server" visible="false">
			<asp:label id="messageLabel" runat="server" />
			<p>
				<textarea id="txtKey" name="txtKey" style="height: 52px; width: 560px" runat="server"></textarea>
			</p>
			<p>
				<input type="button" id="btnSelectKey" name="btnSelectKey" BackColor="#87B33A" onclick="selectKey()" value="Select Generated Key" runat="server" />
			</p>
		</div>
	</section>
        </form>
    </div>
</body>

<script>
    function resetFields() {
        document.getElementById("txtLoyaltyUsername").value = "";
        document.getElementById("txtLoyaltyLastname").value = "";
        document.getElementById('resultsDiv').style.display = "none";

    }

    function selectKey() {
        document.getElementById("txtKey").select();
    }
</script>

</html>