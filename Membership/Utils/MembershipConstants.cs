﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace Scandic.MembershipCards.Utils
{
    public static class MembershipConstants
    {
        public static string OWS_ORIGIN_ENTITY_ID
        {
            get { return WebUtil.GetTextFromXML("root/configEntry/OWS_ORIGIN_ENTITY_ID"); }
            //get { return "SCW"; }
        }

        public static string OWS_ORIGIN_SYSTEM_TYPE
        {
            get { return WebUtil.GetTextFromXML("root/configEntry/OWS_ORIGIN_SYSTEM_TYPE"); }
            //get { return "WEB"; }
        }

        public static string OWS_DESTINATION_ENTITY_ID
        {
            get { return WebUtil.GetTextFromXML("root/configEntry/OWS_DESTINATION_ENTITY_ID"); }
            //get { return "SH"; }
        }

        public static string OWS_DESTINATION_SYSTEM_TYPE
        {
            get { return WebUtil.GetTextFromXML("root/configEntry/OWS_DESTINATION_SYSTEM_TYPE"); }
            //get { return "ORS"; }
        }

        public static string OWS_NAME_SERVICE
        {
            get { return WebUtil.GetTextFromXML("root/configEntry/OWS_NAME_SERVICE"); }
            //get { return "http://10.31.173.162/OWS_WS_51/Name.asmx"; }
        }

        public static string SCANDIC_LOYALTY_MEMBERSHIPTYPE
        {
            get { return WebUtil.GetTextFromXML("root/configEntry/SCANDIC_LOYALTY_MEMBERSHIPTYPE"); }
            //get { return "GUESTPR"; }
        }

        public const string FIRST_FLOOR = "1STFLOOR";
        public const string SECOND_FLOOR = "2NDFLOOR";
        public const string THIRD_FLOOR = "3RDFLOOR";
        public const string TOP_FLOOR = "TOPFLOOR";
        public const string TOP_FLOOR_BY_INVITE = "BYINVIT";
    }
}
