﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Scandic.CryptorEngine;

namespace Scandic.MembershipCards
{
    public partial class GenerateKey : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {

            }
        }
        protected void btnGenerateKey_Click(object sender, EventArgs args)
        {
            if (Page.IsPostBack && !String.IsNullOrEmpty(Request.Form["txtLoyaltyUsername"])
                && !String.IsNullOrEmpty(Request.Form["txtLoyaltyLastname"]))
            {
                string objJsonString = null;
                var payload = new Dictionary<string, string>
                            {
                                { "MembershipID", Request.Form["txtLoyaltyUsername"].Trim()},
                                { "LastName", Request.Form["txtLoyaltyLastname"].Trim()},
                            };
                objJsonString = ScanwebJsonWebToken.Encode(payload);
                resultsDiv.Visible = true;
                txtKey.Visible = true;
                txtKey.Value = objJsonString;
                btnSelectKey.Visible = true;
                messageLabel.Text = string.Empty;
            }
            else
            {
                resultsDiv.Visible = true;
                txtKey.Visible = false;
                btnSelectKey.Visible = false;
                messageLabel.Text = "Please enter both the membership number and last name.";
            }
        }
    }
}
