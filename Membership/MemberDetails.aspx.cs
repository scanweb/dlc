﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using log4net;
using Scandic.CryptorEngine;
using Scandic.MembershipCards.Entity;
using Scandic.MembershipCards.Utils;


namespace Scandic.MembershipCards
{
    public partial class MemberDetails : System.Web.UI.Page
    {
        protected string language;
        protected void Page_Load(object sender, EventArgs e)
        {
            ILog Log = LogManager.GetLogger("MembershipCardLog");
            string membershipId = string.Empty;
            string lastName = string.Empty;
            JavaScriptSerializer jsSerializer = null;
            MembershipDetails membershipDetails = null;
            object objJsonString = null;
            Dictionary<string, string> data = null;
            bool IsLoggingEnabled = Convert.ToBoolean(WebUtil.GetTextFromXML("root/configEntry/ENABLE_LOGGING"));

            //var payload = new Dictionary<string, string>
            //            {
            //                { "MembershipID", "30812384024529" },
            //                { "LastName", "Närvä-Ilkka" },
            //            };
            //string objJsonStringOld = ScanwebJsonWebToken.Encode(payload);

            if (Request.QueryString.Count > 0 && Request.QueryString["memkey"] != null)
            {
                try
                {
                    string keyToDecrypt = WebUtil.GetTextFromXML("root/configEntry/ENCRYPTION_KEY");
                    string strToDecode = Convert.ToString(Request.QueryString["memkey"]);
                    objJsonString = ScanwebJsonWebToken.Decode(strToDecode);

                    // Old way of decoding successful
                    if (!string.IsNullOrEmpty((string)objJsonString))
                    {
                        jsSerializer = new JavaScriptSerializer();
                        data = jsSerializer.Deserialize<Dictionary<string, string>>(Convert.ToString(objJsonString));

                        if (data != null && data.Count > 0)
                        {
                            if (data.ContainsKey("MembershipID"))
                                membershipId = Convert.ToString(data["MembershipID"]);
                            if (data.ContainsKey("LastName"))
                                lastName = Convert.ToString(data["LastName"]);
                        }
                    }
                    else
                    {
                        objJsonString = ScanwebJsonWebToken.Decrypt(strToDecode, keyToDecrypt);

                        if (!string.IsNullOrEmpty((string)objJsonString))
                        {
                            string[] encryptdata = ((string)objJsonString).Split(',');
                            if (encryptdata != null && encryptdata.Length > 1)
                            {
                                membershipId = encryptdata[0].ToString();
                                lastName = encryptdata[1].ToString();
                            }
                        }
                    }
                    language = WebUtil.GetCurrentLanguage();

                    membershipDetails = new ServiceHelper().GetMembershipDetails(membershipId, lastName, language);
                }
                catch (Exception ex)
                {
                    if (IsLoggingEnabled)
                    {
                        LogHelper.ConfigureFileAppender(WebUtil.GetTextFromXML("root/configEntry/ERROR_LOG_PATH"));
                        Log.Fatal(string.Format("Error occured for the key: {0} {1}", Convert.ToString(Request.QueryString["memkey"]), ex.Message), ex);
                    }
                    Server.Transfer("/MembershipCard/notfound.aspx");
                }

                if (membershipDetails != null)
                {
                    string welcomeGreet = WebUtil.GetTextFromXML(string.Format("{0}{1}", "root/membershipMessages/welcomeGreeting", language.ToUpper()));
                    string welcomeMessage = WebUtil.GetTextFromXML(string.Format("{0}{1}", "root/membershipMessages/welcomeMessage", language.ToUpper()));
                    string expiryDate = WebUtil.GetTextFromXML("root/membershipMessages/ExpDate");
                    string memNo = "{0} {1}";
                    welcomeMember.InnerText = string.Format(welcomeGreet, membershipDetails.Name);
                    membershipNo.InnerText = string.Format(memNo, membershipDetails.MembershipNumber.Substring(0, 6), membershipDetails.MembershipNumber.Substring(6, membershipDetails.MembershipNumber.Length - 6));
                    membershipName.InnerText = membershipDetails.Name;
                    headerText.InnerHtml = welcomeMessage;
                    expDate.Visible = true;
                    expDate.InnerText = string.Format(expiryDate, Convert.ToString(membershipDetails.ExpiryDate.ToString("MM")), Convert.ToString(membershipDetails.ExpiryDate.ToString("yyyy")));

                    switch (membershipDetails.MembershipLevel)
                    {
                        case MembershipConstants.FIRST_FLOOR:
                            expDate.Visible = false;
                            cardCSS.Attributes.Add("class", "img-responsive card first-floor x2");
                            break;
                        case MembershipConstants.SECOND_FLOOR:
                            cardCSS.Attributes.Add("class", "img-responsive card second-floor x2");
                            break;
                        case MembershipConstants.THIRD_FLOOR:
                            cardCSS.Attributes.Add("class", "img-responsive card third-floor x2");
                            break;
                        case MembershipConstants.TOP_FLOOR:
                            cardCSS.Attributes.Add("class", "img-responsive card top-floor x2");
                            break;
                        case MembershipConstants.TOP_FLOOR_BY_INVITE:
                            cardCSS.Attributes.Add("class", "img-responsive card top-invitation-floor x2");
                            break;
                    }
                }
                else
                    Server.Transfer("/MembershipCard/notfound.aspx");
            }
            else
            {
                if (IsLoggingEnabled)
                {
                    LogHelper.ConfigureFileAppender(WebUtil.GetTextFromXML("root/configEntry/ERROR_LOG_PATH"));
                    Log.Fatal("Memkey Query String value is null");
                }
                Server.Transfer("/MembershipCard/notfound.aspx");
            }
        }
    }
}
