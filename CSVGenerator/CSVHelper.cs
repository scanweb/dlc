﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using Scandic.CryptorEngine;
using Scandic.CSVGenerator.Log;
using System.Configuration;

namespace CSVGenerator
{
    public class CSVHelper : List<string[]>
    {
        public CSVHelper(string inputCSVDir, char separator, string outputCSV, int memNoIndex, int lastNameIndex)
        {
            try
            {
                if (Directory.Exists(inputCSVDir))
                {
                    string[] filesToProcess = Directory.GetFiles(inputCSVDir);
                    if (filesToProcess != null && filesToProcess.Length > 0)
                    {
                        foreach (string file in filesToProcess)
                        {
                            ReadCSV(file, separator, outputCSV, memNoIndex, lastNameIndex);
                        }
                        Console.WriteLine("Finished Processing CSV Files");
                    }
                    else
                        CSVLogHelper.WriteLog(string.Format("No File available to process at location - {0} ", inputCSVDir));
                }
                else
                {
                    CSVLogHelper.WriteLog(string.Format("Directory \"{0}\" does not exists.", inputCSVDir));
                }
            }
            catch (Exception ex)
            {
                CSVLogHelper.WriteLog("Exception in CSV Helper", ex);
            }
        }

        /// <summary>
        /// Function to read the CSV file
        /// </summary>
        /// <param name="inputCSV">Path to read the CSV file(s) to process</param>
        /// <param name="separator">CSV data separator char</param>
        /// <param name="outputCSV">Path to write the processed CSV file(s)</param>
        void ReadCSV(string inputCSV, char separator, string outputCSV, int memNoIndex, int lastNameIndex)
        {
            // Read sample data from CSV file
            using (CsvFileReader reader = new CsvFileReader(inputCSV))
            {
                StreamWriter writer = new StreamWriter(outputCSV + @"\CSVProcessingLog.txt", true);
                string rowwithKey = string.Empty;
                try
                {
                    int rowIndex = 0;
                    bool firstRow = true;
                    CsvRow csvRow = new CsvRow();
                    StringBuilder sb = new StringBuilder();
                    while (reader.ReadRow(csvRow))
                    {
                        rowIndex++;
                        if (rowIndex > 1)
                            firstRow = false;

                        rowwithKey = ProcessRow(csvRow.LineText, separator, firstRow, memNoIndex, lastNameIndex);
                        sb.AppendLine(rowwithKey);
                    }
                    string fileName = inputCSV.Split('\\')[inputCSV.Split('\\').Length - 1].ToString();

                    File.WriteAllText(outputCSV + "\\" + fileName, sb.ToString(), Encoding.GetEncoding("iso-8859-1"));
                    writer.WriteLine(string.Format("{2} - Processed file {0}\\{1}", outputCSV, fileName, DateTime.Now));
                    writer.WriteLine("------------------------------------");

                }
                catch (Exception ex)
                {
                    CSVLogHelper.WriteLog(string.Format("Exception in Read CSV, Separator value: \"{0}\", Row value: \"{1}\"", separator, rowwithKey), ex);
                }
                finally
                {
                    reader.Close();
                    writer.Close();
                }
            }

        }

        private string CreateCSVRow(CsvRow csvRow, char separator)
        {
            string oneRow = string.Empty;
            foreach (string row in csvRow)
            {
                if (!string.IsNullOrEmpty(oneRow))
                    oneRow = oneRow + separator + row;
                else
                    oneRow = row;
            }
            return oneRow;
        }


        /// <summary>
        /// function to Process CSV row to add Encrypted key to it
        /// </summary>
        /// <param name="rowData">CSV row data</param>
        /// <param name="separator">CSV row data separator</param>
        /// <param name="firstRow">If First row or not</param>
        /// <returns>New CSV row with Encrypted key added in last column</returns>
        private string ProcessRow(string rowData, char separator, bool firstRow, int memNoIndex, int lastNameIndex)
        {
            string newRowData = string.Empty;
            string[] rowValues = rowData.Split(separator);
            if (rowValues != null && rowValues.Length > 0)
            {
                string dblQuotes = string.Empty;
                if (rowData[0] == '"') //Value enclosed in double quotes
                    dblQuotes = "\"";
                if (firstRow)
                    newRowData = rowData + separator + dblQuotes + "EncryptedKey" + dblQuotes;
                else
                {
                    string membershipNo = rowValues[memNoIndex].ToString();
                    string lastName = rowValues[lastNameIndex].ToString();
                    membershipNo = membershipNo.Contains("\"") ? membershipNo.Replace("\"", "") : membershipNo;
                    lastName = lastName.Contains("\"") ? lastName.Replace("\"", "") : lastName;
                    string key = !string.IsNullOrEmpty(lastName) ? GenerateKey(membershipNo, lastName) : "";
                    newRowData = rowData + separator + dblQuotes + key + dblQuotes;
                }
            }
            return newRowData;
        }

        /// <summary>
        /// Function to Generate Encrypted Key
        /// </summary>
        /// <param name="registrationID">Membership No</param>
        /// <param name="lastName">Last Name</param>
        /// <returns>Encrypted Key</returns>
        private string GenerateKey(string registrationID, string lastName)
        {
            //var payload = new Dictionary<string, string>
            //{
            //    { "MembershipID", registrationID },
            //    { "LastName", lastName },
            //};
            string payload = registrationID + "," + lastName;
            return ScanwebJsonWebToken.Encrypt(payload, Convert.ToString(ConfigurationSettings.AppSettings["EncryptionKey"]));
            //return ScanwebJsonWebToken.Encode(payload);
        }

        void WriteCSV(string csv)
        {
            // Write sample data to CSV file
            using (CsvFileWriter writer = new CsvFileWriter(csv))
            {
                for (int i = 0; i < 100; i++)
                {
                    CsvRow row = new CsvRow();
                    for (int j = 0; j < 5; j++)
                        row.Add(String.Format("Column{0}", j));
                    writer.WriteRow(row);
                }
            }
        }
    }

    /// <summary>
    /// Class to store one CSV row
    /// </summary>
    public class CsvRow : List<string>
    {
        public string LineText { get; set; }
    }

    /// <summary>
    /// Class to write data to a CSV file
    /// </summary>
    public class CsvFileWriter : StreamWriter
    {
        public CsvFileWriter(Stream stream)
            : base(stream)
        {
        }

        public CsvFileWriter(string filename)
            : base(filename)
        {
        }

        /// <summary>
        /// Writes a single row to a CSV file.
        /// </summary>
        /// <param name="row">The row to be written</param>
        public void WriteRow(CsvRow row)
        {
            StringBuilder builder = new StringBuilder();
            bool firstColumn = true;
            foreach (string value in row)
            {
                // Add separator if this isn't the first value
                if (!firstColumn)
                    builder.Append(',');
                // Implement special handling for values that contain comma or quote
                // Enclose in quotes and double up any double quotes
                if (value.IndexOfAny(new char[] { '"', ',' }) != -1)
                    builder.AppendFormat("\"{0}\"", value.Replace("\"", "\"\""));
                else
                    builder.Append(value);
                firstColumn = false;
            }
            row.LineText = builder.ToString();
            WriteLine(row.LineText);
        }
    }

    /// <summary>
    /// Class to read data from a CSV file
    /// </summary>
    public class CsvFileReader : StreamReader
    {
        public CsvFileReader(Stream stream)
            : base(stream)
        {
        }

        public CsvFileReader(string filename)
            : base(filename, Encoding.GetEncoding("iso-8859-1"))
        {
        }

        /// <summary>
        /// Reads a row of data value by value separated by Comma from a CSV file
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public bool ReadRowValueByValue(CsvRow row)
        {
            row.LineText = ReadLine();
            if (String.IsNullOrEmpty(row.LineText))
                return false;

            int pos = 0;
            int rows = 0;

            while (pos < row.LineText.Length)
            {
                string value;

                // Special handling for quoted field
                if (row.LineText[pos] == '"')
                {
                    // Skip initial quote
                    pos++;

                    // Parse quoted value
                    int start = pos;
                    while (pos < row.LineText.Length)
                    {
                        // Test for quote character
                        if (row.LineText[pos] == '"')
                        {
                            // Found one
                            pos++;

                            // If two quotes together, keep one
                            // Otherwise, indicates end of value
                            if (pos >= row.LineText.Length || row.LineText[pos] != '"')
                            {
                                pos--;
                                break;
                            }
                        }
                        pos++;
                    }
                    value = row.LineText.Substring(start, pos - start);
                    value = value.Replace("\"\"", "\"");
                }
                else
                {
                    // Parse unquoted value
                    int start = pos;
                    while (pos < row.LineText.Length && row.LineText[pos] != ',')
                        pos++;
                    value = row.LineText.Substring(start, pos - start);
                }

                // Add field to list
                if (rows < row.Count)
                    row[rows] = value;
                else
                    row.Add(value);
                rows++;

                // Eat up to and including next comma
                while (pos < row.LineText.Length && row.LineText[pos] != ',')
                    pos++;
                if (pos < row.LineText.Length)
                    pos++;
            }
            // Delete any unused items
            while (row.Count > rows)
                row.RemoveAt(rows);

            // Return true if any columns read
            return (row.Count > 0);
        }

        /// <summary>
        /// Reads a complete row of data irrespective of values in it from a CSV file
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public bool ReadRow(CsvRow row)
        {
            row.LineText = ReadLine();
            if (String.IsNullOrEmpty(row.LineText))
                return false;

            string value = row.LineText;
            bool hasRows = false;
            if (!string.IsNullOrEmpty(value))
            {
                row.Clear();
                row.Add(value);
                hasRows = true;
            }
            return hasRows;
        }
    }
}
