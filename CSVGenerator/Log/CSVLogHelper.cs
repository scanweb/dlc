﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Configuration;
using System.IO;

namespace Scandic.CSVGenerator.Log
{
    public class CSVLogHelper
    {
        static string m_baseDir = null;
        static CSVLogHelper()
        {
            m_baseDir = ConfigurationSettings.AppSettings["CSVLogPath"].ToString();
        }

        public static string GetFilenameYYYMMDD(string suffix, string extension)
        {
            return string.Format("{0}{1}{2}{3}", System.DateTime.Now.ToString("yyyy_MM_dd"), "_CSV", suffix, extension);
        }

        public static void WriteLog(String message)
        {
            //just in case: we protect code with try.
            try
            {
                if (Directory.Exists(m_baseDir))
                {
                    string filename = m_baseDir + GetFilenameYYYMMDD("_LOG", ".txt");
                    System.IO.StreamWriter sw = new System.IO.StreamWriter(filename, true);
                    XElement xmlEntry = new XElement("logEntry",
                        new XElement("Date", System.DateTime.Now.ToString()),
                        new XElement("Message", message));
                    sw.WriteLine(xmlEntry);
                    sw.Close();
                }
                else
                    Console.WriteLine(string.Format("Directory {0} does not exists.", m_baseDir));
            }
            catch (Exception)
            {
            }
        }

        public static void WriteLog(Exception ex)
        {
            //just in case: we protect code with try.
            try
            {
                if (Directory.Exists(m_baseDir))
                {
                    string filename = m_baseDir + GetFilenameYYYMMDD("_LOG", ".txt");
                    System.IO.StreamWriter sw = new System.IO.StreamWriter(filename, true);
                    XElement xmlEntry = new XElement("logEntry",
                        new XElement("Date", System.DateTime.Now.ToString()),
                        new XElement("Exception",
                            new XElement("Source", ex.Source),
                            new XElement("Message", ex.Message),
                            new XElement("Stack", ex.StackTrace)
                         )//end exception
                    );
                    //has inner exception?
                    if (ex.InnerException != null)
                    {
                        xmlEntry.Element("Exception").Add(
                            new XElement("InnerException",
                                new XElement("Source", ex.InnerException.Source),
                                new XElement("Message", ex.InnerException.Message),
                                new XElement("Stack", ex.InnerException.StackTrace))
                            );
                    }
                    sw.WriteLine(xmlEntry);
                    sw.Close();
                }
                else
                    Console.WriteLine(string.Format("Directory {0} does not exists.", m_baseDir));
            }
            catch (Exception)
            {
            }
        }

        public static void WriteLog(string message, Exception ex)
        {
            //just in case: we protect code with try.
            try
            {
                string filename = m_baseDir + GetFilenameYYYMMDD("_LOG", ".txt");
                System.IO.StreamWriter sw = new System.IO.StreamWriter(filename, true);
                XElement xmlEntry = new XElement("logEntry",
                    new XElement("Date", System.DateTime.Now.ToString()),
                    new XElement("Message", message),
                    new XElement("Exception",
                        new XElement("Source", ex.Source),
                        new XElement("Message", ex.Message),
                        new XElement("Stack", ex.StackTrace)
                     )//end exception
                );
                //has inner exception?
                if (ex.InnerException != null)
                {
                    xmlEntry.Element("Exception").Add(
                        new XElement("InnerException",
                            new XElement("Source", ex.InnerException.Source),
                            new XElement("Message", ex.InnerException.Message),
                            new XElement("Stack", ex.InnerException.StackTrace))
                        );
                }
                sw.WriteLine(xmlEntry);
                sw.Close();
            }
            catch (Exception)
            {
            }
        }
    }
}
