﻿using System;
using System.IO;
using System.Reflection;
using System.Web;
using System.Xml;

namespace Scandic.MembershipCards.Utils
{
    public class WebUtil
    {
        public static string GetCurrentLanguage()
        {
            string language = "en";
            string[] domainParts = HttpContext.Current.Request.Url.Host.Split('.');
            language = domainParts[domainParts.Length - 1];

            if (string.Equals(language, "com", StringComparison.InvariantCultureIgnoreCase))
            {
                language = "en";
            }
            else if (string.Equals(language, "se", StringComparison.InvariantCultureIgnoreCase))
            {
                language = "sv";
            }
            return language;
        }

        public static string GetTextFromXML(string xPath)
        {
            string templateFileName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase).ToString();
            templateFileName = templateFileName.Substring(0, templateFileName.LastIndexOf("\\"));
            templateFileName += "\\data\\MembershipDetailsContent.xml";
            string retValue = string.Empty;
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(templateFileName);
            XmlNode xmlNode = xmlDocument.SelectSingleNode(xPath);
            if (xmlNode != null)
                retValue = xmlNode.InnerText;
            return retValue;
        }
    }
}
