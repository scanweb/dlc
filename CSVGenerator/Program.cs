﻿using System;
using System.Configuration;
namespace CSVGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Starting Processing CSV Files");
            new CSVHelper(Convert.ToString(ConfigurationSettings.AppSettings["CSVPath"]),
                          Convert.ToChar(ConfigurationSettings.AppSettings["SeparatorChar"]),
                          Convert.ToString(ConfigurationSettings.AppSettings["CSVOutputPath"]),
                          Convert.ToInt32(ConfigurationSettings.AppSettings["MembershipNoIndex"]),
                          Convert.ToInt32(ConfigurationSettings.AppSettings["LastNameIndex"]));
        }
    }
}
