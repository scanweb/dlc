﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MemberDetails.aspx.cs"
    Inherits="Scandic.MembershipCards.MemberDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="format-detection" content="telephone=no">
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<head id="Head1" runat="server">
    <link rel="apple-touch-icon-precomposed" href="/MembershipCard/AddToHome/apple-touch-icon.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/MembershipCard/AddToHome/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/MembershipCard/AddToHome/apple-touch-icon-72x72.png" />
    <link rel="stylesheet" type="text/css" href="/MembershipCard/AddToHome/addtohomescreen.css">
    <link rel="stylesheet" type="text/css" href="/MembershipCard/Public/Styles/MembershipDetails.css">

    <script src="/MembershipCard/AddToHome/addtohomescreen.js"></script>

    <script>
        addToHomescreen({ appID: 'Schab', message: '<%=language%>' });
    </script>

    <title>Scandic</title>
</head>
<body>
    <div id="container">
        <header class="topHeader">
		<h2 class="logo"></h2>
		<h2 class="subTitle"></h2>		
		<div class="line"></div>
		<h3 class="personal-greeting" runat="server" ID="welcomeMember"></h3>
		<p id="headerText" runat="server">
		</p>
	</header>
        <section class='main'>	
		<article runat="server" id="cardCSS" class="card first-floor x2">
			<div class="membership-details">
				<div class="membership-number" runat="server" id="membershipNo"></div>
				<div class="info" runat="server" id="membershipName"></div>
				<div class="info expDate" runat="server" id="expDate"></div>
			</div>
		</article>
	</section>
        <div class="bottomHeaderContainer">
            <header class="bottomHeader">
		<h2 class="logo"></h2>
		<div class="line"></div>
	</header>
        </div>
        <section class="bottom">
		<div class="line" id="divWelcomeMsg" runat="server"></div>
		<p id="mobileheaderText" runat="server">
			
		</p>
	</section>
    </div>
</body>
</html>
