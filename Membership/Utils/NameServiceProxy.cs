﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Scandic.MembershipCards.NameServiceProxy
{

    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Web.Services;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;



    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name = "NameServiceSoap", Namespace = "http://tempuri.org/")]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Membership))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(CreditCard))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(SystemID))]
    public partial class NameService : System.Web.Services.Protocols.SoapHttpClientProtocol
    {

        private OGHeader oGHeaderValueField;

        private System.Threading.SendOrPostCallback RegisterNameOperationCompleted;

        private System.Threading.SendOrPostCallback FetchNameOperationCompleted;

        private System.Threading.SendOrPostCallback UpdateNameOperationCompleted;

        private System.Threading.SendOrPostCallback FetchAddressListOperationCompleted;

        private System.Threading.SendOrPostCallback InsertAddressOperationCompleted;

        private System.Threading.SendOrPostCallback UpdateAddressOperationCompleted;

        private System.Threading.SendOrPostCallback DeleteAddressOperationCompleted;

        private System.Threading.SendOrPostCallback FetchPhoneListOperationCompleted;

        private System.Threading.SendOrPostCallback InsertPhoneOperationCompleted;

        private System.Threading.SendOrPostCallback UpdatePhoneOperationCompleted;

        private System.Threading.SendOrPostCallback DeletePhoneOperationCompleted;

        private System.Threading.SendOrPostCallback FetchEmailListOperationCompleted;

        private System.Threading.SendOrPostCallback InsertEmailOperationCompleted;

        private System.Threading.SendOrPostCallback UpdateEmailOperationCompleted;

        private System.Threading.SendOrPostCallback DeleteEmailOperationCompleted;

        private System.Threading.SendOrPostCallback FetchCreditCardListOperationCompleted;

        private System.Threading.SendOrPostCallback InsertCreditCardOperationCompleted;

        private System.Threading.SendOrPostCallback UpdateCreditCardOperationCompleted;

        private System.Threading.SendOrPostCallback DeleteCreditCardOperationCompleted;

        private System.Threading.SendOrPostCallback GetPassportOperationCompleted;

        private System.Threading.SendOrPostCallback UpdatePassportOperationCompleted;

        private System.Threading.SendOrPostCallback DeletePassportOperationCompleted;

        private System.Threading.SendOrPostCallback FetchGuestCardListOperationCompleted;

        private System.Threading.SendOrPostCallback InsertGuestCardOperationCompleted;

        private System.Threading.SendOrPostCallback UpdateGuestCardOperationCompleted;

        private System.Threading.SendOrPostCallback DeleteGuestCardOperationCompleted;

        private System.Threading.SendOrPostCallback FetchPreferenceListOperationCompleted;

        private System.Threading.SendOrPostCallback InsertPreferenceOperationCompleted;

        private System.Threading.SendOrPostCallback DeletePreferenceOperationCompleted;

        private System.Threading.SendOrPostCallback FetchCommentListOperationCompleted;

        private System.Threading.SendOrPostCallback InsertCommentOperationCompleted;

        private System.Threading.SendOrPostCallback UpdateCommentOperationCompleted;

        private System.Threading.SendOrPostCallback DeleteCommentOperationCompleted;

        private System.Threading.SendOrPostCallback FetchPrivacyOptionOperationCompleted;

        private System.Threading.SendOrPostCallback InsertUpdatePrivacyOptionOperationCompleted;

        private System.Threading.SendOrPostCallback DeletePrivacyOptionOperationCompleted;

        private System.Threading.SendOrPostCallback NameLookupOperationCompleted;

        private System.Threading.SendOrPostCallback FetchNameUDFsOperationCompleted;

        private System.Threading.SendOrPostCallback InsertUpdateNameUDFsOperationCompleted;

        private System.Threading.SendOrPostCallback TravelAgentLookupOperationCompleted;

        private System.Threading.SendOrPostCallback FetchProfileOperationCompleted;

        private System.Threading.SendOrPostCallback InsertClaimOperationCompleted;

        private System.Threading.SendOrPostCallback FetchClaimsStatusOperationCompleted;

        private System.Threading.SendOrPostCallback UpdateClaimOperationCompleted;

        private System.Threading.SendOrPostCallback FetchSubscriptionOperationCompleted;

        private System.Threading.SendOrPostCallback FetchProfileBenefitsOperationCompleted;

        /// <remarks/>
        public NameService()
        {
            this.Url = "http://10.31.173.196/OWS_WS_51/Name.asmx";
        }

        public OGHeader OGHeaderValue
        {
            get
            {
                return this.oGHeaderValueField;
            }
            set
            {
                this.oGHeaderValueField = value;
            }
        }

        /// <remarks/>
        public event RegisterNameCompletedEventHandler RegisterNameCompleted;

        /// <remarks/>
        public event FetchNameCompletedEventHandler FetchNameCompleted;

        /// <remarks/>
        public event UpdateNameCompletedEventHandler UpdateNameCompleted;

        /// <remarks/>
        public event FetchAddressListCompletedEventHandler FetchAddressListCompleted;

        /// <remarks/>
        public event InsertAddressCompletedEventHandler InsertAddressCompleted;

        /// <remarks/>
        public event UpdateAddressCompletedEventHandler UpdateAddressCompleted;

        /// <remarks/>
        public event DeleteAddressCompletedEventHandler DeleteAddressCompleted;

        /// <remarks/>
        public event FetchPhoneListCompletedEventHandler FetchPhoneListCompleted;

        /// <remarks/>
        public event InsertPhoneCompletedEventHandler InsertPhoneCompleted;

        /// <remarks/>
        public event UpdatePhoneCompletedEventHandler UpdatePhoneCompleted;

        /// <remarks/>
        public event DeletePhoneCompletedEventHandler DeletePhoneCompleted;

        /// <remarks/>
        public event FetchEmailListCompletedEventHandler FetchEmailListCompleted;

        /// <remarks/>
        public event InsertEmailCompletedEventHandler InsertEmailCompleted;

        /// <remarks/>
        public event UpdateEmailCompletedEventHandler UpdateEmailCompleted;

        /// <remarks/>
        public event DeleteEmailCompletedEventHandler DeleteEmailCompleted;

        /// <remarks/>
        public event FetchCreditCardListCompletedEventHandler FetchCreditCardListCompleted;

        /// <remarks/>
        public event InsertCreditCardCompletedEventHandler InsertCreditCardCompleted;

        /// <remarks/>
        public event UpdateCreditCardCompletedEventHandler UpdateCreditCardCompleted;

        /// <remarks/>
        public event DeleteCreditCardCompletedEventHandler DeleteCreditCardCompleted;

        /// <remarks/>
        public event GetPassportCompletedEventHandler GetPassportCompleted;

        /// <remarks/>
        public event UpdatePassportCompletedEventHandler UpdatePassportCompleted;

        /// <remarks/>
        public event DeletePassportCompletedEventHandler DeletePassportCompleted;

        /// <remarks/>
        public event FetchGuestCardListCompletedEventHandler FetchGuestCardListCompleted;

        /// <remarks/>
        public event InsertGuestCardCompletedEventHandler InsertGuestCardCompleted;

        /// <remarks/>
        public event UpdateGuestCardCompletedEventHandler UpdateGuestCardCompleted;

        /// <remarks/>
        public event DeleteGuestCardCompletedEventHandler DeleteGuestCardCompleted;

        /// <remarks/>
        public event FetchPreferenceListCompletedEventHandler FetchPreferenceListCompleted;

        /// <remarks/>
        public event InsertPreferenceCompletedEventHandler InsertPreferenceCompleted;

        /// <remarks/>
        public event DeletePreferenceCompletedEventHandler DeletePreferenceCompleted;

        /// <remarks/>
        public event FetchCommentListCompletedEventHandler FetchCommentListCompleted;

        /// <remarks/>
        public event InsertCommentCompletedEventHandler InsertCommentCompleted;

        /// <remarks/>
        public event UpdateCommentCompletedEventHandler UpdateCommentCompleted;

        /// <remarks/>
        public event DeleteCommentCompletedEventHandler DeleteCommentCompleted;

        /// <remarks/>
        public event FetchPrivacyOptionCompletedEventHandler FetchPrivacyOptionCompleted;

        /// <remarks/>
        public event InsertUpdatePrivacyOptionCompletedEventHandler InsertUpdatePrivacyOptionCompleted;

        /// <remarks/>
        public event DeletePrivacyOptionCompletedEventHandler DeletePrivacyOptionCompleted;

        /// <remarks/>
        public event NameLookupCompletedEventHandler NameLookupCompleted;

        /// <remarks/>
        public event FetchNameUDFsCompletedEventHandler FetchNameUDFsCompleted;

        /// <remarks/>
        public event InsertUpdateNameUDFsCompletedEventHandler InsertUpdateNameUDFsCompleted;

        /// <remarks/>
        public event TravelAgentLookupCompletedEventHandler TravelAgentLookupCompleted;

        /// <remarks/>
        public event FetchProfileCompletedEventHandler FetchProfileCompleted;

        /// <remarks/>
        public event InsertClaimCompletedEventHandler InsertClaimCompleted;

        /// <remarks/>
        public event FetchClaimsStatusCompletedEventHandler FetchClaimsStatusCompleted;

        /// <remarks/>
        public event UpdateClaimCompletedEventHandler UpdateClaimCompleted;

        /// <remarks/>
        public event FetchSubscriptionCompletedEventHandler FetchSubscriptionCompleted;

        /// <remarks/>
        public event FetchProfileBenefitsCompletedEventHandler FetchProfileBenefitsCompleted;

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#RegisterName", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("RegisterNameResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public RegisterNameResponse RegisterName([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] RegisterNameRequest RegisterNameRequest)
        {
            object[] results = this.Invoke("RegisterName", new object[] {
                    RegisterNameRequest});
            return ((RegisterNameResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginRegisterName(RegisterNameRequest RegisterNameRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("RegisterName", new object[] {
                    RegisterNameRequest}, callback, asyncState);
        }

        /// <remarks/>
        public RegisterNameResponse EndRegisterName(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((RegisterNameResponse)(results[0]));
        }

        /// <remarks/>
        public void RegisterNameAsync(RegisterNameRequest RegisterNameRequest)
        {
            this.RegisterNameAsync(RegisterNameRequest, null);
        }

        /// <remarks/>
        public void RegisterNameAsync(RegisterNameRequest RegisterNameRequest, object userState)
        {
            if ((this.RegisterNameOperationCompleted == null))
            {
                this.RegisterNameOperationCompleted = new System.Threading.SendOrPostCallback(this.OnRegisterNameOperationCompleted);
            }
            this.InvokeAsync("RegisterName", new object[] {
                    RegisterNameRequest}, this.RegisterNameOperationCompleted, userState);
        }

        private void OnRegisterNameOperationCompleted(object arg)
        {
            if ((this.RegisterNameCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.RegisterNameCompleted(this, new RegisterNameCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#FetchName", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("FetchNameResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public FetchNameResponse FetchName([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] FetchNameRequest FetchNameRequest)
        {
            object[] results = this.Invoke("FetchName", new object[] {
                    FetchNameRequest});
            return ((FetchNameResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginFetchName(FetchNameRequest FetchNameRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("FetchName", new object[] {
                    FetchNameRequest}, callback, asyncState);
        }

        /// <remarks/>
        public FetchNameResponse EndFetchName(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((FetchNameResponse)(results[0]));
        }

        /// <remarks/>
        public void FetchNameAsync(FetchNameRequest FetchNameRequest)
        {
            this.FetchNameAsync(FetchNameRequest, null);
        }

        /// <remarks/>
        public void FetchNameAsync(FetchNameRequest FetchNameRequest, object userState)
        {
            if ((this.FetchNameOperationCompleted == null))
            {
                this.FetchNameOperationCompleted = new System.Threading.SendOrPostCallback(this.OnFetchNameOperationCompleted);
            }
            this.InvokeAsync("FetchName", new object[] {
                    FetchNameRequest}, this.FetchNameOperationCompleted, userState);
        }

        private void OnFetchNameOperationCompleted(object arg)
        {
            if ((this.FetchNameCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.FetchNameCompleted(this, new FetchNameCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#UpdateName", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("UpdateNameResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public UpdateNameResponse UpdateName([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] UpdateNameRequest UpdateNameRequest)
        {
            object[] results = this.Invoke("UpdateName", new object[] {
                    UpdateNameRequest});
            return ((UpdateNameResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginUpdateName(UpdateNameRequest UpdateNameRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("UpdateName", new object[] {
                    UpdateNameRequest}, callback, asyncState);
        }

        /// <remarks/>
        public UpdateNameResponse EndUpdateName(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((UpdateNameResponse)(results[0]));
        }

        /// <remarks/>
        public void UpdateNameAsync(UpdateNameRequest UpdateNameRequest)
        {
            this.UpdateNameAsync(UpdateNameRequest, null);
        }

        /// <remarks/>
        public void UpdateNameAsync(UpdateNameRequest UpdateNameRequest, object userState)
        {
            if ((this.UpdateNameOperationCompleted == null))
            {
                this.UpdateNameOperationCompleted = new System.Threading.SendOrPostCallback(this.OnUpdateNameOperationCompleted);
            }
            this.InvokeAsync("UpdateName", new object[] {
                    UpdateNameRequest}, this.UpdateNameOperationCompleted, userState);
        }

        private void OnUpdateNameOperationCompleted(object arg)
        {
            if ((this.UpdateNameCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.UpdateNameCompleted(this, new UpdateNameCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#FetchAddressList", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("FetchAddressListResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public FetchAddressListResponse FetchAddressList([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] FetchAddressListRequest FetchAddressListRequest)
        {
            object[] results = this.Invoke("FetchAddressList", new object[] {
                    FetchAddressListRequest});
            return ((FetchAddressListResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginFetchAddressList(FetchAddressListRequest FetchAddressListRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("FetchAddressList", new object[] {
                    FetchAddressListRequest}, callback, asyncState);
        }

        /// <remarks/>
        public FetchAddressListResponse EndFetchAddressList(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((FetchAddressListResponse)(results[0]));
        }

        /// <remarks/>
        public void FetchAddressListAsync(FetchAddressListRequest FetchAddressListRequest)
        {
            this.FetchAddressListAsync(FetchAddressListRequest, null);
        }

        /// <remarks/>
        public void FetchAddressListAsync(FetchAddressListRequest FetchAddressListRequest, object userState)
        {
            if ((this.FetchAddressListOperationCompleted == null))
            {
                this.FetchAddressListOperationCompleted = new System.Threading.SendOrPostCallback(this.OnFetchAddressListOperationCompleted);
            }
            this.InvokeAsync("FetchAddressList", new object[] {
                    FetchAddressListRequest}, this.FetchAddressListOperationCompleted, userState);
        }

        private void OnFetchAddressListOperationCompleted(object arg)
        {
            if ((this.FetchAddressListCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.FetchAddressListCompleted(this, new FetchAddressListCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#InsertAddress", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("InsertAddressResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public InsertAddressResponse InsertAddress([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] InsertAddressRequest InsertAddressRequest)
        {
            object[] results = this.Invoke("InsertAddress", new object[] {
                    InsertAddressRequest});
            return ((InsertAddressResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginInsertAddress(InsertAddressRequest InsertAddressRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("InsertAddress", new object[] {
                    InsertAddressRequest}, callback, asyncState);
        }

        /// <remarks/>
        public InsertAddressResponse EndInsertAddress(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((InsertAddressResponse)(results[0]));
        }

        /// <remarks/>
        public void InsertAddressAsync(InsertAddressRequest InsertAddressRequest)
        {
            this.InsertAddressAsync(InsertAddressRequest, null);
        }

        /// <remarks/>
        public void InsertAddressAsync(InsertAddressRequest InsertAddressRequest, object userState)
        {
            if ((this.InsertAddressOperationCompleted == null))
            {
                this.InsertAddressOperationCompleted = new System.Threading.SendOrPostCallback(this.OnInsertAddressOperationCompleted);
            }
            this.InvokeAsync("InsertAddress", new object[] {
                    InsertAddressRequest}, this.InsertAddressOperationCompleted, userState);
        }

        private void OnInsertAddressOperationCompleted(object arg)
        {
            if ((this.InsertAddressCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.InsertAddressCompleted(this, new InsertAddressCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#UpdateAddress", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("UpdateAddressResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public UpdateAddressResponse UpdateAddress([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] UpdateAddressRequest UpdateAddressRequest)
        {
            object[] results = this.Invoke("UpdateAddress", new object[] {
                    UpdateAddressRequest});
            return ((UpdateAddressResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginUpdateAddress(UpdateAddressRequest UpdateAddressRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("UpdateAddress", new object[] {
                    UpdateAddressRequest}, callback, asyncState);
        }

        /// <remarks/>
        public UpdateAddressResponse EndUpdateAddress(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((UpdateAddressResponse)(results[0]));
        }

        /// <remarks/>
        public void UpdateAddressAsync(UpdateAddressRequest UpdateAddressRequest)
        {
            this.UpdateAddressAsync(UpdateAddressRequest, null);
        }

        /// <remarks/>
        public void UpdateAddressAsync(UpdateAddressRequest UpdateAddressRequest, object userState)
        {
            if ((this.UpdateAddressOperationCompleted == null))
            {
                this.UpdateAddressOperationCompleted = new System.Threading.SendOrPostCallback(this.OnUpdateAddressOperationCompleted);
            }
            this.InvokeAsync("UpdateAddress", new object[] {
                    UpdateAddressRequest}, this.UpdateAddressOperationCompleted, userState);
        }

        private void OnUpdateAddressOperationCompleted(object arg)
        {
            if ((this.UpdateAddressCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.UpdateAddressCompleted(this, new UpdateAddressCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#DeleteAddress", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("DeleteAddressResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public DeleteAddressResponse DeleteAddress([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] DeleteAddressRequest DeleteAddressRequest)
        {
            object[] results = this.Invoke("DeleteAddress", new object[] {
                    DeleteAddressRequest});
            return ((DeleteAddressResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginDeleteAddress(DeleteAddressRequest DeleteAddressRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("DeleteAddress", new object[] {
                    DeleteAddressRequest}, callback, asyncState);
        }

        /// <remarks/>
        public DeleteAddressResponse EndDeleteAddress(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((DeleteAddressResponse)(results[0]));
        }

        /// <remarks/>
        public void DeleteAddressAsync(DeleteAddressRequest DeleteAddressRequest)
        {
            this.DeleteAddressAsync(DeleteAddressRequest, null);
        }

        /// <remarks/>
        public void DeleteAddressAsync(DeleteAddressRequest DeleteAddressRequest, object userState)
        {
            if ((this.DeleteAddressOperationCompleted == null))
            {
                this.DeleteAddressOperationCompleted = new System.Threading.SendOrPostCallback(this.OnDeleteAddressOperationCompleted);
            }
            this.InvokeAsync("DeleteAddress", new object[] {
                    DeleteAddressRequest}, this.DeleteAddressOperationCompleted, userState);
        }

        private void OnDeleteAddressOperationCompleted(object arg)
        {
            if ((this.DeleteAddressCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.DeleteAddressCompleted(this, new DeleteAddressCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#FetchPhoneList", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("FetchPhoneListResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public FetchPhoneListResponse FetchPhoneList([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] FetchPhoneListRequest FetchPhoneListRequest)
        {
            object[] results = this.Invoke("FetchPhoneList", new object[] {
                    FetchPhoneListRequest});
            return ((FetchPhoneListResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginFetchPhoneList(FetchPhoneListRequest FetchPhoneListRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("FetchPhoneList", new object[] {
                    FetchPhoneListRequest}, callback, asyncState);
        }

        /// <remarks/>
        public FetchPhoneListResponse EndFetchPhoneList(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((FetchPhoneListResponse)(results[0]));
        }

        /// <remarks/>
        public void FetchPhoneListAsync(FetchPhoneListRequest FetchPhoneListRequest)
        {
            this.FetchPhoneListAsync(FetchPhoneListRequest, null);
        }

        /// <remarks/>
        public void FetchPhoneListAsync(FetchPhoneListRequest FetchPhoneListRequest, object userState)
        {
            if ((this.FetchPhoneListOperationCompleted == null))
            {
                this.FetchPhoneListOperationCompleted = new System.Threading.SendOrPostCallback(this.OnFetchPhoneListOperationCompleted);
            }
            this.InvokeAsync("FetchPhoneList", new object[] {
                    FetchPhoneListRequest}, this.FetchPhoneListOperationCompleted, userState);
        }

        private void OnFetchPhoneListOperationCompleted(object arg)
        {
            if ((this.FetchPhoneListCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.FetchPhoneListCompleted(this, new FetchPhoneListCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#InsertPhone", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("InsertPhoneResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public InsertPhoneResponse InsertPhone([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] InsertPhoneRequest InsertPhoneRequest)
        {
            object[] results = this.Invoke("InsertPhone", new object[] {
                    InsertPhoneRequest});
            return ((InsertPhoneResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginInsertPhone(InsertPhoneRequest InsertPhoneRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("InsertPhone", new object[] {
                    InsertPhoneRequest}, callback, asyncState);
        }

        /// <remarks/>
        public InsertPhoneResponse EndInsertPhone(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((InsertPhoneResponse)(results[0]));
        }

        /// <remarks/>
        public void InsertPhoneAsync(InsertPhoneRequest InsertPhoneRequest)
        {
            this.InsertPhoneAsync(InsertPhoneRequest, null);
        }

        /// <remarks/>
        public void InsertPhoneAsync(InsertPhoneRequest InsertPhoneRequest, object userState)
        {
            if ((this.InsertPhoneOperationCompleted == null))
            {
                this.InsertPhoneOperationCompleted = new System.Threading.SendOrPostCallback(this.OnInsertPhoneOperationCompleted);
            }
            this.InvokeAsync("InsertPhone", new object[] {
                    InsertPhoneRequest}, this.InsertPhoneOperationCompleted, userState);
        }

        private void OnInsertPhoneOperationCompleted(object arg)
        {
            if ((this.InsertPhoneCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.InsertPhoneCompleted(this, new InsertPhoneCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#UpdatePhone", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("UpdatePhoneResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public UpdatePhoneResponse UpdatePhone([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] UpdatePhoneRequest UpdatePhoneRequest)
        {
            object[] results = this.Invoke("UpdatePhone", new object[] {
                    UpdatePhoneRequest});
            return ((UpdatePhoneResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginUpdatePhone(UpdatePhoneRequest UpdatePhoneRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("UpdatePhone", new object[] {
                    UpdatePhoneRequest}, callback, asyncState);
        }

        /// <remarks/>
        public UpdatePhoneResponse EndUpdatePhone(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((UpdatePhoneResponse)(results[0]));
        }

        /// <remarks/>
        public void UpdatePhoneAsync(UpdatePhoneRequest UpdatePhoneRequest)
        {
            this.UpdatePhoneAsync(UpdatePhoneRequest, null);
        }

        /// <remarks/>
        public void UpdatePhoneAsync(UpdatePhoneRequest UpdatePhoneRequest, object userState)
        {
            if ((this.UpdatePhoneOperationCompleted == null))
            {
                this.UpdatePhoneOperationCompleted = new System.Threading.SendOrPostCallback(this.OnUpdatePhoneOperationCompleted);
            }
            this.InvokeAsync("UpdatePhone", new object[] {
                    UpdatePhoneRequest}, this.UpdatePhoneOperationCompleted, userState);
        }

        private void OnUpdatePhoneOperationCompleted(object arg)
        {
            if ((this.UpdatePhoneCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.UpdatePhoneCompleted(this, new UpdatePhoneCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#DeletePhone", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("DeletePhoneResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public DeletePhoneResponse DeletePhone([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] DeletePhoneRequest DeletePhoneRequest)
        {
            object[] results = this.Invoke("DeletePhone", new object[] {
                    DeletePhoneRequest});
            return ((DeletePhoneResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginDeletePhone(DeletePhoneRequest DeletePhoneRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("DeletePhone", new object[] {
                    DeletePhoneRequest}, callback, asyncState);
        }

        /// <remarks/>
        public DeletePhoneResponse EndDeletePhone(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((DeletePhoneResponse)(results[0]));
        }

        /// <remarks/>
        public void DeletePhoneAsync(DeletePhoneRequest DeletePhoneRequest)
        {
            this.DeletePhoneAsync(DeletePhoneRequest, null);
        }

        /// <remarks/>
        public void DeletePhoneAsync(DeletePhoneRequest DeletePhoneRequest, object userState)
        {
            if ((this.DeletePhoneOperationCompleted == null))
            {
                this.DeletePhoneOperationCompleted = new System.Threading.SendOrPostCallback(this.OnDeletePhoneOperationCompleted);
            }
            this.InvokeAsync("DeletePhone", new object[] {
                    DeletePhoneRequest}, this.DeletePhoneOperationCompleted, userState);
        }

        private void OnDeletePhoneOperationCompleted(object arg)
        {
            if ((this.DeletePhoneCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.DeletePhoneCompleted(this, new DeletePhoneCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#FetchEmailList", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("FetchEmailListResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public FetchEmailListResponse FetchEmailList([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] FetchEmailListRequest FetchEmailListRequest)
        {
            object[] results = this.Invoke("FetchEmailList", new object[] {
                    FetchEmailListRequest});
            return ((FetchEmailListResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginFetchEmailList(FetchEmailListRequest FetchEmailListRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("FetchEmailList", new object[] {
                    FetchEmailListRequest}, callback, asyncState);
        }

        /// <remarks/>
        public FetchEmailListResponse EndFetchEmailList(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((FetchEmailListResponse)(results[0]));
        }

        /// <remarks/>
        public void FetchEmailListAsync(FetchEmailListRequest FetchEmailListRequest)
        {
            this.FetchEmailListAsync(FetchEmailListRequest, null);
        }

        /// <remarks/>
        public void FetchEmailListAsync(FetchEmailListRequest FetchEmailListRequest, object userState)
        {
            if ((this.FetchEmailListOperationCompleted == null))
            {
                this.FetchEmailListOperationCompleted = new System.Threading.SendOrPostCallback(this.OnFetchEmailListOperationCompleted);
            }
            this.InvokeAsync("FetchEmailList", new object[] {
                    FetchEmailListRequest}, this.FetchEmailListOperationCompleted, userState);
        }

        private void OnFetchEmailListOperationCompleted(object arg)
        {
            if ((this.FetchEmailListCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.FetchEmailListCompleted(this, new FetchEmailListCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#InsertEmail", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("InsertEmailResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public InsertEmailResponse InsertEmail([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] InsertEmailRequest InsertEmailRequest)
        {
            object[] results = this.Invoke("InsertEmail", new object[] {
                    InsertEmailRequest});
            return ((InsertEmailResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginInsertEmail(InsertEmailRequest InsertEmailRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("InsertEmail", new object[] {
                    InsertEmailRequest}, callback, asyncState);
        }

        /// <remarks/>
        public InsertEmailResponse EndInsertEmail(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((InsertEmailResponse)(results[0]));
        }

        /// <remarks/>
        public void InsertEmailAsync(InsertEmailRequest InsertEmailRequest)
        {
            this.InsertEmailAsync(InsertEmailRequest, null);
        }

        /// <remarks/>
        public void InsertEmailAsync(InsertEmailRequest InsertEmailRequest, object userState)
        {
            if ((this.InsertEmailOperationCompleted == null))
            {
                this.InsertEmailOperationCompleted = new System.Threading.SendOrPostCallback(this.OnInsertEmailOperationCompleted);
            }
            this.InvokeAsync("InsertEmail", new object[] {
                    InsertEmailRequest}, this.InsertEmailOperationCompleted, userState);
        }

        private void OnInsertEmailOperationCompleted(object arg)
        {
            if ((this.InsertEmailCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.InsertEmailCompleted(this, new InsertEmailCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#UpdateEmail", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("UpdateEmailResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public UpdateEmailResponse UpdateEmail([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] UpdateEmailRequest UpdateEmailRequest)
        {
            object[] results = this.Invoke("UpdateEmail", new object[] {
                    UpdateEmailRequest});
            return ((UpdateEmailResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginUpdateEmail(UpdateEmailRequest UpdateEmailRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("UpdateEmail", new object[] {
                    UpdateEmailRequest}, callback, asyncState);
        }

        /// <remarks/>
        public UpdateEmailResponse EndUpdateEmail(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((UpdateEmailResponse)(results[0]));
        }

        /// <remarks/>
        public void UpdateEmailAsync(UpdateEmailRequest UpdateEmailRequest)
        {
            this.UpdateEmailAsync(UpdateEmailRequest, null);
        }

        /// <remarks/>
        public void UpdateEmailAsync(UpdateEmailRequest UpdateEmailRequest, object userState)
        {
            if ((this.UpdateEmailOperationCompleted == null))
            {
                this.UpdateEmailOperationCompleted = new System.Threading.SendOrPostCallback(this.OnUpdateEmailOperationCompleted);
            }
            this.InvokeAsync("UpdateEmail", new object[] {
                    UpdateEmailRequest}, this.UpdateEmailOperationCompleted, userState);
        }

        private void OnUpdateEmailOperationCompleted(object arg)
        {
            if ((this.UpdateEmailCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.UpdateEmailCompleted(this, new UpdateEmailCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#DeleteEmail", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("DeleteEmailResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public DeleteEmailResponse DeleteEmail([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] DeleteEmailRequest DeleteEmailRequest)
        {
            object[] results = this.Invoke("DeleteEmail", new object[] {
                    DeleteEmailRequest});
            return ((DeleteEmailResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginDeleteEmail(DeleteEmailRequest DeleteEmailRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("DeleteEmail", new object[] {
                    DeleteEmailRequest}, callback, asyncState);
        }

        /// <remarks/>
        public DeleteEmailResponse EndDeleteEmail(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((DeleteEmailResponse)(results[0]));
        }

        /// <remarks/>
        public void DeleteEmailAsync(DeleteEmailRequest DeleteEmailRequest)
        {
            this.DeleteEmailAsync(DeleteEmailRequest, null);
        }

        /// <remarks/>
        public void DeleteEmailAsync(DeleteEmailRequest DeleteEmailRequest, object userState)
        {
            if ((this.DeleteEmailOperationCompleted == null))
            {
                this.DeleteEmailOperationCompleted = new System.Threading.SendOrPostCallback(this.OnDeleteEmailOperationCompleted);
            }
            this.InvokeAsync("DeleteEmail", new object[] {
                    DeleteEmailRequest}, this.DeleteEmailOperationCompleted, userState);
        }

        private void OnDeleteEmailOperationCompleted(object arg)
        {
            if ((this.DeleteEmailCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.DeleteEmailCompleted(this, new DeleteEmailCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#FetchCreditCardList", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("FetchCreditCardListResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public FetchCreditCardListResponse FetchCreditCardList([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] FetchCreditCardListRequest FetchCreditCardListRequest)
        {
            object[] results = this.Invoke("FetchCreditCardList", new object[] {
                    FetchCreditCardListRequest});
            return ((FetchCreditCardListResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginFetchCreditCardList(FetchCreditCardListRequest FetchCreditCardListRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("FetchCreditCardList", new object[] {
                    FetchCreditCardListRequest}, callback, asyncState);
        }

        /// <remarks/>
        public FetchCreditCardListResponse EndFetchCreditCardList(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((FetchCreditCardListResponse)(results[0]));
        }

        /// <remarks/>
        public void FetchCreditCardListAsync(FetchCreditCardListRequest FetchCreditCardListRequest)
        {
            this.FetchCreditCardListAsync(FetchCreditCardListRequest, null);
        }

        /// <remarks/>
        public void FetchCreditCardListAsync(FetchCreditCardListRequest FetchCreditCardListRequest, object userState)
        {
            if ((this.FetchCreditCardListOperationCompleted == null))
            {
                this.FetchCreditCardListOperationCompleted = new System.Threading.SendOrPostCallback(this.OnFetchCreditCardListOperationCompleted);
            }
            this.InvokeAsync("FetchCreditCardList", new object[] {
                    FetchCreditCardListRequest}, this.FetchCreditCardListOperationCompleted, userState);
        }

        private void OnFetchCreditCardListOperationCompleted(object arg)
        {
            if ((this.FetchCreditCardListCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.FetchCreditCardListCompleted(this, new FetchCreditCardListCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#InsertCreditCard", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("InsertCreditCardResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public InsertCreditCardResponse InsertCreditCard([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] InsertCreditCardRequest InsertCreditCardRequest)
        {
            object[] results = this.Invoke("InsertCreditCard", new object[] {
                    InsertCreditCardRequest});
            return ((InsertCreditCardResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginInsertCreditCard(InsertCreditCardRequest InsertCreditCardRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("InsertCreditCard", new object[] {
                    InsertCreditCardRequest}, callback, asyncState);
        }

        /// <remarks/>
        public InsertCreditCardResponse EndInsertCreditCard(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((InsertCreditCardResponse)(results[0]));
        }

        /// <remarks/>
        public void InsertCreditCardAsync(InsertCreditCardRequest InsertCreditCardRequest)
        {
            this.InsertCreditCardAsync(InsertCreditCardRequest, null);
        }

        /// <remarks/>
        public void InsertCreditCardAsync(InsertCreditCardRequest InsertCreditCardRequest, object userState)
        {
            if ((this.InsertCreditCardOperationCompleted == null))
            {
                this.InsertCreditCardOperationCompleted = new System.Threading.SendOrPostCallback(this.OnInsertCreditCardOperationCompleted);
            }
            this.InvokeAsync("InsertCreditCard", new object[] {
                    InsertCreditCardRequest}, this.InsertCreditCardOperationCompleted, userState);
        }

        private void OnInsertCreditCardOperationCompleted(object arg)
        {
            if ((this.InsertCreditCardCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.InsertCreditCardCompleted(this, new InsertCreditCardCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#UpdateCreditCard", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("UpdateCreditCardResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public UpdateCreditCardResponse UpdateCreditCard([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] UpdateCreditCardRequest UpdateCreditCardRequest)
        {
            object[] results = this.Invoke("UpdateCreditCard", new object[] {
                    UpdateCreditCardRequest});
            return ((UpdateCreditCardResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginUpdateCreditCard(UpdateCreditCardRequest UpdateCreditCardRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("UpdateCreditCard", new object[] {
                    UpdateCreditCardRequest}, callback, asyncState);
        }

        /// <remarks/>
        public UpdateCreditCardResponse EndUpdateCreditCard(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((UpdateCreditCardResponse)(results[0]));
        }

        /// <remarks/>
        public void UpdateCreditCardAsync(UpdateCreditCardRequest UpdateCreditCardRequest)
        {
            this.UpdateCreditCardAsync(UpdateCreditCardRequest, null);
        }

        /// <remarks/>
        public void UpdateCreditCardAsync(UpdateCreditCardRequest UpdateCreditCardRequest, object userState)
        {
            if ((this.UpdateCreditCardOperationCompleted == null))
            {
                this.UpdateCreditCardOperationCompleted = new System.Threading.SendOrPostCallback(this.OnUpdateCreditCardOperationCompleted);
            }
            this.InvokeAsync("UpdateCreditCard", new object[] {
                    UpdateCreditCardRequest}, this.UpdateCreditCardOperationCompleted, userState);
        }

        private void OnUpdateCreditCardOperationCompleted(object arg)
        {
            if ((this.UpdateCreditCardCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.UpdateCreditCardCompleted(this, new UpdateCreditCardCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#DeleteCreditCard", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("DeleteCreditCardResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public DeleteCreditCardResponse DeleteCreditCard([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] DeleteCreditCardRequest DeleteCreditCardRequest)
        {
            object[] results = this.Invoke("DeleteCreditCard", new object[] {
                    DeleteCreditCardRequest});
            return ((DeleteCreditCardResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginDeleteCreditCard(DeleteCreditCardRequest DeleteCreditCardRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("DeleteCreditCard", new object[] {
                    DeleteCreditCardRequest}, callback, asyncState);
        }

        /// <remarks/>
        public DeleteCreditCardResponse EndDeleteCreditCard(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((DeleteCreditCardResponse)(results[0]));
        }

        /// <remarks/>
        public void DeleteCreditCardAsync(DeleteCreditCardRequest DeleteCreditCardRequest)
        {
            this.DeleteCreditCardAsync(DeleteCreditCardRequest, null);
        }

        /// <remarks/>
        public void DeleteCreditCardAsync(DeleteCreditCardRequest DeleteCreditCardRequest, object userState)
        {
            if ((this.DeleteCreditCardOperationCompleted == null))
            {
                this.DeleteCreditCardOperationCompleted = new System.Threading.SendOrPostCallback(this.OnDeleteCreditCardOperationCompleted);
            }
            this.InvokeAsync("DeleteCreditCard", new object[] {
                    DeleteCreditCardRequest}, this.DeleteCreditCardOperationCompleted, userState);
        }

        private void OnDeleteCreditCardOperationCompleted(object arg)
        {
            if ((this.DeleteCreditCardCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.DeleteCreditCardCompleted(this, new DeleteCreditCardCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#GetPassport", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("GetPassportResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public GetPassportResponse GetPassport([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] GetPassportRequest GetPassportRequest)
        {
            object[] results = this.Invoke("GetPassport", new object[] {
                    GetPassportRequest});
            return ((GetPassportResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginGetPassport(GetPassportRequest GetPassportRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("GetPassport", new object[] {
                    GetPassportRequest}, callback, asyncState);
        }

        /// <remarks/>
        public GetPassportResponse EndGetPassport(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((GetPassportResponse)(results[0]));
        }

        /// <remarks/>
        public void GetPassportAsync(GetPassportRequest GetPassportRequest)
        {
            this.GetPassportAsync(GetPassportRequest, null);
        }

        /// <remarks/>
        public void GetPassportAsync(GetPassportRequest GetPassportRequest, object userState)
        {
            if ((this.GetPassportOperationCompleted == null))
            {
                this.GetPassportOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetPassportOperationCompleted);
            }
            this.InvokeAsync("GetPassport", new object[] {
                    GetPassportRequest}, this.GetPassportOperationCompleted, userState);
        }

        private void OnGetPassportOperationCompleted(object arg)
        {
            if ((this.GetPassportCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetPassportCompleted(this, new GetPassportCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#UpdatePassport", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("UpdatePassportResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public UpdatePassportResponse UpdatePassport([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] UpdatePassportRequest UpdatePassportRequest)
        {
            object[] results = this.Invoke("UpdatePassport", new object[] {
                    UpdatePassportRequest});
            return ((UpdatePassportResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginUpdatePassport(UpdatePassportRequest UpdatePassportRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("UpdatePassport", new object[] {
                    UpdatePassportRequest}, callback, asyncState);
        }

        /// <remarks/>
        public UpdatePassportResponse EndUpdatePassport(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((UpdatePassportResponse)(results[0]));
        }

        /// <remarks/>
        public void UpdatePassportAsync(UpdatePassportRequest UpdatePassportRequest)
        {
            this.UpdatePassportAsync(UpdatePassportRequest, null);
        }

        /// <remarks/>
        public void UpdatePassportAsync(UpdatePassportRequest UpdatePassportRequest, object userState)
        {
            if ((this.UpdatePassportOperationCompleted == null))
            {
                this.UpdatePassportOperationCompleted = new System.Threading.SendOrPostCallback(this.OnUpdatePassportOperationCompleted);
            }
            this.InvokeAsync("UpdatePassport", new object[] {
                    UpdatePassportRequest}, this.UpdatePassportOperationCompleted, userState);
        }

        private void OnUpdatePassportOperationCompleted(object arg)
        {
            if ((this.UpdatePassportCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.UpdatePassportCompleted(this, new UpdatePassportCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#DeletePassport", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("DeletePassportResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public DeletePassportResponse DeletePassport([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] DeletePassportRequest DeletePassportRequest)
        {
            object[] results = this.Invoke("DeletePassport", new object[] {
                    DeletePassportRequest});
            return ((DeletePassportResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginDeletePassport(DeletePassportRequest DeletePassportRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("DeletePassport", new object[] {
                    DeletePassportRequest}, callback, asyncState);
        }

        /// <remarks/>
        public DeletePassportResponse EndDeletePassport(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((DeletePassportResponse)(results[0]));
        }

        /// <remarks/>
        public void DeletePassportAsync(DeletePassportRequest DeletePassportRequest)
        {
            this.DeletePassportAsync(DeletePassportRequest, null);
        }

        /// <remarks/>
        public void DeletePassportAsync(DeletePassportRequest DeletePassportRequest, object userState)
        {
            if ((this.DeletePassportOperationCompleted == null))
            {
                this.DeletePassportOperationCompleted = new System.Threading.SendOrPostCallback(this.OnDeletePassportOperationCompleted);
            }
            this.InvokeAsync("DeletePassport", new object[] {
                    DeletePassportRequest}, this.DeletePassportOperationCompleted, userState);
        }

        private void OnDeletePassportOperationCompleted(object arg)
        {
            if ((this.DeletePassportCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.DeletePassportCompleted(this, new DeletePassportCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#FetchGuestCardList", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("FetchGuestCardListResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public FetchGuestCardListResponse FetchGuestCardList([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] FetchGuestCardListRequest FetchGuestCardListRequest)
        {
            object[] results = this.Invoke("FetchGuestCardList", new object[] {
                    FetchGuestCardListRequest});
            return ((FetchGuestCardListResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginFetchGuestCardList(FetchGuestCardListRequest FetchGuestCardListRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("FetchGuestCardList", new object[] {
                    FetchGuestCardListRequest}, callback, asyncState);
        }

        /// <remarks/>
        public FetchGuestCardListResponse EndFetchGuestCardList(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((FetchGuestCardListResponse)(results[0]));
        }

        /// <remarks/>
        public void FetchGuestCardListAsync(FetchGuestCardListRequest FetchGuestCardListRequest)
        {
            this.FetchGuestCardListAsync(FetchGuestCardListRequest, null);
        }

        /// <remarks/>
        public void FetchGuestCardListAsync(FetchGuestCardListRequest FetchGuestCardListRequest, object userState)
        {
            if ((this.FetchGuestCardListOperationCompleted == null))
            {
                this.FetchGuestCardListOperationCompleted = new System.Threading.SendOrPostCallback(this.OnFetchGuestCardListOperationCompleted);
            }
            this.InvokeAsync("FetchGuestCardList", new object[] {
                    FetchGuestCardListRequest}, this.FetchGuestCardListOperationCompleted, userState);
        }

        private void OnFetchGuestCardListOperationCompleted(object arg)
        {
            if ((this.FetchGuestCardListCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.FetchGuestCardListCompleted(this, new FetchGuestCardListCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#InsertGuestCard", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("InsertGuestCardResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public InsertGuestCardResponse InsertGuestCard([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] InsertGuestCardRequest InsertGuestCardRequest)
        {
            object[] results = this.Invoke("InsertGuestCard", new object[] {
                    InsertGuestCardRequest});
            return ((InsertGuestCardResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginInsertGuestCard(InsertGuestCardRequest InsertGuestCardRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("InsertGuestCard", new object[] {
                    InsertGuestCardRequest}, callback, asyncState);
        }

        /// <remarks/>
        public InsertGuestCardResponse EndInsertGuestCard(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((InsertGuestCardResponse)(results[0]));
        }

        /// <remarks/>
        public void InsertGuestCardAsync(InsertGuestCardRequest InsertGuestCardRequest)
        {
            this.InsertGuestCardAsync(InsertGuestCardRequest, null);
        }

        /// <remarks/>
        public void InsertGuestCardAsync(InsertGuestCardRequest InsertGuestCardRequest, object userState)
        {
            if ((this.InsertGuestCardOperationCompleted == null))
            {
                this.InsertGuestCardOperationCompleted = new System.Threading.SendOrPostCallback(this.OnInsertGuestCardOperationCompleted);
            }
            this.InvokeAsync("InsertGuestCard", new object[] {
                    InsertGuestCardRequest}, this.InsertGuestCardOperationCompleted, userState);
        }

        private void OnInsertGuestCardOperationCompleted(object arg)
        {
            if ((this.InsertGuestCardCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.InsertGuestCardCompleted(this, new InsertGuestCardCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#UpdateGuestCard", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("UpdateGuestCardResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public UpdateGuestCardResponse UpdateGuestCard([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] UpdateGuestCardRequest UpdateGuestCardRequest)
        {
            object[] results = this.Invoke("UpdateGuestCard", new object[] {
                    UpdateGuestCardRequest});
            return ((UpdateGuestCardResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginUpdateGuestCard(UpdateGuestCardRequest UpdateGuestCardRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("UpdateGuestCard", new object[] {
                    UpdateGuestCardRequest}, callback, asyncState);
        }

        /// <remarks/>
        public UpdateGuestCardResponse EndUpdateGuestCard(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((UpdateGuestCardResponse)(results[0]));
        }

        /// <remarks/>
        public void UpdateGuestCardAsync(UpdateGuestCardRequest UpdateGuestCardRequest)
        {
            this.UpdateGuestCardAsync(UpdateGuestCardRequest, null);
        }

        /// <remarks/>
        public void UpdateGuestCardAsync(UpdateGuestCardRequest UpdateGuestCardRequest, object userState)
        {
            if ((this.UpdateGuestCardOperationCompleted == null))
            {
                this.UpdateGuestCardOperationCompleted = new System.Threading.SendOrPostCallback(this.OnUpdateGuestCardOperationCompleted);
            }
            this.InvokeAsync("UpdateGuestCard", new object[] {
                    UpdateGuestCardRequest}, this.UpdateGuestCardOperationCompleted, userState);
        }

        private void OnUpdateGuestCardOperationCompleted(object arg)
        {
            if ((this.UpdateGuestCardCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.UpdateGuestCardCompleted(this, new UpdateGuestCardCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#DeleteGuestCard", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("DeleteGuestCardResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public DeleteGuestCardResponse DeleteGuestCard([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] DeleteGuestCardRequest DeleteGuestCardRequest)
        {
            object[] results = this.Invoke("DeleteGuestCard", new object[] {
                    DeleteGuestCardRequest});
            return ((DeleteGuestCardResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginDeleteGuestCard(DeleteGuestCardRequest DeleteGuestCardRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("DeleteGuestCard", new object[] {
                    DeleteGuestCardRequest}, callback, asyncState);
        }

        /// <remarks/>
        public DeleteGuestCardResponse EndDeleteGuestCard(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((DeleteGuestCardResponse)(results[0]));
        }

        /// <remarks/>
        public void DeleteGuestCardAsync(DeleteGuestCardRequest DeleteGuestCardRequest)
        {
            this.DeleteGuestCardAsync(DeleteGuestCardRequest, null);
        }

        /// <remarks/>
        public void DeleteGuestCardAsync(DeleteGuestCardRequest DeleteGuestCardRequest, object userState)
        {
            if ((this.DeleteGuestCardOperationCompleted == null))
            {
                this.DeleteGuestCardOperationCompleted = new System.Threading.SendOrPostCallback(this.OnDeleteGuestCardOperationCompleted);
            }
            this.InvokeAsync("DeleteGuestCard", new object[] {
                    DeleteGuestCardRequest}, this.DeleteGuestCardOperationCompleted, userState);
        }

        private void OnDeleteGuestCardOperationCompleted(object arg)
        {
            if ((this.DeleteGuestCardCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.DeleteGuestCardCompleted(this, new DeleteGuestCardCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#FetchPreferenceList", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("FetchPreferenceListResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public FetchPreferenceListResponse FetchPreferenceList([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] FetchPreferenceListRequest FetchPreferenceListRequest)
        {
            object[] results = this.Invoke("FetchPreferenceList", new object[] {
                    FetchPreferenceListRequest});
            return ((FetchPreferenceListResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginFetchPreferenceList(FetchPreferenceListRequest FetchPreferenceListRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("FetchPreferenceList", new object[] {
                    FetchPreferenceListRequest}, callback, asyncState);
        }

        /// <remarks/>
        public FetchPreferenceListResponse EndFetchPreferenceList(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((FetchPreferenceListResponse)(results[0]));
        }

        /// <remarks/>
        public void FetchPreferenceListAsync(FetchPreferenceListRequest FetchPreferenceListRequest)
        {
            this.FetchPreferenceListAsync(FetchPreferenceListRequest, null);
        }

        /// <remarks/>
        public void FetchPreferenceListAsync(FetchPreferenceListRequest FetchPreferenceListRequest, object userState)
        {
            if ((this.FetchPreferenceListOperationCompleted == null))
            {
                this.FetchPreferenceListOperationCompleted = new System.Threading.SendOrPostCallback(this.OnFetchPreferenceListOperationCompleted);
            }
            this.InvokeAsync("FetchPreferenceList", new object[] {
                    FetchPreferenceListRequest}, this.FetchPreferenceListOperationCompleted, userState);
        }

        private void OnFetchPreferenceListOperationCompleted(object arg)
        {
            if ((this.FetchPreferenceListCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.FetchPreferenceListCompleted(this, new FetchPreferenceListCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#InsertPreference", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("InsertPreferenceResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public InsertPreferenceResponse InsertPreference([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] InsertPreferenceRequest InsertPreferenceRequest)
        {
            object[] results = this.Invoke("InsertPreference", new object[] {
                    InsertPreferenceRequest});
            return ((InsertPreferenceResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginInsertPreference(InsertPreferenceRequest InsertPreferenceRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("InsertPreference", new object[] {
                    InsertPreferenceRequest}, callback, asyncState);
        }

        /// <remarks/>
        public InsertPreferenceResponse EndInsertPreference(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((InsertPreferenceResponse)(results[0]));
        }

        /// <remarks/>
        public void InsertPreferenceAsync(InsertPreferenceRequest InsertPreferenceRequest)
        {
            this.InsertPreferenceAsync(InsertPreferenceRequest, null);
        }

        /// <remarks/>
        public void InsertPreferenceAsync(InsertPreferenceRequest InsertPreferenceRequest, object userState)
        {
            if ((this.InsertPreferenceOperationCompleted == null))
            {
                this.InsertPreferenceOperationCompleted = new System.Threading.SendOrPostCallback(this.OnInsertPreferenceOperationCompleted);
            }
            this.InvokeAsync("InsertPreference", new object[] {
                    InsertPreferenceRequest}, this.InsertPreferenceOperationCompleted, userState);
        }

        private void OnInsertPreferenceOperationCompleted(object arg)
        {
            if ((this.InsertPreferenceCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.InsertPreferenceCompleted(this, new InsertPreferenceCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#DeletePreference", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("DeletePreferenceResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public DeletePreferenceResponse DeletePreference([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] DeletePreferenceRequest DeletePreferenceRequest)
        {
            object[] results = this.Invoke("DeletePreference", new object[] {
                    DeletePreferenceRequest});
            return ((DeletePreferenceResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginDeletePreference(DeletePreferenceRequest DeletePreferenceRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("DeletePreference", new object[] {
                    DeletePreferenceRequest}, callback, asyncState);
        }

        /// <remarks/>
        public DeletePreferenceResponse EndDeletePreference(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((DeletePreferenceResponse)(results[0]));
        }

        /// <remarks/>
        public void DeletePreferenceAsync(DeletePreferenceRequest DeletePreferenceRequest)
        {
            this.DeletePreferenceAsync(DeletePreferenceRequest, null);
        }

        /// <remarks/>
        public void DeletePreferenceAsync(DeletePreferenceRequest DeletePreferenceRequest, object userState)
        {
            if ((this.DeletePreferenceOperationCompleted == null))
            {
                this.DeletePreferenceOperationCompleted = new System.Threading.SendOrPostCallback(this.OnDeletePreferenceOperationCompleted);
            }
            this.InvokeAsync("DeletePreference", new object[] {
                    DeletePreferenceRequest}, this.DeletePreferenceOperationCompleted, userState);
        }

        private void OnDeletePreferenceOperationCompleted(object arg)
        {
            if ((this.DeletePreferenceCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.DeletePreferenceCompleted(this, new DeletePreferenceCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#FetchCommentList", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("FetchCommentListResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public FetchCommentListResponse FetchCommentList([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] FetchCommentListRequest FetchCommentListRequest)
        {
            object[] results = this.Invoke("FetchCommentList", new object[] {
                    FetchCommentListRequest});
            return ((FetchCommentListResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginFetchCommentList(FetchCommentListRequest FetchCommentListRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("FetchCommentList", new object[] {
                    FetchCommentListRequest}, callback, asyncState);
        }

        /// <remarks/>
        public FetchCommentListResponse EndFetchCommentList(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((FetchCommentListResponse)(results[0]));
        }

        /// <remarks/>
        public void FetchCommentListAsync(FetchCommentListRequest FetchCommentListRequest)
        {
            this.FetchCommentListAsync(FetchCommentListRequest, null);
        }

        /// <remarks/>
        public void FetchCommentListAsync(FetchCommentListRequest FetchCommentListRequest, object userState)
        {
            if ((this.FetchCommentListOperationCompleted == null))
            {
                this.FetchCommentListOperationCompleted = new System.Threading.SendOrPostCallback(this.OnFetchCommentListOperationCompleted);
            }
            this.InvokeAsync("FetchCommentList", new object[] {
                    FetchCommentListRequest}, this.FetchCommentListOperationCompleted, userState);
        }

        private void OnFetchCommentListOperationCompleted(object arg)
        {
            if ((this.FetchCommentListCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.FetchCommentListCompleted(this, new FetchCommentListCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#InsertComment", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("InsertCommentResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public InsertCommentResponse InsertComment([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] InsertCommentRequest InsertCommentRequest)
        {
            object[] results = this.Invoke("InsertComment", new object[] {
                    InsertCommentRequest});
            return ((InsertCommentResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginInsertComment(InsertCommentRequest InsertCommentRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("InsertComment", new object[] {
                    InsertCommentRequest}, callback, asyncState);
        }

        /// <remarks/>
        public InsertCommentResponse EndInsertComment(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((InsertCommentResponse)(results[0]));
        }

        /// <remarks/>
        public void InsertCommentAsync(InsertCommentRequest InsertCommentRequest)
        {
            this.InsertCommentAsync(InsertCommentRequest, null);
        }

        /// <remarks/>
        public void InsertCommentAsync(InsertCommentRequest InsertCommentRequest, object userState)
        {
            if ((this.InsertCommentOperationCompleted == null))
            {
                this.InsertCommentOperationCompleted = new System.Threading.SendOrPostCallback(this.OnInsertCommentOperationCompleted);
            }
            this.InvokeAsync("InsertComment", new object[] {
                    InsertCommentRequest}, this.InsertCommentOperationCompleted, userState);
        }

        private void OnInsertCommentOperationCompleted(object arg)
        {
            if ((this.InsertCommentCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.InsertCommentCompleted(this, new InsertCommentCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#UpdateComment", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("UpdateCommentResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public UpdateCommentResponse UpdateComment([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] UpdateCommentRequest UpdateCommentRequest)
        {
            object[] results = this.Invoke("UpdateComment", new object[] {
                    UpdateCommentRequest});
            return ((UpdateCommentResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginUpdateComment(UpdateCommentRequest UpdateCommentRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("UpdateComment", new object[] {
                    UpdateCommentRequest}, callback, asyncState);
        }

        /// <remarks/>
        public UpdateCommentResponse EndUpdateComment(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((UpdateCommentResponse)(results[0]));
        }

        /// <remarks/>
        public void UpdateCommentAsync(UpdateCommentRequest UpdateCommentRequest)
        {
            this.UpdateCommentAsync(UpdateCommentRequest, null);
        }

        /// <remarks/>
        public void UpdateCommentAsync(UpdateCommentRequest UpdateCommentRequest, object userState)
        {
            if ((this.UpdateCommentOperationCompleted == null))
            {
                this.UpdateCommentOperationCompleted = new System.Threading.SendOrPostCallback(this.OnUpdateCommentOperationCompleted);
            }
            this.InvokeAsync("UpdateComment", new object[] {
                    UpdateCommentRequest}, this.UpdateCommentOperationCompleted, userState);
        }

        private void OnUpdateCommentOperationCompleted(object arg)
        {
            if ((this.UpdateCommentCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.UpdateCommentCompleted(this, new UpdateCommentCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#DeleteComment", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("DeleteCommentResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public DeleteCommentResponse DeleteComment([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] DeleteCommentRequest DeleteCommentRequest)
        {
            object[] results = this.Invoke("DeleteComment", new object[] {
                    DeleteCommentRequest});
            return ((DeleteCommentResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginDeleteComment(DeleteCommentRequest DeleteCommentRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("DeleteComment", new object[] {
                    DeleteCommentRequest}, callback, asyncState);
        }

        /// <remarks/>
        public DeleteCommentResponse EndDeleteComment(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((DeleteCommentResponse)(results[0]));
        }

        /// <remarks/>
        public void DeleteCommentAsync(DeleteCommentRequest DeleteCommentRequest)
        {
            this.DeleteCommentAsync(DeleteCommentRequest, null);
        }

        /// <remarks/>
        public void DeleteCommentAsync(DeleteCommentRequest DeleteCommentRequest, object userState)
        {
            if ((this.DeleteCommentOperationCompleted == null))
            {
                this.DeleteCommentOperationCompleted = new System.Threading.SendOrPostCallback(this.OnDeleteCommentOperationCompleted);
            }
            this.InvokeAsync("DeleteComment", new object[] {
                    DeleteCommentRequest}, this.DeleteCommentOperationCompleted, userState);
        }

        private void OnDeleteCommentOperationCompleted(object arg)
        {
            if ((this.DeleteCommentCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.DeleteCommentCompleted(this, new DeleteCommentCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#FetchPrivacy", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("FetchPrivacyOptionResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public FetchPrivacyOptionResponse FetchPrivacyOption([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] FetchPrivacyOptionRequest FetchPrivacyOptionRequest)
        {
            object[] results = this.Invoke("FetchPrivacyOption", new object[] {
                    FetchPrivacyOptionRequest});
            return ((FetchPrivacyOptionResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginFetchPrivacyOption(FetchPrivacyOptionRequest FetchPrivacyOptionRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("FetchPrivacyOption", new object[] {
                    FetchPrivacyOptionRequest}, callback, asyncState);
        }

        /// <remarks/>
        public FetchPrivacyOptionResponse EndFetchPrivacyOption(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((FetchPrivacyOptionResponse)(results[0]));
        }

        /// <remarks/>
        public void FetchPrivacyOptionAsync(FetchPrivacyOptionRequest FetchPrivacyOptionRequest)
        {
            this.FetchPrivacyOptionAsync(FetchPrivacyOptionRequest, null);
        }

        /// <remarks/>
        public void FetchPrivacyOptionAsync(FetchPrivacyOptionRequest FetchPrivacyOptionRequest, object userState)
        {
            if ((this.FetchPrivacyOptionOperationCompleted == null))
            {
                this.FetchPrivacyOptionOperationCompleted = new System.Threading.SendOrPostCallback(this.OnFetchPrivacyOptionOperationCompleted);
            }
            this.InvokeAsync("FetchPrivacyOption", new object[] {
                    FetchPrivacyOptionRequest}, this.FetchPrivacyOptionOperationCompleted, userState);
        }

        private void OnFetchPrivacyOptionOperationCompleted(object arg)
        {
            if ((this.FetchPrivacyOptionCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.FetchPrivacyOptionCompleted(this, new FetchPrivacyOptionCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#InsertUpdatePrivacy", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("InsertUpdatePrivacyOptionResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public InsertUpdatePrivacyOptionResponse InsertUpdatePrivacyOption([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] InsertUpdatePrivacyOptionRequest InsertUpdatePrivacyOptionRequest)
        {
            object[] results = this.Invoke("InsertUpdatePrivacyOption", new object[] {
                    InsertUpdatePrivacyOptionRequest});
            return ((InsertUpdatePrivacyOptionResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginInsertUpdatePrivacyOption(InsertUpdatePrivacyOptionRequest InsertUpdatePrivacyOptionRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("InsertUpdatePrivacyOption", new object[] {
                    InsertUpdatePrivacyOptionRequest}, callback, asyncState);
        }

        /// <remarks/>
        public InsertUpdatePrivacyOptionResponse EndInsertUpdatePrivacyOption(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((InsertUpdatePrivacyOptionResponse)(results[0]));
        }

        /// <remarks/>
        public void InsertUpdatePrivacyOptionAsync(InsertUpdatePrivacyOptionRequest InsertUpdatePrivacyOptionRequest)
        {
            this.InsertUpdatePrivacyOptionAsync(InsertUpdatePrivacyOptionRequest, null);
        }

        /// <remarks/>
        public void InsertUpdatePrivacyOptionAsync(InsertUpdatePrivacyOptionRequest InsertUpdatePrivacyOptionRequest, object userState)
        {
            if ((this.InsertUpdatePrivacyOptionOperationCompleted == null))
            {
                this.InsertUpdatePrivacyOptionOperationCompleted = new System.Threading.SendOrPostCallback(this.OnInsertUpdatePrivacyOptionOperationCompleted);
            }
            this.InvokeAsync("InsertUpdatePrivacyOption", new object[] {
                    InsertUpdatePrivacyOptionRequest}, this.InsertUpdatePrivacyOptionOperationCompleted, userState);
        }

        private void OnInsertUpdatePrivacyOptionOperationCompleted(object arg)
        {
            if ((this.InsertUpdatePrivacyOptionCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.InsertUpdatePrivacyOptionCompleted(this, new InsertUpdatePrivacyOptionCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#DeletePrivacy", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("DeletePrivacyOptionResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public DeletePrivacyOptionResponse DeletePrivacyOption([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] DeletePrivacyOptionRequest DeletePrivacyOptionRequest)
        {
            object[] results = this.Invoke("DeletePrivacyOption", new object[] {
                    DeletePrivacyOptionRequest});
            return ((DeletePrivacyOptionResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginDeletePrivacyOption(DeletePrivacyOptionRequest DeletePrivacyOptionRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("DeletePrivacyOption", new object[] {
                    DeletePrivacyOptionRequest}, callback, asyncState);
        }

        /// <remarks/>
        public DeletePrivacyOptionResponse EndDeletePrivacyOption(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((DeletePrivacyOptionResponse)(results[0]));
        }

        /// <remarks/>
        public void DeletePrivacyOptionAsync(DeletePrivacyOptionRequest DeletePrivacyOptionRequest)
        {
            this.DeletePrivacyOptionAsync(DeletePrivacyOptionRequest, null);
        }

        /// <remarks/>
        public void DeletePrivacyOptionAsync(DeletePrivacyOptionRequest DeletePrivacyOptionRequest, object userState)
        {
            if ((this.DeletePrivacyOptionOperationCompleted == null))
            {
                this.DeletePrivacyOptionOperationCompleted = new System.Threading.SendOrPostCallback(this.OnDeletePrivacyOptionOperationCompleted);
            }
            this.InvokeAsync("DeletePrivacyOption", new object[] {
                    DeletePrivacyOptionRequest}, this.DeletePrivacyOptionOperationCompleted, userState);
        }

        private void OnDeletePrivacyOptionOperationCompleted(object arg)
        {
            if ((this.DeletePrivacyOptionCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.DeletePrivacyOptionCompleted(this, new DeletePrivacyOptionCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#NameLookup", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("NameLookupResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public NameLookupResponse NameLookup([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] NameLookupRequest NameLookupRequest)
        {
            object[] results = this.Invoke("NameLookup", new object[] {
                    NameLookupRequest});
            return ((NameLookupResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginNameLookup(NameLookupRequest NameLookupRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("NameLookup", new object[] {
                    NameLookupRequest}, callback, asyncState);
        }

        /// <remarks/>
        public NameLookupResponse EndNameLookup(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((NameLookupResponse)(results[0]));
        }

        /// <remarks/>
        public void NameLookupAsync(NameLookupRequest NameLookupRequest)
        {
            this.NameLookupAsync(NameLookupRequest, null);
        }

        /// <remarks/>
        public void NameLookupAsync(NameLookupRequest NameLookupRequest, object userState)
        {
            if ((this.NameLookupOperationCompleted == null))
            {
                this.NameLookupOperationCompleted = new System.Threading.SendOrPostCallback(this.OnNameLookupOperationCompleted);
            }
            this.InvokeAsync("NameLookup", new object[] {
                    NameLookupRequest}, this.NameLookupOperationCompleted, userState);
        }

        private void OnNameLookupOperationCompleted(object arg)
        {
            if ((this.NameLookupCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.NameLookupCompleted(this, new NameLookupCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#FetchNameUDFs", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("FetchNameUDFsResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public FetchNameUDFsResponse FetchNameUDFs([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] FetchNameUDFsRequest FetchNameUDFsRequest)
        {
            object[] results = this.Invoke("FetchNameUDFs", new object[] {
                    FetchNameUDFsRequest});
            return ((FetchNameUDFsResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginFetchNameUDFs(FetchNameUDFsRequest FetchNameUDFsRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("FetchNameUDFs", new object[] {
                    FetchNameUDFsRequest}, callback, asyncState);
        }

        /// <remarks/>
        public FetchNameUDFsResponse EndFetchNameUDFs(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((FetchNameUDFsResponse)(results[0]));
        }

        /// <remarks/>
        public void FetchNameUDFsAsync(FetchNameUDFsRequest FetchNameUDFsRequest)
        {
            this.FetchNameUDFsAsync(FetchNameUDFsRequest, null);
        }

        /// <remarks/>
        public void FetchNameUDFsAsync(FetchNameUDFsRequest FetchNameUDFsRequest, object userState)
        {
            if ((this.FetchNameUDFsOperationCompleted == null))
            {
                this.FetchNameUDFsOperationCompleted = new System.Threading.SendOrPostCallback(this.OnFetchNameUDFsOperationCompleted);
            }
            this.InvokeAsync("FetchNameUDFs", new object[] {
                    FetchNameUDFsRequest}, this.FetchNameUDFsOperationCompleted, userState);
        }

        private void OnFetchNameUDFsOperationCompleted(object arg)
        {
            if ((this.FetchNameUDFsCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.FetchNameUDFsCompleted(this, new FetchNameUDFsCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#InsertUpdateNameUDFs", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("InsertUpdateNameUDFsResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public InsertUpdateNameUDFsResponse InsertUpdateNameUDFs([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] InsertUpdateNameUDFsRequest InsertUpdateNameUDFsRequest)
        {
            object[] results = this.Invoke("InsertUpdateNameUDFs", new object[] {
                    InsertUpdateNameUDFsRequest});
            return ((InsertUpdateNameUDFsResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginInsertUpdateNameUDFs(InsertUpdateNameUDFsRequest InsertUpdateNameUDFsRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("InsertUpdateNameUDFs", new object[] {
                    InsertUpdateNameUDFsRequest}, callback, asyncState);
        }

        /// <remarks/>
        public InsertUpdateNameUDFsResponse EndInsertUpdateNameUDFs(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((InsertUpdateNameUDFsResponse)(results[0]));
        }

        /// <remarks/>
        public void InsertUpdateNameUDFsAsync(InsertUpdateNameUDFsRequest InsertUpdateNameUDFsRequest)
        {
            this.InsertUpdateNameUDFsAsync(InsertUpdateNameUDFsRequest, null);
        }

        /// <remarks/>
        public void InsertUpdateNameUDFsAsync(InsertUpdateNameUDFsRequest InsertUpdateNameUDFsRequest, object userState)
        {
            if ((this.InsertUpdateNameUDFsOperationCompleted == null))
            {
                this.InsertUpdateNameUDFsOperationCompleted = new System.Threading.SendOrPostCallback(this.OnInsertUpdateNameUDFsOperationCompleted);
            }
            this.InvokeAsync("InsertUpdateNameUDFs", new object[] {
                    InsertUpdateNameUDFsRequest}, this.InsertUpdateNameUDFsOperationCompleted, userState);
        }

        private void OnInsertUpdateNameUDFsOperationCompleted(object arg)
        {
            if ((this.InsertUpdateNameUDFsCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.InsertUpdateNameUDFsCompleted(this, new InsertUpdateNameUDFsCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#TravelAgentLookup", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("TravelAgentLookupResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public TravelAgentLookupResponse TravelAgentLookup([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] TravelAgentLookupRequest TravelAgentLookupRequest)
        {
            object[] results = this.Invoke("TravelAgentLookup", new object[] {
                    TravelAgentLookupRequest});
            return ((TravelAgentLookupResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginTravelAgentLookup(TravelAgentLookupRequest TravelAgentLookupRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("TravelAgentLookup", new object[] {
                    TravelAgentLookupRequest}, callback, asyncState);
        }

        /// <remarks/>
        public TravelAgentLookupResponse EndTravelAgentLookup(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((TravelAgentLookupResponse)(results[0]));
        }

        /// <remarks/>
        public void TravelAgentLookupAsync(TravelAgentLookupRequest TravelAgentLookupRequest)
        {
            this.TravelAgentLookupAsync(TravelAgentLookupRequest, null);
        }

        /// <remarks/>
        public void TravelAgentLookupAsync(TravelAgentLookupRequest TravelAgentLookupRequest, object userState)
        {
            if ((this.TravelAgentLookupOperationCompleted == null))
            {
                this.TravelAgentLookupOperationCompleted = new System.Threading.SendOrPostCallback(this.OnTravelAgentLookupOperationCompleted);
            }
            this.InvokeAsync("TravelAgentLookup", new object[] {
                    TravelAgentLookupRequest}, this.TravelAgentLookupOperationCompleted, userState);
        }

        private void OnTravelAgentLookupOperationCompleted(object arg)
        {
            if ((this.TravelAgentLookupCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.TravelAgentLookupCompleted(this, new TravelAgentLookupCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#FetchProfile", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("FetchProfileResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public FetchProfileResponse FetchProfile([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] FetchProfileRequest FetchProfileRequest)
        {
            object[] results = this.Invoke("FetchProfile", new object[] {
                    FetchProfileRequest});
            return ((FetchProfileResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginFetchProfile(FetchProfileRequest FetchProfileRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("FetchProfile", new object[] {
                    FetchProfileRequest}, callback, asyncState);
        }

        /// <remarks/>
        public FetchProfileResponse EndFetchProfile(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((FetchProfileResponse)(results[0]));
        }

        /// <remarks/>
        public void FetchProfileAsync(FetchProfileRequest FetchProfileRequest)
        {
            this.FetchProfileAsync(FetchProfileRequest, null);
        }

        /// <remarks/>
        public void FetchProfileAsync(FetchProfileRequest FetchProfileRequest, object userState)
        {
            if ((this.FetchProfileOperationCompleted == null))
            {
                this.FetchProfileOperationCompleted = new System.Threading.SendOrPostCallback(this.OnFetchProfileOperationCompleted);
            }
            this.InvokeAsync("FetchProfile", new object[] {
                    FetchProfileRequest}, this.FetchProfileOperationCompleted, userState);
        }

        private void OnFetchProfileOperationCompleted(object arg)
        {
            if ((this.FetchProfileCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.FetchProfileCompleted(this, new FetchProfileCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#InsertClaim", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("InsertClaimResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public InsertClaimResponse InsertClaim([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] InsertClaimRequest InsertClaimRequest)
        {
            object[] results = this.Invoke("InsertClaim", new object[] {
                    InsertClaimRequest});
            return ((InsertClaimResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginInsertClaim(InsertClaimRequest InsertClaimRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("InsertClaim", new object[] {
                    InsertClaimRequest}, callback, asyncState);
        }

        /// <remarks/>
        public InsertClaimResponse EndInsertClaim(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((InsertClaimResponse)(results[0]));
        }

        /// <remarks/>
        public void InsertClaimAsync(InsertClaimRequest InsertClaimRequest)
        {
            this.InsertClaimAsync(InsertClaimRequest, null);
        }

        /// <remarks/>
        public void InsertClaimAsync(InsertClaimRequest InsertClaimRequest, object userState)
        {
            if ((this.InsertClaimOperationCompleted == null))
            {
                this.InsertClaimOperationCompleted = new System.Threading.SendOrPostCallback(this.OnInsertClaimOperationCompleted);
            }
            this.InvokeAsync("InsertClaim", new object[] {
                    InsertClaimRequest}, this.InsertClaimOperationCompleted, userState);
        }

        private void OnInsertClaimOperationCompleted(object arg)
        {
            if ((this.InsertClaimCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.InsertClaimCompleted(this, new InsertClaimCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#FetchClaimsStatus", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("FetchClaimsStatusResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public FetchClaimsStatusResponse FetchClaimsStatus([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] FetchClaimsStatusRequest FetchClaimsStatusRequest)
        {
            object[] results = this.Invoke("FetchClaimsStatus", new object[] {
                    FetchClaimsStatusRequest});
            return ((FetchClaimsStatusResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginFetchClaimsStatus(FetchClaimsStatusRequest FetchClaimsStatusRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("FetchClaimsStatus", new object[] {
                    FetchClaimsStatusRequest}, callback, asyncState);
        }

        /// <remarks/>
        public FetchClaimsStatusResponse EndFetchClaimsStatus(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((FetchClaimsStatusResponse)(results[0]));
        }

        /// <remarks/>
        public void FetchClaimsStatusAsync(FetchClaimsStatusRequest FetchClaimsStatusRequest)
        {
            this.FetchClaimsStatusAsync(FetchClaimsStatusRequest, null);
        }

        /// <remarks/>
        public void FetchClaimsStatusAsync(FetchClaimsStatusRequest FetchClaimsStatusRequest, object userState)
        {
            if ((this.FetchClaimsStatusOperationCompleted == null))
            {
                this.FetchClaimsStatusOperationCompleted = new System.Threading.SendOrPostCallback(this.OnFetchClaimsStatusOperationCompleted);
            }
            this.InvokeAsync("FetchClaimsStatus", new object[] {
                    FetchClaimsStatusRequest}, this.FetchClaimsStatusOperationCompleted, userState);
        }

        private void OnFetchClaimsStatusOperationCompleted(object arg)
        {
            if ((this.FetchClaimsStatusCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.FetchClaimsStatusCompleted(this, new FetchClaimsStatusCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#UpdateClaim", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("UpdateClaimResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public UpdateClaimResponse UpdateClaim([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] UpdateClaimRequest UpdateClaimRequest)
        {
            object[] results = this.Invoke("UpdateClaim", new object[] {
                    UpdateClaimRequest});
            return ((UpdateClaimResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginUpdateClaim(UpdateClaimRequest UpdateClaimRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("UpdateClaim", new object[] {
                    UpdateClaimRequest}, callback, asyncState);
        }

        /// <remarks/>
        public UpdateClaimResponse EndUpdateClaim(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((UpdateClaimResponse)(results[0]));
        }

        /// <remarks/>
        public void UpdateClaimAsync(UpdateClaimRequest UpdateClaimRequest)
        {
            this.UpdateClaimAsync(UpdateClaimRequest, null);
        }

        /// <remarks/>
        public void UpdateClaimAsync(UpdateClaimRequest UpdateClaimRequest, object userState)
        {
            if ((this.UpdateClaimOperationCompleted == null))
            {
                this.UpdateClaimOperationCompleted = new System.Threading.SendOrPostCallback(this.OnUpdateClaimOperationCompleted);
            }
            this.InvokeAsync("UpdateClaim", new object[] {
                    UpdateClaimRequest}, this.UpdateClaimOperationCompleted, userState);
        }

        private void OnUpdateClaimOperationCompleted(object arg)
        {
            if ((this.UpdateClaimCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.UpdateClaimCompleted(this, new UpdateClaimCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#FetchSubscription", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("FetchSubscriptionResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public FetchSubscriptionResponse FetchSubscription([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] FetchSubscriptionRequest FetchSubscriptionRequest)
        {
            object[] results = this.Invoke("FetchSubscription", new object[] {
                    FetchSubscriptionRequest});
            return ((FetchSubscriptionResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginFetchSubscription(FetchSubscriptionRequest FetchSubscriptionRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("FetchSubscription", new object[] {
                    FetchSubscriptionRequest}, callback, asyncState);
        }

        /// <remarks/>
        public FetchSubscriptionResponse EndFetchSubscription(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((FetchSubscriptionResponse)(results[0]));
        }

        /// <remarks/>
        public void FetchSubscriptionAsync(FetchSubscriptionRequest FetchSubscriptionRequest)
        {
            this.FetchSubscriptionAsync(FetchSubscriptionRequest, null);
        }

        /// <remarks/>
        public void FetchSubscriptionAsync(FetchSubscriptionRequest FetchSubscriptionRequest, object userState)
        {
            if ((this.FetchSubscriptionOperationCompleted == null))
            {
                this.FetchSubscriptionOperationCompleted = new System.Threading.SendOrPostCallback(this.OnFetchSubscriptionOperationCompleted);
            }
            this.InvokeAsync("FetchSubscription", new object[] {
                    FetchSubscriptionRequest}, this.FetchSubscriptionOperationCompleted, userState);
        }

        private void OnFetchSubscriptionOperationCompleted(object arg)
        {
            if ((this.FetchSubscriptionCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.FetchSubscriptionCompleted(this, new FetchSubscriptionCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("OGHeaderValue", Direction = System.Web.Services.Protocols.SoapHeaderDirection.InOut)]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://webservices.micros.com/ows/5.1/Name.wsdl#FetchProfileBenefits", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("FetchProfileBenefitsResponse", Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
        public FetchProfileBenefitsResponse FetchProfileBenefits([System.Xml.Serialization.XmlElementAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")] FetchProfileBenefitsRequest FetchProfileBenefitsRequest)
        {
            object[] results = this.Invoke("FetchProfileBenefits", new object[] {
                    FetchProfileBenefitsRequest});
            return ((FetchProfileBenefitsResponse)(results[0]));
        }

        /// <remarks/>
        public System.IAsyncResult BeginFetchProfileBenefits(FetchProfileBenefitsRequest FetchProfileBenefitsRequest, System.AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("FetchProfileBenefits", new object[] {
                    FetchProfileBenefitsRequest}, callback, asyncState);
        }

        /// <remarks/>
        public FetchProfileBenefitsResponse EndFetchProfileBenefits(System.IAsyncResult asyncResult)
        {
            object[] results = this.EndInvoke(asyncResult);
            return ((FetchProfileBenefitsResponse)(results[0]));
        }

        /// <remarks/>
        public void FetchProfileBenefitsAsync(FetchProfileBenefitsRequest FetchProfileBenefitsRequest)
        {
            this.FetchProfileBenefitsAsync(FetchProfileBenefitsRequest, null);
        }

        /// <remarks/>
        public void FetchProfileBenefitsAsync(FetchProfileBenefitsRequest FetchProfileBenefitsRequest, object userState)
        {
            if ((this.FetchProfileBenefitsOperationCompleted == null))
            {
                this.FetchProfileBenefitsOperationCompleted = new System.Threading.SendOrPostCallback(this.OnFetchProfileBenefitsOperationCompleted);
            }
            this.InvokeAsync("FetchProfileBenefits", new object[] {
                    FetchProfileBenefitsRequest}, this.FetchProfileBenefitsOperationCompleted, userState);
        }

        private void OnFetchProfileBenefitsOperationCompleted(object arg)
        {
            if ((this.FetchProfileBenefitsCompleted != null))
            {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.FetchProfileBenefitsCompleted(this, new FetchProfileBenefitsCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        public new void CancelAsync(object userState)
        {
            base.CancelAsync(userState);
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Core/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://webservices.micros.com/og/4.3/Core/", IsNullable = false)]
    public partial class OGHeader : System.Web.Services.Protocols.SoapHeader
    {

        private EndPoint originField;

        private EndPoint destinationField;

        private EndPoint[] intermediariesField;

        private OGHeaderAuthentication authenticationField;

        private string transactionIDField;

        private string authTokenField;

        private System.DateTime timeStampField;

        private bool timeStampFieldSpecified;

        private string primaryLangIDField;

        private System.Xml.XmlAttribute[] anyAttrField;

        /// <remarks/>
        public EndPoint Origin
        {
            get
            {
                return this.originField;
            }
            set
            {
                this.originField = value;
            }
        }

        /// <remarks/>
        public EndPoint Destination
        {
            get
            {
                return this.destinationField;
            }
            set
            {
                this.destinationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public EndPoint[] Intermediaries
        {
            get
            {
                return this.intermediariesField;
            }
            set
            {
                this.intermediariesField = value;
            }
        }

        /// <remarks/>
        public OGHeaderAuthentication Authentication
        {
            get
            {
                return this.authenticationField;
            }
            set
            {
                this.authenticationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string transactionID
        {
            get
            {
                return this.transactionIDField;
            }
            set
            {
                this.transactionIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string authToken
        {
            get
            {
                return this.authTokenField;
            }
            set
            {
                this.authTokenField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime timeStamp
        {
            get
            {
                return this.timeStampField;
            }
            set
            {
                this.timeStampField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool timeStampSpecified
        {
            get
            {
                return this.timeStampFieldSpecified;
            }
            set
            {
                this.timeStampFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string primaryLangID
        {
            get
            {
                return this.primaryLangIDField;
            }
            set
            {
                this.primaryLangIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAnyAttributeAttribute()]
        public System.Xml.XmlAttribute[] AnyAttr
        {
            get
            {
                return this.anyAttrField;
            }
            set
            {
                this.anyAttrField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Core/")]
    public partial class EndPoint : SystemID
    {

        private string systemTypeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string systemType
        {
            get
            {
                return this.systemTypeField;
            }
            set
            {
                this.systemTypeField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(EndPoint))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Core/")]
    public partial class SystemID
    {

        private string entityIDField;

        private string organizationIDField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string entityID
        {
            get
            {
                return this.entityIDField;
            }
            set
            {
                this.entityIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string organizationID
        {
            get
            {
                return this.organizationIDField;
            }
            set
            {
                this.organizationIDField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/HotelCommon/")]
    public partial class HotelReference
    {

        private string chainCodeField;

        private string hotelCodeField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string chainCode
        {
            get
            {
                return this.chainCodeField;
            }
            set
            {
                this.chainCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string hotelCode
        {
            get
            {
                return this.hotelCodeField;
            }
            set
            {
                this.hotelCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Membership/")]
    public partial class ECertificate
    {

        private UniqueID certificateIDField;

        private string voucherNumberField;

        private string certificateNumberField;

        private string certificateCodeField;

        private HotelReference hotelReferenceField;

        private string membershipTypeField;

        private string awardTypeField;

        private string promotionCodeField;

        private string shortDescriptionField;

        private string longDescriptionField;

        private System.DateTime expirationDateField;

        private bool expirationDateFieldSpecified;

        private string reservationCertificateYNField;

        private Amount certificateValueField;

        private Amount certificateCostField;

        private string certificateLabelField;

        private UniqueID nameIDField;

        private string consumedAtField;

        private string consumerLastNameField;

        private string consumerFirstNameField;

        private string consumerMiddleNameField;

        private string consumerEmailField;

        private System.DateTime consumptionDateField;

        private bool consumptionDateFieldSpecified;

        private HotelReference consumedHotelReferenceField;

        private UniqueID consumptionRefNoField;

        private string consumptionRefTypeField;

        private UniqueID consumptionLegNoField;

        private string userNotesField;

        private string statusField;

        private string printStatusField;

        private UniqueID membershipAwardIDField;

        private string issueTypeField;

        private string issueSourceField;

        private double awardPointsField;

        private bool awardPointsFieldSpecified;

        /// <remarks/>
        public UniqueID CertificateID
        {
            get
            {
                return this.certificateIDField;
            }
            set
            {
                this.certificateIDField = value;
            }
        }

        /// <remarks/>
        public string VoucherNumber
        {
            get
            {
                return this.voucherNumberField;
            }
            set
            {
                this.voucherNumberField = value;
            }
        }

        /// <remarks/>
        public string CertificateNumber
        {
            get
            {
                return this.certificateNumberField;
            }
            set
            {
                this.certificateNumberField = value;
            }
        }

        /// <remarks/>
        public string CertificateCode
        {
            get
            {
                return this.certificateCodeField;
            }
            set
            {
                this.certificateCodeField = value;
            }
        }

        /// <remarks/>
        public HotelReference HotelReference
        {
            get
            {
                return this.hotelReferenceField;
            }
            set
            {
                this.hotelReferenceField = value;
            }
        }

        /// <remarks/>
        public string MembershipType
        {
            get
            {
                return this.membershipTypeField;
            }
            set
            {
                this.membershipTypeField = value;
            }
        }

        /// <remarks/>
        public string AwardType
        {
            get
            {
                return this.awardTypeField;
            }
            set
            {
                this.awardTypeField = value;
            }
        }

        /// <remarks/>
        public string PromotionCode
        {
            get
            {
                return this.promotionCodeField;
            }
            set
            {
                this.promotionCodeField = value;
            }
        }

        /// <remarks/>
        public string ShortDescription
        {
            get
            {
                return this.shortDescriptionField;
            }
            set
            {
                this.shortDescriptionField = value;
            }
        }

        /// <remarks/>
        public string LongDescription
        {
            get
            {
                return this.longDescriptionField;
            }
            set
            {
                this.longDescriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime ExpirationDate
        {
            get
            {
                return this.expirationDateField;
            }
            set
            {
                this.expirationDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ExpirationDateSpecified
        {
            get
            {
                return this.expirationDateFieldSpecified;
            }
            set
            {
                this.expirationDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string ReservationCertificateYN
        {
            get
            {
                return this.reservationCertificateYNField;
            }
            set
            {
                this.reservationCertificateYNField = value;
            }
        }

        /// <remarks/>
        public Amount CertificateValue
        {
            get
            {
                return this.certificateValueField;
            }
            set
            {
                this.certificateValueField = value;
            }
        }

        /// <remarks/>
        public Amount CertificateCost
        {
            get
            {
                return this.certificateCostField;
            }
            set
            {
                this.certificateCostField = value;
            }
        }

        /// <remarks/>
        public string CertificateLabel
        {
            get
            {
                return this.certificateLabelField;
            }
            set
            {
                this.certificateLabelField = value;
            }
        }

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }

        /// <remarks/>
        public string ConsumedAt
        {
            get
            {
                return this.consumedAtField;
            }
            set
            {
                this.consumedAtField = value;
            }
        }

        /// <remarks/>
        public string ConsumerLastName
        {
            get
            {
                return this.consumerLastNameField;
            }
            set
            {
                this.consumerLastNameField = value;
            }
        }

        /// <remarks/>
        public string ConsumerFirstName
        {
            get
            {
                return this.consumerFirstNameField;
            }
            set
            {
                this.consumerFirstNameField = value;
            }
        }

        /// <remarks/>
        public string ConsumerMiddleName
        {
            get
            {
                return this.consumerMiddleNameField;
            }
            set
            {
                this.consumerMiddleNameField = value;
            }
        }

        /// <remarks/>
        public string ConsumerEmail
        {
            get
            {
                return this.consumerEmailField;
            }
            set
            {
                this.consumerEmailField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime ConsumptionDate
        {
            get
            {
                return this.consumptionDateField;
            }
            set
            {
                this.consumptionDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ConsumptionDateSpecified
        {
            get
            {
                return this.consumptionDateFieldSpecified;
            }
            set
            {
                this.consumptionDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public HotelReference ConsumedHotelReference
        {
            get
            {
                return this.consumedHotelReferenceField;
            }
            set
            {
                this.consumedHotelReferenceField = value;
            }
        }

        /// <remarks/>
        public UniqueID ConsumptionRefNo
        {
            get
            {
                return this.consumptionRefNoField;
            }
            set
            {
                this.consumptionRefNoField = value;
            }
        }

        /// <remarks/>
        public string ConsumptionRefType
        {
            get
            {
                return this.consumptionRefTypeField;
            }
            set
            {
                this.consumptionRefTypeField = value;
            }
        }

        /// <remarks/>
        public UniqueID ConsumptionLegNo
        {
            get
            {
                return this.consumptionLegNoField;
            }
            set
            {
                this.consumptionLegNoField = value;
            }
        }

        /// <remarks/>
        public string UserNotes
        {
            get
            {
                return this.userNotesField;
            }
            set
            {
                this.userNotesField = value;
            }
        }

        /// <remarks/>
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        public string PrintStatus
        {
            get
            {
                return this.printStatusField;
            }
            set
            {
                this.printStatusField = value;
            }
        }

        /// <remarks/>
        public UniqueID MembershipAwardID
        {
            get
            {
                return this.membershipAwardIDField;
            }
            set
            {
                this.membershipAwardIDField = value;
            }
        }

        /// <remarks/>
        public string IssueType
        {
            get
            {
                return this.issueTypeField;
            }
            set
            {
                this.issueTypeField = value;
            }
        }

        /// <remarks/>
        public string IssueSource
        {
            get
            {
                return this.issueSourceField;
            }
            set
            {
                this.issueSourceField = value;
            }
        }

        /// <remarks/>
        public double AwardPoints
        {
            get
            {
                return this.awardPointsField;
            }
            set
            {
                this.awardPointsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AwardPointsSpecified
        {
            get
            {
                return this.awardPointsFieldSpecified;
            }
            set
            {
                this.awardPointsFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Common/")]
    public partial class UniqueID
    {

        private UniqueIDType typeField;

        private string sourceField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public UniqueIDType type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string source
        {
            get
            {
                return this.sourceField;
            }
            set
            {
                this.sourceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Common/")]
    public enum UniqueIDType
    {

        /// <remarks/>
        EXTERNAL,

        /// <remarks/>
        INTERNAL,

        /// <remarks/>
        CANCELLATIONEXTERNAL,

        /// <remarks/>
        CANCELLATIONINTERNAL,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Common/")]
    public partial class Amount
    {

        private string currencyCodeField;

        private short decimalsField;

        private bool decimalsFieldSpecified;

        private string currencyTextField;

        private double valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string currencyCode
        {
            get
            {
                return this.currencyCodeField;
            }
            set
            {
                this.currencyCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public short decimals
        {
            get
            {
                return this.decimalsField;
            }
            set
            {
                this.decimalsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool decimalsSpecified
        {
            get
            {
                return this.decimalsFieldSpecified;
            }
            set
            {
                this.decimalsFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string currencyText
        {
            get
            {
                return this.currencyTextField;
            }
            set
            {
                this.currencyTextField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public double Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/HotelCommon/")]
    public partial class TimeSpan
    {

        private System.DateTime startDateField;

        private object itemField;

        /// <remarks/>
        public System.DateTime StartDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Duration", typeof(string), DataType = "duration")]
        [System.Xml.Serialization.XmlElementAttribute("EndDate", typeof(System.DateTime))]
        public object Item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Membership/")]
    public partial class Promotion
    {

        private string codeField;

        private TimeSpan datesField;

        private string nameField;

        private PromotionSubscriptionStatusType statusField;

        private bool statusFieldSpecified;

        private PromotionIssueType issueTypeField;

        private bool issueTypeFieldSpecified;

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public TimeSpan Dates
        {
            get
            {
                return this.datesField;
            }
            set
            {
                this.datesField = value;
            }
        }

        /// <remarks/>
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public PromotionSubscriptionStatusType Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool StatusSpecified
        {
            get
            {
                return this.statusFieldSpecified;
            }
            set
            {
                this.statusFieldSpecified = value;
            }
        }

        /// <remarks/>
        public PromotionIssueType IssueType
        {
            get
            {
                return this.issueTypeField;
            }
            set
            {
                this.issueTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IssueTypeSpecified
        {
            get
            {
                return this.issueTypeFieldSpecified;
            }
            set
            {
                this.issueTypeFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Membership/")]
    public enum PromotionSubscriptionStatusType
    {

        /// <remarks/>
        ACTIVE,

        /// <remarks/>
        INACTIVE,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Membership/")]
    public enum PromotionIssueType
    {

        /// <remarks/>
        ASSIGNED,

        /// <remarks/>
        OPTIN,

        /// <remarks/>
        PURCHASED,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Membership/")]
    public partial class ProfilePromotion
    {

        private Promotion promotionField;

        private int membershipIDField;

        private string membershipTypeField;

        private string membershipNumberField;

        /// <remarks/>
        public Promotion Promotion
        {
            get
            {
                return this.promotionField;
            }
            set
            {
                this.promotionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int membershipID
        {
            get
            {
                return this.membershipIDField;
            }
            set
            {
                this.membershipIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string membershipType
        {
            get
            {
                return this.membershipTypeField;
            }
            set
            {
                this.membershipTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string membershipNumber
        {
            get
            {
                return this.membershipNumberField;
            }
            set
            {
                this.membershipNumberField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class FetchProfileBenefitsRequest
    {

        private UniqueID nameIDField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class FetchSubscriptionRequest
    {

        private UniqueID nameIDField;

        private string externalSystemField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }

        /// <remarks/>
        public string ExternalSystem
        {
            get
            {
                return this.externalSystemField;
            }
            set
            {
                this.externalSystemField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class UpdateClaimResponse
    {

        private ResultStatus resultField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(GDSResultStatus))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Common/")]
    public partial class ResultStatus
    {

        private Text[] textField;

        private IDPair[] iDsField;

        private ResultStatusFlag resultStatusFlagField;

        private bool resultStatusFlagFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("TextElement", IsNullable = false)]
        public Text[] Text
        {
            get
            {
                return this.textField;
            }
            set
            {
                this.textField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public IDPair[] IDs
        {
            get
            {
                return this.iDsField;
            }
            set
            {
                this.iDsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ResultStatusFlag resultStatusFlag
        {
            get
            {
                return this.resultStatusFlagField;
            }
            set
            {
                this.resultStatusFlagField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool resultStatusFlagSpecified
        {
            get
            {
                return this.resultStatusFlagFieldSpecified;
            }
            set
            {
                this.resultStatusFlagFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(TextElement))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Common/")]
    public partial class Text
    {

        private bool formattedField;

        private bool formattedFieldSpecified;

        private string languageField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool formatted
        {
            get
            {
                return this.formattedField;
            }
            set
            {
                this.formattedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool formattedSpecified
        {
            get
            {
                return this.formattedFieldSpecified;
            }
            set
            {
                this.formattedFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "language")]
        public string language
        {
            get
            {
                return this.languageField;
            }
            set
            {
                this.languageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute(DataType = "normalizedString")]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Common/")]
    public partial class TextElement : Text
    {
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Common/")]
    public partial class IDPair
    {

        private string idTypeField;

        private long operaIdField;

        private bool operaIdFieldSpecified;

        private string externalIdField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string idType
        {
            get
            {
                return this.idTypeField;
            }
            set
            {
                this.idTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public long operaId
        {
            get
            {
                return this.operaIdField;
            }
            set
            {
                this.operaIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool operaIdSpecified
        {
            get
            {
                return this.operaIdFieldSpecified;
            }
            set
            {
                this.operaIdFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string externalId
        {
            get
            {
                return this.externalIdField;
            }
            set
            {
                this.externalIdField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Common/")]
    public enum ResultStatusFlag
    {

        /// <remarks/>
        FAIL,

        /// <remarks/>
        SUCCESS,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/HotelCommon/")]
    public partial class GDSResultStatus : ResultStatus
    {

        private GDSError gDSErrorField;

        /// <remarks/>
        public GDSError GDSError
        {
            get
            {
                return this.gDSErrorField;
            }
            set
            {
                this.gDSErrorField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/HotelCommon/")]
    public partial class GDSError
    {

        private string errorCodeField;

        private string elementIdField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string errorCode
        {
            get
            {
                return this.errorCodeField;
            }
            set
            {
                this.errorCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string elementId
        {
            get
            {
                return this.elementIdField;
            }
            set
            {
                this.elementIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class UpdateClaimRequest
    {

        private UniqueID claimIDField;

        private string commentsField;

        /// <remarks/>
        public UniqueID ClaimID
        {
            get
            {
                return this.claimIDField;
            }
            set
            {
                this.claimIDField = value;
            }
        }

        /// <remarks/>
        public string Comments
        {
            get
            {
                return this.commentsField;
            }
            set
            {
                this.commentsField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class FetchClaimsStatusResponse
    {

        private ResultStatus resultField;

        private Claim[] claimsListField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("ClaimsInfo", Namespace = "http://webservices.micros.com/og/4.3/Common/", IsNullable = false)]
        public Claim[] ClaimsList
        {
            get
            {
                return this.claimsListField;
            }
            set
            {
                this.claimsListField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Common/")]
    public partial class Claim
    {

        private double claimIdField;

        private bool claimIdFieldSpecified;

        private System.DateTime claimDateField;

        private bool claimDateFieldSpecified;

        private string claimTypeField;

        private string recordTypeField;

        private string resortField;

        private string claimStatusField;

        private string finalStatusField;

        private string claimInformationField;

        private System.DateTime closeDateField;

        private bool closeDateFieldSpecified;

        private string claimSourceField;

        private string callerNameField;

        private string ownerField;

        private UniqueID nameIDField;

        private UniqueID membershipIDField;

        private string membershipTypeField;

        private string membershipCardNumberField;

        private string pmsReferenceNumberField;

        private string orsReferenceNumberField;

        private System.DateTime startDateField;

        private bool startDateFieldSpecified;

        private System.DateTime endDateField;

        private bool endDateFieldSpecified;

        private ClaimComment[] claimCommentsField;

        /// <remarks/>
        public double ClaimId
        {
            get
            {
                return this.claimIdField;
            }
            set
            {
                this.claimIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ClaimIdSpecified
        {
            get
            {
                return this.claimIdFieldSpecified;
            }
            set
            {
                this.claimIdFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime ClaimDate
        {
            get
            {
                return this.claimDateField;
            }
            set
            {
                this.claimDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ClaimDateSpecified
        {
            get
            {
                return this.claimDateFieldSpecified;
            }
            set
            {
                this.claimDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string ClaimType
        {
            get
            {
                return this.claimTypeField;
            }
            set
            {
                this.claimTypeField = value;
            }
        }

        /// <remarks/>
        public string RecordType
        {
            get
            {
                return this.recordTypeField;
            }
            set
            {
                this.recordTypeField = value;
            }
        }

        /// <remarks/>
        public string Resort
        {
            get
            {
                return this.resortField;
            }
            set
            {
                this.resortField = value;
            }
        }

        /// <remarks/>
        public string ClaimStatus
        {
            get
            {
                return this.claimStatusField;
            }
            set
            {
                this.claimStatusField = value;
            }
        }

        /// <remarks/>
        public string FinalStatus
        {
            get
            {
                return this.finalStatusField;
            }
            set
            {
                this.finalStatusField = value;
            }
        }

        /// <remarks/>
        public string ClaimInformation
        {
            get
            {
                return this.claimInformationField;
            }
            set
            {
                this.claimInformationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime CloseDate
        {
            get
            {
                return this.closeDateField;
            }
            set
            {
                this.closeDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CloseDateSpecified
        {
            get
            {
                return this.closeDateFieldSpecified;
            }
            set
            {
                this.closeDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string ClaimSource
        {
            get
            {
                return this.claimSourceField;
            }
            set
            {
                this.claimSourceField = value;
            }
        }

        /// <remarks/>
        public string CallerName
        {
            get
            {
                return this.callerNameField;
            }
            set
            {
                this.callerNameField = value;
            }
        }

        /// <remarks/>
        public string Owner
        {
            get
            {
                return this.ownerField;
            }
            set
            {
                this.ownerField = value;
            }
        }

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }

        /// <remarks/>
        public UniqueID MembershipID
        {
            get
            {
                return this.membershipIDField;
            }
            set
            {
                this.membershipIDField = value;
            }
        }

        /// <remarks/>
        public string MembershipType
        {
            get
            {
                return this.membershipTypeField;
            }
            set
            {
                this.membershipTypeField = value;
            }
        }

        /// <remarks/>
        public string MembershipCardNumber
        {
            get
            {
                return this.membershipCardNumberField;
            }
            set
            {
                this.membershipCardNumberField = value;
            }
        }

        /// <remarks/>
        public string PmsReferenceNumber
        {
            get
            {
                return this.pmsReferenceNumberField;
            }
            set
            {
                this.pmsReferenceNumberField = value;
            }
        }

        /// <remarks/>
        public string OrsReferenceNumber
        {
            get
            {
                return this.orsReferenceNumberField;
            }
            set
            {
                this.orsReferenceNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime StartDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool StartDateSpecified
        {
            get
            {
                return this.startDateFieldSpecified;
            }
            set
            {
                this.startDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime EndDate
        {
            get
            {
                return this.endDateField;
            }
            set
            {
                this.endDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EndDateSpecified
        {
            get
            {
                return this.endDateFieldSpecified;
            }
            set
            {
                this.endDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("ClaimCommentsInfo", IsNullable = false)]
        public ClaimComment[] ClaimComments
        {
            get
            {
                return this.claimCommentsField;
            }
            set
            {
                this.claimCommentsField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Common/")]
    public partial class ClaimComment
    {

        private int sequenceField;

        private bool sequenceFieldSpecified;

        private System.DateTime claimDateField;

        private bool claimDateFieldSpecified;

        private string claimActivityField;

        private string commentsField;

        /// <remarks/>
        public int Sequence
        {
            get
            {
                return this.sequenceField;
            }
            set
            {
                this.sequenceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SequenceSpecified
        {
            get
            {
                return this.sequenceFieldSpecified;
            }
            set
            {
                this.sequenceFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime ClaimDate
        {
            get
            {
                return this.claimDateField;
            }
            set
            {
                this.claimDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ClaimDateSpecified
        {
            get
            {
                return this.claimDateFieldSpecified;
            }
            set
            {
                this.claimDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string ClaimActivity
        {
            get
            {
                return this.claimActivityField;
            }
            set
            {
                this.claimActivityField = value;
            }
        }

        /// <remarks/>
        public string Comments
        {
            get
            {
                return this.commentsField;
            }
            set
            {
                this.commentsField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class FetchClaimsStatusRequest
    {

        private UniqueID nameIDField;

        private System.DateTime beginDateField;

        private System.DateTime endDateField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }

        /// <remarks/>
        public System.DateTime BeginDate
        {
            get
            {
                return this.beginDateField;
            }
            set
            {
                this.beginDateField = value;
            }
        }

        /// <remarks/>
        public System.DateTime EndDate
        {
            get
            {
                return this.endDateField;
            }
            set
            {
                this.endDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class InsertClaimResponse
    {

        private ResultStatus resultField;

        private UniqueID nameIDField;

        private Claim[] claimsListField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("ClaimsInfo", Namespace = "http://webservices.micros.com/og/4.3/Common/", IsNullable = false)]
        public Claim[] ClaimsList
        {
            get
            {
                return this.claimsListField;
            }
            set
            {
                this.claimsListField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Common/")]
    public partial class GuestProfile
    {

        private UniqueID nameIDField;

        private PersonName personNameField;

        private System.DateTime birthDateField;

        private bool birthDateFieldSpecified;

        private Gender genderField;

        private bool genderFieldSpecified;

        private Address addressField;

        private Phone phoneField;

        private string emailAddressField;

        private UniqueID membershipIDField;

        private string membershipNumberField;

        private string membershipTypeField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }

        /// <remarks/>
        public PersonName PersonName
        {
            get
            {
                return this.personNameField;
            }
            set
            {
                this.personNameField = value;
            }
        }

        /// <remarks/>
        public System.DateTime BirthDate
        {
            get
            {
                return this.birthDateField;
            }
            set
            {
                this.birthDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool BirthDateSpecified
        {
            get
            {
                return this.birthDateFieldSpecified;
            }
            set
            {
                this.birthDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public Gender Gender
        {
            get
            {
                return this.genderField;
            }
            set
            {
                this.genderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool GenderSpecified
        {
            get
            {
                return this.genderFieldSpecified;
            }
            set
            {
                this.genderFieldSpecified = value;
            }
        }

        /// <remarks/>
        public Address Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        public Phone Phone
        {
            get
            {
                return this.phoneField;
            }
            set
            {
                this.phoneField = value;
            }
        }

        /// <remarks/>
        public string EmailAddress
        {
            get
            {
                return this.emailAddressField;
            }
            set
            {
                this.emailAddressField = value;
            }
        }

        /// <remarks/>
        public UniqueID MembershipID
        {
            get
            {
                return this.membershipIDField;
            }
            set
            {
                this.membershipIDField = value;
            }
        }

        /// <remarks/>
        public string MembershipNumber
        {
            get
            {
                return this.membershipNumberField;
            }
            set
            {
                this.membershipNumberField = value;
            }
        }

        /// <remarks/>
        public string MembershipType
        {
            get
            {
                return this.membershipTypeField;
            }
            set
            {
                this.membershipTypeField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(NativeName))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Common/")]
    public partial class PersonName
    {

        private string[] nameTitleField;

        private string firstNameField;

        private string[] middleNameField;

        private string lastNameField;

        private string[] nameSuffixField;

        private string nameOrderedField;

        private string familiarNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("nameTitle")]
        public string[] nameTitle
        {
            get
            {
                return this.nameTitleField;
            }
            set
            {
                this.nameTitleField = value;
            }
        }

        /// <remarks/>
        public string firstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("middleName")]
        public string[] middleName
        {
            get
            {
                return this.middleNameField;
            }
            set
            {
                this.middleNameField = value;
            }
        }

        /// <remarks/>
        public string lastName
        {
            get
            {
                return this.lastNameField;
            }
            set
            {
                this.lastNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("nameSuffix")]
        public string[] nameSuffix
        {
            get
            {
                return this.nameSuffixField;
            }
            set
            {
                this.nameSuffixField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string nameOrdered
        {
            get
            {
                return this.nameOrderedField;
            }
            set
            {
                this.nameOrderedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string familiarName
        {
            get
            {
                return this.familiarNameField;
            }
            set
            {
                this.familiarNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/")]
    public partial class NativeName : PersonName
    {

        private string languageCodeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string languageCode
        {
            get
            {
                return this.languageCodeField;
            }
            set
            {
                this.languageCodeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Common/")]
    public enum Gender
    {
        /// <remarks/>
        UNKNOWN,

        /// <remarks/>
        FEMALE,

        /// <remarks/>
        MALE


    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(NameAddress))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Common/")]
    public partial class Address
    {

        private string[] addressLineField;

        private string cityNameField;

        private string stateProvField;

        private string countryCodeField;

        private string postalCodeField;

        private string addressTypeField;

        private string otherAddressTypeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("AddressLine")]
        public string[] AddressLine
        {
            get
            {
                return this.addressLineField;
            }
            set
            {
                this.addressLineField = value;
            }
        }

        /// <remarks/>
        public string cityName
        {
            get
            {
                return this.cityNameField;
            }
            set
            {
                this.cityNameField = value;
            }
        }

        /// <remarks/>
        public string stateProv
        {
            get
            {
                return this.stateProvField;
            }
            set
            {
                this.stateProvField = value;
            }
        }

        /// <remarks/>
        public string countryCode
        {
            get
            {
                return this.countryCodeField;
            }
            set
            {
                this.countryCodeField = value;
            }
        }

        /// <remarks/>
        public string postalCode
        {
            get
            {
                return this.postalCodeField;
            }
            set
            {
                this.postalCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string addressType
        {
            get
            {
                return this.addressTypeField;
            }
            set
            {
                this.addressTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string otherAddressType
        {
            get
            {
                return this.otherAddressTypeField;
            }
            set
            {
                this.otherAddressTypeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/")]
    public partial class NameAddress : Address
    {

        private long operaIdField;

        private bool operaIdFieldSpecified;

        private string externalIdField;

        private bool primaryField;

        private bool primaryFieldSpecified;

        private int displaySequenceField;

        private bool displaySequenceFieldSpecified;

        private string insertUserField;

        private System.DateTime insertDateField;

        private bool insertDateFieldSpecified;

        private string updateUserField;

        private System.DateTime updateDateField;

        private bool updateDateFieldSpecified;

        private System.DateTime inactiveDateField;

        private bool inactiveDateFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public long operaId
        {
            get
            {
                return this.operaIdField;
            }
            set
            {
                this.operaIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool operaIdSpecified
        {
            get
            {
                return this.operaIdFieldSpecified;
            }
            set
            {
                this.operaIdFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string externalId
        {
            get
            {
                return this.externalIdField;
            }
            set
            {
                this.externalIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool primary
        {
            get
            {
                return this.primaryField;
            }
            set
            {
                this.primaryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool primarySpecified
        {
            get
            {
                return this.primaryFieldSpecified;
            }
            set
            {
                this.primaryFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int displaySequence
        {
            get
            {
                return this.displaySequenceField;
            }
            set
            {
                this.displaySequenceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool displaySequenceSpecified
        {
            get
            {
                return this.displaySequenceFieldSpecified;
            }
            set
            {
                this.displaySequenceFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string insertUser
        {
            get
            {
                return this.insertUserField;
            }
            set
            {
                this.insertUserField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime insertDate
        {
            get
            {
                return this.insertDateField;
            }
            set
            {
                this.insertDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool insertDateSpecified
        {
            get
            {
                return this.insertDateFieldSpecified;
            }
            set
            {
                this.insertDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string updateUser
        {
            get
            {
                return this.updateUserField;
            }
            set
            {
                this.updateUserField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime updateDate
        {
            get
            {
                return this.updateDateField;
            }
            set
            {
                this.updateDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool updateDateSpecified
        {
            get
            {
                return this.updateDateFieldSpecified;
            }
            set
            {
                this.updateDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime inactiveDate
        {
            get
            {
                return this.inactiveDateField;
            }
            set
            {
                this.inactiveDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool inactiveDateSpecified
        {
            get
            {
                return this.inactiveDateFieldSpecified;
            }
            set
            {
                this.inactiveDateFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(NamePhone))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Common/")]
    public partial class Phone
    {

        private object itemField;

        private string phoneTypeField;

        private string phoneRoleField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("PhoneData", typeof(PhonePhoneData))]
        [System.Xml.Serialization.XmlElementAttribute("PhoneNumber", typeof(string))]
        public object Item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string phoneType
        {
            get
            {
                return this.phoneTypeField;
            }
            set
            {
                this.phoneTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string phoneRole
        {
            get
            {
                return this.phoneRoleField;
            }
            set
            {
                this.phoneRoleField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Common/")]
    public partial class PhonePhoneData
    {

        private string countryAccessCodeField;

        private string areaCodeField;

        private string phoneNumberField;

        private string extensionField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string countryAccessCode
        {
            get
            {
                return this.countryAccessCodeField;
            }
            set
            {
                this.countryAccessCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string areaCode
        {
            get
            {
                return this.areaCodeField;
            }
            set
            {
                this.areaCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string phoneNumber
        {
            get
            {
                return this.phoneNumberField;
            }
            set
            {
                this.phoneNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string extension
        {
            get
            {
                return this.extensionField;
            }
            set
            {
                this.extensionField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/")]
    public partial class NamePhone : Phone
    {

        private long operaIdField;

        private bool operaIdFieldSpecified;

        private string externalIdField;

        private bool primaryField;

        private bool primaryFieldSpecified;

        private int displaySequenceField;

        private bool displaySequenceFieldSpecified;

        private string insertUserField;

        private System.DateTime insertDateField;

        private bool insertDateFieldSpecified;

        private string updateUserField;

        private System.DateTime updateDateField;

        private bool updateDateFieldSpecified;

        private System.DateTime inactiveDateField;

        private bool inactiveDateFieldSpecified;

        private string extensionField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public long operaId
        {
            get
            {
                return this.operaIdField;
            }
            set
            {
                this.operaIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool operaIdSpecified
        {
            get
            {
                return this.operaIdFieldSpecified;
            }
            set
            {
                this.operaIdFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string externalId
        {
            get
            {
                return this.externalIdField;
            }
            set
            {
                this.externalIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool primary
        {
            get
            {
                return this.primaryField;
            }
            set
            {
                this.primaryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool primarySpecified
        {
            get
            {
                return this.primaryFieldSpecified;
            }
            set
            {
                this.primaryFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int displaySequence
        {
            get
            {
                return this.displaySequenceField;
            }
            set
            {
                this.displaySequenceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool displaySequenceSpecified
        {
            get
            {
                return this.displaySequenceFieldSpecified;
            }
            set
            {
                this.displaySequenceFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string insertUser
        {
            get
            {
                return this.insertUserField;
            }
            set
            {
                this.insertUserField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime insertDate
        {
            get
            {
                return this.insertDateField;
            }
            set
            {
                this.insertDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool insertDateSpecified
        {
            get
            {
                return this.insertDateFieldSpecified;
            }
            set
            {
                this.insertDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string updateUser
        {
            get
            {
                return this.updateUserField;
            }
            set
            {
                this.updateUserField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime updateDate
        {
            get
            {
                return this.updateDateField;
            }
            set
            {
                this.updateDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool updateDateSpecified
        {
            get
            {
                return this.updateDateFieldSpecified;
            }
            set
            {
                this.updateDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime inactiveDate
        {
            get
            {
                return this.inactiveDateField;
            }
            set
            {
                this.inactiveDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool inactiveDateSpecified
        {
            get
            {
                return this.inactiveDateFieldSpecified;
            }
            set
            {
                this.inactiveDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string extension
        {
            get
            {
                return this.extensionField;
            }
            set
            {
                this.extensionField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class InsertClaimRequest
    {

        private GuestProfile profileField;

        private string callerNameField;

        private Claim claimField;

        /// <remarks/>
        public GuestProfile Profile
        {
            get
            {
                return this.profileField;
            }
            set
            {
                this.profileField = value;
            }
        }

        /// <remarks/>
        public string CallerName
        {
            get
            {
                return this.callerNameField;
            }
            set
            {
                this.callerNameField = value;
            }
        }

        /// <remarks/>
        public Claim Claim
        {
            get
            {
                return this.claimField;
            }
            set
            {
                this.claimField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class FetchProfileResponse
    {

        private ResultStatus resultField;

        private Profile profileDetailsField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        public Profile ProfileDetails
        {
            get
            {
                return this.profileDetailsField;
            }
            set
            {
                this.profileDetailsField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/")]
    public partial class Profile
    {

        private UniqueID[] profileIDsField;

        private object itemField;

        private NameCreditCard[] creditCardsField;

        private NameAddress[] addressesField;

        private BlackList blacklistField;

        private NamePhone[] phonesField;

        private Preference[] preferencesField;

        private NameEmail[] eMailsField;

        private NameMembership[] membershipsField;

        private NegotiatedRate[] negotiatedRatesField;

        private Comment[] commentsField;

        private UserDefinedValue[] userDefinedValuesField;

        private PrivacyOptionType[] privacyField;

        private ProfileUserGroup userGroupField;

        private StayHistoryData stayHistoryField;

        private string nameTypeField;

        private bool protectedField;

        private bool protectedFieldSpecified;

        private string languageCodeField;

        private string nationalityField;

        private string vipCodeField;

        private bool taxExemptField;

        private bool taxExemptFieldSpecified;

        private string insertUserField;

        private System.DateTime insertDateField;

        private bool insertDateFieldSpecified;

        private string updateUserField;

        private System.DateTime updateDateField;

        private bool updateDateFieldSpecified;

        private System.DateTime inactiveDateField;

        private bool inactiveDateFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(Namespace = "http://webservices.micros.com/og/4.3/Common/", IsNullable = false)]
        public UniqueID[] ProfileIDs
        {
            get
            {
                return this.profileIDsField;
            }
            set
            {
                this.profileIDsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Company", typeof(Company))]
        [System.Xml.Serialization.XmlElementAttribute("Customer", typeof(Customer))]
        public object Item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public NameCreditCard[] CreditCards
        {
            get
            {
                return this.creditCardsField;
            }
            set
            {
                this.creditCardsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public NameAddress[] Addresses
        {
            get
            {
                return this.addressesField;
            }
            set
            {
                this.addressesField = value;
            }
        }

        /// <remarks/>
        public BlackList Blacklist
        {
            get
            {
                return this.blacklistField;
            }
            set
            {
                this.blacklistField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public NamePhone[] Phones
        {
            get
            {
                return this.phonesField;
            }
            set
            {
                this.phonesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public Preference[] Preferences
        {
            get
            {
                return this.preferencesField;
            }
            set
            {
                this.preferencesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public NameEmail[] EMails
        {
            get
            {
                return this.eMailsField;
            }
            set
            {
                this.eMailsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public NameMembership[] Memberships
        {
            get
            {
                return this.membershipsField;
            }
            set
            {
                this.membershipsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public NegotiatedRate[] NegotiatedRates
        {
            get
            {
                return this.negotiatedRatesField;
            }
            set
            {
                this.negotiatedRatesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public Comment[] Comments
        {
            get
            {
                return this.commentsField;
            }
            set
            {
                this.commentsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(Namespace = "http://webservices.micros.com/og/4.3/Common/", IsNullable = false)]
        public UserDefinedValue[] UserDefinedValues
        {
            get
            {
                return this.userDefinedValuesField;
            }
            set
            {
                this.userDefinedValuesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("PrivacyOption", IsNullable = false)]
        public PrivacyOptionType[] Privacy
        {
            get
            {
                return this.privacyField;
            }
            set
            {
                this.privacyField = value;
            }
        }

        /// <remarks/>
        public ProfileUserGroup UserGroup
        {
            get
            {
                return this.userGroupField;
            }
            set
            {
                this.userGroupField = value;
            }
        }

        /// <remarks/>
        public StayHistoryData StayHistory
        {
            get
            {
                return this.stayHistoryField;
            }
            set
            {
                this.stayHistoryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string nameType
        {
            get
            {
                return this.nameTypeField;
            }
            set
            {
                this.nameTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool @protected
        {
            get
            {
                return this.protectedField;
            }
            set
            {
                this.protectedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool protectedSpecified
        {
            get
            {
                return this.protectedFieldSpecified;
            }
            set
            {
                this.protectedFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string languageCode
        {
            get
            {
                return this.languageCodeField;
            }
            set
            {
                this.languageCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string nationality
        {
            get
            {
                return this.nationalityField;
            }
            set
            {
                this.nationalityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string vipCode
        {
            get
            {
                return this.vipCodeField;
            }
            set
            {
                this.vipCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool taxExempt
        {
            get
            {
                return this.taxExemptField;
            }
            set
            {
                this.taxExemptField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool taxExemptSpecified
        {
            get
            {
                return this.taxExemptFieldSpecified;
            }
            set
            {
                this.taxExemptFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string insertUser
        {
            get
            {
                return this.insertUserField;
            }
            set
            {
                this.insertUserField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime insertDate
        {
            get
            {
                return this.insertDateField;
            }
            set
            {
                this.insertDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool insertDateSpecified
        {
            get
            {
                return this.insertDateFieldSpecified;
            }
            set
            {
                this.insertDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string updateUser
        {
            get
            {
                return this.updateUserField;
            }
            set
            {
                this.updateUserField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime updateDate
        {
            get
            {
                return this.updateDateField;
            }
            set
            {
                this.updateDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool updateDateSpecified
        {
            get
            {
                return this.updateDateFieldSpecified;
            }
            set
            {
                this.updateDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime inactiveDate
        {
            get
            {
                return this.inactiveDateField;
            }
            set
            {
                this.inactiveDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool inactiveDateSpecified
        {
            get
            {
                return this.inactiveDateFieldSpecified;
            }
            set
            {
                this.inactiveDateFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/")]
    public partial class Company
    {

        private string companyNameField;

        private CompanyCompanyType companyTypeField;

        private string companyIDField;

        private string commissionCodeField;

        /// <remarks/>
        public string CompanyName
        {
            get
            {
                return this.companyNameField;
            }
            set
            {
                this.companyNameField = value;
            }
        }

        /// <remarks/>
        public CompanyCompanyType CompanyType
        {
            get
            {
                return this.companyTypeField;
            }
            set
            {
                this.companyTypeField = value;
            }
        }

        /// <remarks/>
        public string CompanyID
        {
            get
            {
                return this.companyIDField;
            }
            set
            {
                this.companyIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string commissionCode
        {
            get
            {
                return this.commissionCodeField;
            }
            set
            {
                this.commissionCodeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/")]
    public enum CompanyCompanyType
    {

        /// <remarks/>
        TRAVEL_AGENT,

        /// <remarks/>
        COMPANY,

        /// <remarks/>
        SOURCE,

        /// <remarks/>
        GROUP,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/")]
    public partial class Customer
    {

        private PersonName personNameField;

        private NativeName nativeNameField;

        private string businessTitleField;

        private GovernmentID[] governmentIDListField;

        private Gender genderField;

        private bool genderFieldSpecified;

        private System.DateTime birthDateField;

        private bool birthDateFieldSpecified;

        /// <remarks/>
        public PersonName PersonName
        {
            get
            {
                return this.personNameField;
            }
            set
            {
                this.personNameField = value;
            }
        }

        /// <remarks/>
        public NativeName NativeName
        {
            get
            {
                return this.nativeNameField;
            }
            set
            {
                this.nativeNameField = value;
            }
        }

        /// <remarks/>
        public string BusinessTitle
        {
            get
            {
                return this.businessTitleField;
            }
            set
            {
                this.businessTitleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(Namespace = "http://webservices.micros.com/og/4.3/Common/", IsNullable = false)]
        public GovernmentID[] GovernmentIDList
        {
            get
            {
                return this.governmentIDListField;
            }
            set
            {
                this.governmentIDListField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public Gender gender
        {
            get
            {
                return this.genderField;
            }
            set
            {
                this.genderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool genderSpecified
        {
            get
            {
                return this.genderFieldSpecified;
            }
            set
            {
                this.genderFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime birthDate
        {
            get
            {
                return this.birthDateField;
            }
            set
            {
                this.birthDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool birthDateSpecified
        {
            get
            {
                return this.birthDateFieldSpecified;
            }
            set
            {
                this.birthDateFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Common/")]
    public partial class GovernmentID
    {

        private string documentTypeField;

        private string documentNumberField;

        private System.DateTime effectiveDateField;

        private bool effectiveDateFieldSpecified;

        private System.DateTime expirationDateField;

        private bool expirationDateFieldSpecified;

        private string placeOfIssueField;

        private string countryOfIssueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string documentType
        {
            get
            {
                return this.documentTypeField;
            }
            set
            {
                this.documentTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string documentNumber
        {
            get
            {
                return this.documentNumberField;
            }
            set
            {
                this.documentNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime effectiveDate
        {
            get
            {
                return this.effectiveDateField;
            }
            set
            {
                this.effectiveDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool effectiveDateSpecified
        {
            get
            {
                return this.effectiveDateFieldSpecified;
            }
            set
            {
                this.effectiveDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime expirationDate
        {
            get
            {
                return this.expirationDateField;
            }
            set
            {
                this.expirationDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool expirationDateSpecified
        {
            get
            {
                return this.expirationDateFieldSpecified;
            }
            set
            {
                this.expirationDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string placeOfIssue
        {
            get
            {
                return this.placeOfIssueField;
            }
            set
            {
                this.placeOfIssueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string countryOfIssue
        {
            get
            {
                return this.countryOfIssueField;
            }
            set
            {
                this.countryOfIssueField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/")]
    public partial class NameCreditCard : CreditCard
    {

        private long operaIdField;

        private bool operaIdFieldSpecified;

        private string externalIdField;

        private bool primaryField;

        private bool primaryFieldSpecified;

        private int displaySequenceField;

        private bool displaySequenceFieldSpecified;

        private string insertUserField;

        private System.DateTime insertDateField;

        private bool insertDateFieldSpecified;

        private string updateUserField;

        private System.DateTime updateDateField;

        private bool updateDateFieldSpecified;

        private System.DateTime inactiveDateField;

        private bool inactiveDateFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public long operaId
        {
            get
            {
                return this.operaIdField;
            }
            set
            {
                this.operaIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool operaIdSpecified
        {
            get
            {
                return this.operaIdFieldSpecified;
            }
            set
            {
                this.operaIdFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string externalId
        {
            get
            {
                return this.externalIdField;
            }
            set
            {
                this.externalIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool primary
        {
            get
            {
                return this.primaryField;
            }
            set
            {
                this.primaryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool primarySpecified
        {
            get
            {
                return this.primaryFieldSpecified;
            }
            set
            {
                this.primaryFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int displaySequence
        {
            get
            {
                return this.displaySequenceField;
            }
            set
            {
                this.displaySequenceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool displaySequenceSpecified
        {
            get
            {
                return this.displaySequenceFieldSpecified;
            }
            set
            {
                this.displaySequenceFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string insertUser
        {
            get
            {
                return this.insertUserField;
            }
            set
            {
                this.insertUserField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime insertDate
        {
            get
            {
                return this.insertDateField;
            }
            set
            {
                this.insertDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool insertDateSpecified
        {
            get
            {
                return this.insertDateFieldSpecified;
            }
            set
            {
                this.insertDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string updateUser
        {
            get
            {
                return this.updateUserField;
            }
            set
            {
                this.updateUserField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime updateDate
        {
            get
            {
                return this.updateDateField;
            }
            set
            {
                this.updateDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool updateDateSpecified
        {
            get
            {
                return this.updateDateFieldSpecified;
            }
            set
            {
                this.updateDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime inactiveDate
        {
            get
            {
                return this.inactiveDateField;
            }
            set
            {
                this.inactiveDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool inactiveDateSpecified
        {
            get
            {
                return this.inactiveDateFieldSpecified;
            }
            set
            {
                this.inactiveDateFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(NameCreditCard))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(CreditCardPayment))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Common/")]
    public partial class CreditCard
    {

        private string cardCodeField;

        private string cardHolderNameField;

        private string cardNumberField;

        private string seriesCodeField;

        private string track2Field;

        private System.DateTime effectiveDateField;

        private bool effectiveDateFieldSpecified;

        private System.DateTime expirationDateField;

        private string cardTypeField;

        private string otherCardTypeField;

        /// <remarks/>
        public string cardCode
        {
            get
            {
                return this.cardCodeField;
            }
            set
            {
                this.cardCodeField = value;
            }
        }

        /// <remarks/>
        public string cardHolderName
        {
            get
            {
                return this.cardHolderNameField;
            }
            set
            {
                this.cardHolderNameField = value;
            }
        }

        /// <remarks/>
        public string cardNumber
        {
            get
            {
                return this.cardNumberField;
            }
            set
            {
                this.cardNumberField = value;
            }
        }

        /// <remarks/>
        public string seriesCode
        {
            get
            {
                return this.seriesCodeField;
            }
            set
            {
                this.seriesCodeField = value;
            }
        }

        /// <remarks/>
        public string Track2
        {
            get
            {
                return this.track2Field;
            }
            set
            {
                this.track2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime effectiveDate
        {
            get
            {
                return this.effectiveDateField;
            }
            set
            {
                this.effectiveDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool effectiveDateSpecified
        {
            get
            {
                return this.effectiveDateFieldSpecified;
            }
            set
            {
                this.effectiveDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime expirationDate
        {
            get
            {
                return this.expirationDateField;
            }
            set
            {
                this.expirationDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string cardType
        {
            get
            {
                return this.cardTypeField;
            }
            set
            {
                this.cardTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string otherCardType
        {
            get
            {
                return this.otherCardTypeField;
            }
            set
            {
                this.otherCardTypeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/HotelCommon/")]
    public partial class CreditCardPayment : CreditCard
    {

        private string cIDNumberField;

        private Address addressField;

        private string issueNumberField;

        private string approvalCodeField;

        private Amount depositAmountField;

        /// <remarks/>
        public string CIDNumber
        {
            get
            {
                return this.cIDNumberField;
            }
            set
            {
                this.cIDNumberField = value;
            }
        }

        /// <remarks/>
        public Address Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        public string IssueNumber
        {
            get
            {
                return this.issueNumberField;
            }
            set
            {
                this.issueNumberField = value;
            }
        }

        /// <remarks/>
        public string ApprovalCode
        {
            get
            {
                return this.approvalCodeField;
            }
            set
            {
                this.approvalCodeField = value;
            }
        }

        /// <remarks/>
        public Amount DepositAmount
        {
            get
            {
                return this.depositAmountField;
            }
            set
            {
                this.depositAmountField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/")]
    public partial class BlackList
    {

        private BlackListFlag flagField;

        private bool flagFieldSpecified;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public BlackListFlag flag
        {
            get
            {
                return this.flagField;
            }
            set
            {
                this.flagField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool flagSpecified
        {
            get
            {
                return this.flagFieldSpecified;
            }
            set
            {
                this.flagFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Common/")]
    public enum BlackListFlag
    {

        /// <remarks/>
        REMOVE,

        /// <remarks/>
        SET,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/")]
    public partial class Preference
    {

        private DescriptiveText preferenceDescriptionField;

        private string resortCodeField;

        private string preferenceTypeField;

        private string otherPreferenceTypeField;

        private string preferenceValueField;

        private string insertUserField;

        private System.DateTime insertDateField;

        private bool insertDateFieldSpecified;

        private string updateUserField;

        private System.DateTime updateDateField;

        private bool updateDateFieldSpecified;

        private System.DateTime inactiveDateField;

        private bool inactiveDateFieldSpecified;

        /// <remarks/>
        public DescriptiveText PreferenceDescription
        {
            get
            {
                return this.preferenceDescriptionField;
            }
            set
            {
                this.preferenceDescriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string resortCode
        {
            get
            {
                return this.resortCodeField;
            }
            set
            {
                this.resortCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string preferenceType
        {
            get
            {
                return this.preferenceTypeField;
            }
            set
            {
                this.preferenceTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string otherPreferenceType
        {
            get
            {
                return this.otherPreferenceTypeField;
            }
            set
            {
                this.otherPreferenceTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string preferenceValue
        {
            get
            {
                return this.preferenceValueField;
            }
            set
            {
                this.preferenceValueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string insertUser
        {
            get
            {
                return this.insertUserField;
            }
            set
            {
                this.insertUserField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime insertDate
        {
            get
            {
                return this.insertDateField;
            }
            set
            {
                this.insertDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool insertDateSpecified
        {
            get
            {
                return this.insertDateFieldSpecified;
            }
            set
            {
                this.insertDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string updateUser
        {
            get
            {
                return this.updateUserField;
            }
            set
            {
                this.updateUserField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime updateDate
        {
            get
            {
                return this.updateDateField;
            }
            set
            {
                this.updateDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool updateDateSpecified
        {
            get
            {
                return this.updateDateFieldSpecified;
            }
            set
            {
                this.updateDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime inactiveDate
        {
            get
            {
                return this.inactiveDateField;
            }
            set
            {
                this.inactiveDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool inactiveDateSpecified
        {
            get
            {
                return this.inactiveDateFieldSpecified;
            }
            set
            {
                this.inactiveDateFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Comment))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(HotelInfo))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Common/")]
    public partial class DescriptiveText
    {

        private object itemField;

        private ItemChoiceType itemElementNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Image", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("Text", typeof(TextElement[]))]
        [System.Xml.Serialization.XmlElementAttribute("Url", typeof(string), DataType = "anyURI")]
        [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemElementName")]
        public object Item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public ItemChoiceType ItemElementName
        {
            get
            {
                return this.itemElementNameField;
            }
            set
            {
                this.itemElementNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Common/", IncludeInSchema = false)]
    public enum ItemChoiceType
    {

        /// <remarks/>
        Image,

        /// <remarks/>
        Text,

        /// <remarks/>
        Url,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/")]
    public partial class Comment : DescriptiveText
    {

        private long operaIdField;

        private bool operaIdFieldSpecified;

        private string externalIdField;

        private string commentTitleField;

        private string insertUserField;

        private System.DateTime insertDateField;

        private bool insertDateFieldSpecified;

        private string updateUserField;

        private System.DateTime updateDateField;

        private bool updateDateFieldSpecified;

        private System.DateTime inactiveDateField;

        private bool inactiveDateFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public long operaId
        {
            get
            {
                return this.operaIdField;
            }
            set
            {
                this.operaIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool operaIdSpecified
        {
            get
            {
                return this.operaIdFieldSpecified;
            }
            set
            {
                this.operaIdFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string externalId
        {
            get
            {
                return this.externalIdField;
            }
            set
            {
                this.externalIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string commentTitle
        {
            get
            {
                return this.commentTitleField;
            }
            set
            {
                this.commentTitleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string insertUser
        {
            get
            {
                return this.insertUserField;
            }
            set
            {
                this.insertUserField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime insertDate
        {
            get
            {
                return this.insertDateField;
            }
            set
            {
                this.insertDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool insertDateSpecified
        {
            get
            {
                return this.insertDateFieldSpecified;
            }
            set
            {
                this.insertDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string updateUser
        {
            get
            {
                return this.updateUserField;
            }
            set
            {
                this.updateUserField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime updateDate
        {
            get
            {
                return this.updateDateField;
            }
            set
            {
                this.updateDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool updateDateSpecified
        {
            get
            {
                return this.updateDateFieldSpecified;
            }
            set
            {
                this.updateDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime inactiveDate
        {
            get
            {
                return this.inactiveDateField;
            }
            set
            {
                this.inactiveDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool inactiveDateSpecified
        {
            get
            {
                return this.inactiveDateFieldSpecified;
            }
            set
            {
                this.inactiveDateFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/HotelCommon/")]
    public partial class HotelInfo : DescriptiveText
    {

        private HotelInfoType hotelInfoTypeField;

        private string otherHotelInfoTypeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public HotelInfoType hotelInfoType
        {
            get
            {
                return this.hotelInfoTypeField;
            }
            set
            {
                this.hotelInfoTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string otherHotelInfoType
        {
            get
            {
                return this.otherHotelInfoTypeField;
            }
            set
            {
                this.otherHotelInfoTypeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/HotelCommon/")]
    public enum HotelInfoType
    {

        /// <remarks/>
        DIRECTIONS,

        /// <remarks/>
        CHECKININFO,

        /// <remarks/>
        CHECKOUTINFO,

        /// <remarks/>
        OTHER,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/")]
    public partial class NameEmail
    {

        private long operaIdField;

        private bool operaIdFieldSpecified;

        private string externalIdField;

        private bool primaryField;

        private bool primaryFieldSpecified;

        private int displaySequenceField;

        private bool displaySequenceFieldSpecified;

        private string insertUserField;

        private System.DateTime insertDateField;

        private bool insertDateFieldSpecified;

        private string updateUserField;

        private System.DateTime updateDateField;

        private bool updateDateFieldSpecified;

        private System.DateTime inactiveDateField;

        private bool inactiveDateFieldSpecified;

        private string emailTypeField;

        private string emailFormatField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public long operaId
        {
            get
            {
                return this.operaIdField;
            }
            set
            {
                this.operaIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool operaIdSpecified
        {
            get
            {
                return this.operaIdFieldSpecified;
            }
            set
            {
                this.operaIdFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string externalId
        {
            get
            {
                return this.externalIdField;
            }
            set
            {
                this.externalIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool primary
        {
            get
            {
                return this.primaryField;
            }
            set
            {
                this.primaryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool primarySpecified
        {
            get
            {
                return this.primaryFieldSpecified;
            }
            set
            {
                this.primaryFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int displaySequence
        {
            get
            {
                return this.displaySequenceField;
            }
            set
            {
                this.displaySequenceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool displaySequenceSpecified
        {
            get
            {
                return this.displaySequenceFieldSpecified;
            }
            set
            {
                this.displaySequenceFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string insertUser
        {
            get
            {
                return this.insertUserField;
            }
            set
            {
                this.insertUserField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime insertDate
        {
            get
            {
                return this.insertDateField;
            }
            set
            {
                this.insertDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool insertDateSpecified
        {
            get
            {
                return this.insertDateFieldSpecified;
            }
            set
            {
                this.insertDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string updateUser
        {
            get
            {
                return this.updateUserField;
            }
            set
            {
                this.updateUserField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime updateDate
        {
            get
            {
                return this.updateDateField;
            }
            set
            {
                this.updateDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool updateDateSpecified
        {
            get
            {
                return this.updateDateFieldSpecified;
            }
            set
            {
                this.updateDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime inactiveDate
        {
            get
            {
                return this.inactiveDateField;
            }
            set
            {
                this.inactiveDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool inactiveDateSpecified
        {
            get
            {
                return this.inactiveDateFieldSpecified;
            }
            set
            {
                this.inactiveDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string emailType
        {
            get
            {
                return this.emailTypeField;
            }
            set
            {
                this.emailTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string emailFormat
        {
            get
            {
                return this.emailFormatField;
            }
            set
            {
                this.emailFormatField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/")]
    public partial class NameMembership : Membership
    {

        private long operaIdField;

        private bool operaIdFieldSpecified;

        private string externalIdField;

        private bool primaryField;

        private bool primaryFieldSpecified;

        private bool preferredField;

        private bool preferredFieldSpecified;

        private bool centralField;

        private bool centralFieldSpecified;

        private string membershipClassField;

        private string pointsLabelField;

        private string statusField;

        private int displaySequenceField;

        private bool displaySequenceFieldSpecified;

        private string insertUserField;

        private System.DateTime insertDateField;

        private bool insertDateFieldSpecified;

        private string updateUserField;

        private System.DateTime updateDateField;

        private bool updateDateFieldSpecified;

        private System.DateTime inactiveDateField;

        private bool inactiveDateFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public long operaId
        {
            get
            {
                return this.operaIdField;
            }
            set
            {
                this.operaIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool operaIdSpecified
        {
            get
            {
                return this.operaIdFieldSpecified;
            }
            set
            {
                this.operaIdFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string externalId
        {
            get
            {
                return this.externalIdField;
            }
            set
            {
                this.externalIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool primary
        {
            get
            {
                return this.primaryField;
            }
            set
            {
                this.primaryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool primarySpecified
        {
            get
            {
                return this.primaryFieldSpecified;
            }
            set
            {
                this.primaryFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool preferred
        {
            get
            {
                return this.preferredField;
            }
            set
            {
                this.preferredField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool preferredSpecified
        {
            get
            {
                return this.preferredFieldSpecified;
            }
            set
            {
                this.preferredFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool central
        {
            get
            {
                return this.centralField;
            }
            set
            {
                this.centralField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool centralSpecified
        {
            get
            {
                return this.centralFieldSpecified;
            }
            set
            {
                this.centralFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string membershipClass
        {
            get
            {
                return this.membershipClassField;
            }
            set
            {
                this.membershipClassField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string pointsLabel
        {
            get
            {
                return this.pointsLabelField;
            }
            set
            {
                this.pointsLabelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int displaySequence
        {
            get
            {
                return this.displaySequenceField;
            }
            set
            {
                this.displaySequenceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool displaySequenceSpecified
        {
            get
            {
                return this.displaySequenceFieldSpecified;
            }
            set
            {
                this.displaySequenceFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string insertUser
        {
            get
            {
                return this.insertUserField;
            }
            set
            {
                this.insertUserField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime insertDate
        {
            get
            {
                return this.insertDateField;
            }
            set
            {
                this.insertDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool insertDateSpecified
        {
            get
            {
                return this.insertDateFieldSpecified;
            }
            set
            {
                this.insertDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string updateUser
        {
            get
            {
                return this.updateUserField;
            }
            set
            {
                this.updateUserField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime updateDate
        {
            get
            {
                return this.updateDateField;
            }
            set
            {
                this.updateDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool updateDateSpecified
        {
            get
            {
                return this.updateDateFieldSpecified;
            }
            set
            {
                this.updateDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime inactiveDate
        {
            get
            {
                return this.inactiveDateField;
            }
            set
            {
                this.inactiveDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool inactiveDateSpecified
        {
            get
            {
                return this.inactiveDateFieldSpecified;
            }
            set
            {
                this.inactiveDateFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(NameMembership))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Common/")]
    public partial class Membership
    {

        private string membershipTypeField;

        private string membershipNumberField;

        private string membershipLevelField;

        private string memberNameField;

        private System.DateTime effectiveDateField;

        private bool effectiveDateFieldSpecified;

        private System.DateTime expirationDateField;

        private bool expirationDateFieldSpecified;

        private double currentPointsField;

        private bool currentPointsFieldSpecified;

        private string enrollmentCodeField;

        private UniqueID[] resvNameIdField;

        private UniqueID membershipidField;

        private long transferPointsField;

        private bool transferPointsFieldSpecified;

        private string enrollmentSourceField;

        private string enrolledAtField;

        private AwardPointsToExpire[] awardPointsToExpiresField;

        /// <remarks/>
        public string membershipType
        {
            get
            {
                return this.membershipTypeField;
            }
            set
            {
                this.membershipTypeField = value;
            }
        }

        /// <remarks/>
        public string membershipNumber
        {
            get
            {
                return this.membershipNumberField;
            }
            set
            {
                this.membershipNumberField = value;
            }
        }

        /// <remarks/>
        public string membershipLevel
        {
            get
            {
                return this.membershipLevelField;
            }
            set
            {
                this.membershipLevelField = value;
            }
        }

        /// <remarks/>
        public string memberName
        {
            get
            {
                return this.memberNameField;
            }
            set
            {
                this.memberNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime effectiveDate
        {
            get
            {
                return this.effectiveDateField;
            }
            set
            {
                this.effectiveDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool effectiveDateSpecified
        {
            get
            {
                return this.effectiveDateFieldSpecified;
            }
            set
            {
                this.effectiveDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime expirationDate
        {
            get
            {
                return this.expirationDateField;
            }
            set
            {
                this.expirationDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool expirationDateSpecified
        {
            get
            {
                return this.expirationDateFieldSpecified;
            }
            set
            {
                this.expirationDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public double currentPoints
        {
            get
            {
                return this.currentPointsField;
            }
            set
            {
                this.currentPointsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool currentPointsSpecified
        {
            get
            {
                return this.currentPointsFieldSpecified;
            }
            set
            {
                this.currentPointsFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string enrollmentCode
        {
            get
            {
                return this.enrollmentCodeField;
            }
            set
            {
                this.enrollmentCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public UniqueID[] ResvNameId
        {
            get
            {
                return this.resvNameIdField;
            }
            set
            {
                this.resvNameIdField = value;
            }
        }

        /// <remarks/>
        public UniqueID membershipid
        {
            get
            {
                return this.membershipidField;
            }
            set
            {
                this.membershipidField = value;
            }
        }

        /// <remarks/>
        public long transferPoints
        {
            get
            {
                return this.transferPointsField;
            }
            set
            {
                this.transferPointsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool transferPointsSpecified
        {
            get
            {
                return this.transferPointsFieldSpecified;
            }
            set
            {
                this.transferPointsFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string enrollmentSource
        {
            get
            {
                return this.enrollmentSourceField;
            }
            set
            {
                this.enrollmentSourceField = value;
            }
        }

        /// <remarks/>
        public string enrolledAt
        {
            get
            {
                return this.enrolledAtField;
            }
            set
            {
                this.enrolledAtField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public AwardPointsToExpire[] awardPointsToExpires
        {
            get
            {
                return this.awardPointsToExpiresField;
            }
            set
            {
                this.awardPointsToExpiresField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Common/")]
    public partial class AwardPointsToExpire
    {

        private System.DateTime expirationDateField;

        private double totalToExpireField;

        private double expireByDateField;

        private bool expireByDateFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime expirationDate
        {
            get
            {
                return this.expirationDateField;
            }
            set
            {
                this.expirationDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public double totalToExpire
        {
            get
            {
                return this.totalToExpireField;
            }
            set
            {
                this.totalToExpireField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public double expireByDate
        {
            get
            {
                return this.expireByDateField;
            }
            set
            {
                this.expireByDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool expireByDateSpecified
        {
            get
            {
                return this.expireByDateFieldSpecified;
            }
            set
            {
                this.expireByDateFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/")]
    public partial class NegotiatedRate
    {

        private long operaIdField;

        private bool operaIdFieldSpecified;

        private string resortCodeField;

        private string rateCodeField;

        private string commissionCodeField;

        private System.DateTime beginDateField;

        private bool beginDateFieldSpecified;

        private System.DateTime endDateField;

        private bool endDateFieldSpecified;

        private int displaySequenceField;

        private bool displaySequenceFieldSpecified;

        private string insertUserField;

        private System.DateTime insertDateField;

        private bool insertDateFieldSpecified;

        private string updateUserField;

        private System.DateTime updateDateField;

        private bool updateDateFieldSpecified;

        private System.DateTime inactiveDateField;

        private bool inactiveDateFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public long operaId
        {
            get
            {
                return this.operaIdField;
            }
            set
            {
                this.operaIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool operaIdSpecified
        {
            get
            {
                return this.operaIdFieldSpecified;
            }
            set
            {
                this.operaIdFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string resortCode
        {
            get
            {
                return this.resortCodeField;
            }
            set
            {
                this.resortCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string rateCode
        {
            get
            {
                return this.rateCodeField;
            }
            set
            {
                this.rateCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string commissionCode
        {
            get
            {
                return this.commissionCodeField;
            }
            set
            {
                this.commissionCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime beginDate
        {
            get
            {
                return this.beginDateField;
            }
            set
            {
                this.beginDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool beginDateSpecified
        {
            get
            {
                return this.beginDateFieldSpecified;
            }
            set
            {
                this.beginDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime endDate
        {
            get
            {
                return this.endDateField;
            }
            set
            {
                this.endDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool endDateSpecified
        {
            get
            {
                return this.endDateFieldSpecified;
            }
            set
            {
                this.endDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int displaySequence
        {
            get
            {
                return this.displaySequenceField;
            }
            set
            {
                this.displaySequenceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool displaySequenceSpecified
        {
            get
            {
                return this.displaySequenceFieldSpecified;
            }
            set
            {
                this.displaySequenceFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string insertUser
        {
            get
            {
                return this.insertUserField;
            }
            set
            {
                this.insertUserField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime insertDate
        {
            get
            {
                return this.insertDateField;
            }
            set
            {
                this.insertDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool insertDateSpecified
        {
            get
            {
                return this.insertDateFieldSpecified;
            }
            set
            {
                this.insertDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string updateUser
        {
            get
            {
                return this.updateUserField;
            }
            set
            {
                this.updateUserField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime updateDate
        {
            get
            {
                return this.updateDateField;
            }
            set
            {
                this.updateDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool updateDateSpecified
        {
            get
            {
                return this.updateDateFieldSpecified;
            }
            set
            {
                this.updateDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "date")]
        public System.DateTime inactiveDate
        {
            get
            {
                return this.inactiveDateField;
            }
            set
            {
                this.inactiveDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool inactiveDateSpecified
        {
            get
            {
                return this.inactiveDateFieldSpecified;
            }
            set
            {
                this.inactiveDateFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Common/")]
    public partial class UserDefinedValue
    {

        private object itemField;

        private string valueNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CharacterValue", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("DateValue", typeof(System.DateTime))]
        [System.Xml.Serialization.XmlElementAttribute("NumericValue", typeof(double))]
        public object Item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string valueName
        {
            get
            {
                return this.valueNameField;
            }
            set
            {
                this.valueNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/")]
    public partial class PrivacyOptionType
    {

        private PrivacyOptionTypeOptionType optionTypeField;

        private PrivacyOptionTypeOptionValue optionValueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public PrivacyOptionTypeOptionType OptionType
        {
            get
            {
                return this.optionTypeField;
            }
            set
            {
                this.optionTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public PrivacyOptionTypeOptionValue OptionValue
        {
            get
            {
                return this.optionValueField;
            }
            set
            {
                this.optionValueField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/")]
    public enum PrivacyOptionTypeOptionType
    {

        /// <remarks/>
        Promotions,

        /// <remarks/>
        MarketResearch,

        /// <remarks/>
        ThirdParties,

        /// <remarks/>
        LoyaltyProgram,

        /// <remarks/>
        Privacy,

        /// <remarks/>
        Email,

        /// <remarks/>
        Mail,

        /// <remarks/>
        Phone,

        /// <remarks/>
        SMS,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/")]
    public enum PrivacyOptionTypeOptionValue
    {

        /// <remarks/>
        YES,

        /// <remarks/>
        NO,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/")]
    public partial class ProfileUserGroup
    {

        private UserGroupType groupTypeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public UserGroupType groupType
        {
            get
            {
                return this.groupTypeField;
            }
            set
            {
                this.groupTypeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/")]
    public enum UserGroupType
    {

        /// <remarks/>
        TAM,

        /// <remarks/>
        BOOKER,

        /// <remarks/>
        BOOKER2,

        /// <remarks/>
        COMPANY,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/")]
    public partial class StayHistoryData
    {

        private System.DateTime lastStayField;

        private bool lastStayFieldSpecified;

        private uint totalNumberOfStaysField;

        private bool totalNumberOfStaysFieldSpecified;

        private string lastRoomNumberField;

        private Amount lastRateField;

        /// <remarks/>
        public System.DateTime LastStay
        {
            get
            {
                return this.lastStayField;
            }
            set
            {
                this.lastStayField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool LastStaySpecified
        {
            get
            {
                return this.lastStayFieldSpecified;
            }
            set
            {
                this.lastStayFieldSpecified = value;
            }
        }

        /// <remarks/>
        public uint TotalNumberOfStays
        {
            get
            {
                return this.totalNumberOfStaysField;
            }
            set
            {
                this.totalNumberOfStaysField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalNumberOfStaysSpecified
        {
            get
            {
                return this.totalNumberOfStaysFieldSpecified;
            }
            set
            {
                this.totalNumberOfStaysFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string LastRoomNumber
        {
            get
            {
                return this.lastRoomNumberField;
            }
            set
            {
                this.lastRoomNumberField = value;
            }
        }

        /// <remarks/>
        public Amount LastRate
        {
            get
            {
                return this.lastRateField;
            }
            set
            {
                this.lastRateField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class FetchProfileRequest
    {

        private UniqueID nameIDField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class TravelAgentLookupResponse
    {

        private ResultStatus resultField;

        private Profile profileDetailsField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        public Profile ProfileDetails
        {
            get
            {
                return this.profileDetailsField;
            }
            set
            {
                this.profileDetailsField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class TravelAgentLookupRequest
    {

        private UniqueID nameIDField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class InsertUpdateNameUDFsResponse
    {

        private ResultStatus resultField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class InsertUpdateNameUDFsRequest
    {

        private UniqueID nameIDField;

        private UserDefinedValue[] userDefinedValuesField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }

        /// <remarks/>
        public UserDefinedValue[] UserDefinedValues
        {
            get
            {
                return this.userDefinedValuesField;
            }
            set
            {
                this.userDefinedValuesField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class FetchNameUDFsResponse
    {

        private ResultStatus resultField;

        private UserDefinedValue[] userDefinedValuesField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(Namespace = "http://webservices.micros.com/og/4.3/Common/", IsNullable = false)]
        public UserDefinedValue[] UserDefinedValues
        {
            get
            {
                return this.userDefinedValuesField;
            }
            set
            {
                this.userDefinedValuesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class FetchNameUDFsRequest
    {

        private UniqueID nameIDField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class NameLookupResponse
    {

        private ResultStatus resultField;

        private Profile[] profilesField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/", IsNullable = false)]
        public Profile[] Profiles
        {
            get
            {
                return this.profilesField;
            }
            set
            {
                this.profilesField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/")]
    public partial class NameLookupCriteriaMembership
    {

        private string lastNameField;

        private string membershipTypeField;

        private string membershipNumberField;

        private NativeName nativeNameField;

        /// <remarks/>
        public string LastName
        {
            get
            {
                return this.lastNameField;
            }
            set
            {
                this.lastNameField = value;
            }
        }

        /// <remarks/>
        public string MembershipType
        {
            get
            {
                return this.membershipTypeField;
            }
            set
            {
                this.membershipTypeField = value;
            }
        }

        /// <remarks/>
        public string MembershipNumber
        {
            get
            {
                return this.membershipNumberField;
            }
            set
            {
                this.membershipNumberField = value;
            }
        }

        /// <remarks/>
        public NativeName NativeName
        {
            get
            {
                return this.nativeNameField;
            }
            set
            {
                this.nativeNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/")]
    public partial class NameLookupCriteriaCreditCard
    {

        private string lastNameField;

        private string cardCodeField;

        private string cardNumberField;

        /// <remarks/>
        public string LastName
        {
            get
            {
                return this.lastNameField;
            }
            set
            {
                this.lastNameField = value;
            }
        }

        /// <remarks/>
        public string CardCode
        {
            get
            {
                return this.cardCodeField;
            }
            set
            {
                this.cardCodeField = value;
            }
        }

        /// <remarks/>
        public string CardNumber
        {
            get
            {
                return this.cardNumberField;
            }
            set
            {
                this.cardNumberField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/")]
    public partial class NameLookupCriteriaEmailAddress
    {

        private string emailAddressField;

        /// <remarks/>
        public string EmailAddress
        {
            get
            {
                return this.emailAddressField;
            }
            set
            {
                this.emailAddressField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/")]
    public partial class NameLookupInput
    {

        private object itemField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CreditCard", typeof(NameLookupCriteriaCreditCard))]
        [System.Xml.Serialization.XmlElementAttribute("EmailAddress", typeof(NameLookupCriteriaEmailAddress))]
        [System.Xml.Serialization.XmlElementAttribute("Membership", typeof(NameLookupCriteriaMembership))]
        public object Item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class NameLookupRequest
    {

        private NameLookupInput nameLookupCriteriaField;

        /// <remarks/>
        public NameLookupInput NameLookupCriteria
        {
            get
            {
                return this.nameLookupCriteriaField;
            }
            set
            {
                this.nameLookupCriteriaField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class DeletePrivacyOptionResponse
    {

        private ResultStatus resultField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class DeletePrivacyOptionRequest
    {

        private UniqueID nameIDField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class InsertUpdatePrivacyOptionResponse
    {

        private ResultStatus resultField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class InsertUpdatePrivacyOptionRequest
    {

        private UniqueID nameIDField;

        private PrivacyOptionType[] privacyField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("PrivacyOption", Namespace = "http://webservices.micros.com/og/4.3/Name/", IsNullable = false)]
        public PrivacyOptionType[] Privacy
        {
            get
            {
                return this.privacyField;
            }
            set
            {
                this.privacyField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class FetchPrivacyOptionResponse
    {

        private ResultStatus resultField;

        private PrivacyOptionType[] privacyField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("PrivacyOption", Namespace = "http://webservices.micros.com/og/4.3/Name/", IsNullable = false)]
        public PrivacyOptionType[] Privacy
        {
            get
            {
                return this.privacyField;
            }
            set
            {
                this.privacyField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class FetchPrivacyOptionRequest
    {

        private UniqueID nameIDField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class DeleteCommentResponse
    {

        private ResultStatus resultField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class DeleteCommentRequest
    {

        private UniqueID nameIDField;

        private UniqueID commentIDField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }

        /// <remarks/>
        public UniqueID CommentID
        {
            get
            {
                return this.commentIDField;
            }
            set
            {
                this.commentIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class UpdateCommentResponse
    {

        private ResultStatus resultField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class UpdateCommentRequest
    {

        private UniqueID nameIDField;

        private Comment commentField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }

        /// <remarks/>
        public Comment Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class InsertCommentResponse
    {

        private ResultStatus resultField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class InsertCommentRequest
    {

        private UniqueID nameIDField;

        private Comment commentField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }

        /// <remarks/>
        public Comment Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class FetchCommentListResponse
    {

        private ResultStatus resultField;

        private Comment[] commentListField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/", IsNullable = false)]
        public Comment[] CommentList
        {
            get
            {
                return this.commentListField;
            }
            set
            {
                this.commentListField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class FetchCommentListRequest
    {

        private UniqueID nameIDField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class DeletePreferenceResponse
    {

        private ResultStatus resultField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class DeletePreferenceRequest
    {

        private UniqueID nameIDField;

        private Preference preferenceField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }

        /// <remarks/>
        public Preference Preference
        {
            get
            {
                return this.preferenceField;
            }
            set
            {
                this.preferenceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class InsertPreferenceResponse
    {

        private ResultStatus resultField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class InsertPreferenceRequest
    {

        private UniqueID nameIDField;

        private Preference preferenceField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }

        /// <remarks/>
        public Preference Preference
        {
            get
            {
                return this.preferenceField;
            }
            set
            {
                this.preferenceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class FetchPreferenceListResponse
    {

        private ResultStatus resultField;

        private Preference[] preferenceListField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/", IsNullable = false)]
        public Preference[] PreferenceList
        {
            get
            {
                return this.preferenceListField;
            }
            set
            {
                this.preferenceListField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class FetchPreferenceListRequest
    {

        private UniqueID nameIDField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class DeleteGuestCardResponse
    {

        private ResultStatus resultField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class DeleteGuestCardRequest
    {

        private UniqueID guestCardIDField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public UniqueID GuestCardID
        {
            get
            {
                return this.guestCardIDField;
            }
            set
            {
                this.guestCardIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class UpdateGuestCardResponse
    {

        private ResultStatus resultField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class UpdateGuestCardRequest
    {

        private NameMembership nameMembershipField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public NameMembership NameMembership
        {
            get
            {
                return this.nameMembershipField;
            }
            set
            {
                this.nameMembershipField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class InsertGuestCardResponse
    {

        private ResultStatus resultField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class InsertGuestCardRequest
    {

        private UniqueID nameIDField;

        private NameMembership nameMembershipField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }

        /// <remarks/>
        public NameMembership NameMembership
        {
            get
            {
                return this.nameMembershipField;
            }
            set
            {
                this.nameMembershipField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class FetchGuestCardListResponse
    {

        private ResultStatus resultField;

        private NameMembership[] guestCardListField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/", IsNullable = false)]
        public NameMembership[] GuestCardList
        {
            get
            {
                return this.guestCardListField;
            }
            set
            {
                this.guestCardListField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Common/")]
    public partial class KeyTrack
    {

        private string key1TrackField;

        private string key2TrackField;

        private string key3TrackField;

        private string key4TrackField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Key1Track
        {
            get
            {
                return this.key1TrackField;
            }
            set
            {
                this.key1TrackField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Key2Track
        {
            get
            {
                return this.key2TrackField;
            }
            set
            {
                this.key2TrackField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Key3Track
        {
            get
            {
                return this.key3TrackField;
            }
            set
            {
                this.key3TrackField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Key4Track
        {
            get
            {
                return this.key4TrackField;
            }
            set
            {
                this.key4TrackField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class FetchGuestCardListRequest
    {

        private UniqueID nameIDField;

        private bool returnInactiveRecordsField;

        private UniqueID resvNameIdField;

        private KeyTrack keyTrackField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }

        /// <remarks/>
        public bool ReturnInactiveRecords
        {
            get
            {
                return this.returnInactiveRecordsField;
            }
            set
            {
                this.returnInactiveRecordsField = value;
            }
        }

        /// <remarks/>
        public UniqueID ResvNameId
        {
            get
            {
                return this.resvNameIdField;
            }
            set
            {
                this.resvNameIdField = value;
            }
        }

        /// <remarks/>
        public KeyTrack KeyTrack
        {
            get
            {
                return this.keyTrackField;
            }
            set
            {
                this.keyTrackField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class DeletePassportResponse
    {

        private ResultStatus resultField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class DeletePassportRequest
    {

        private UniqueID nameIDField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class UpdatePassportResponse
    {

        private ResultStatus resultField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class UpdatePassportRequest
    {

        private UniqueID nameIDField;

        private GovernmentID passportField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }

        /// <remarks/>
        public GovernmentID Passport
        {
            get
            {
                return this.passportField;
            }
            set
            {
                this.passportField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class GetPassportResponse
    {

        private ResultStatus resultField;

        private GovernmentID passportField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        public GovernmentID Passport
        {
            get
            {
                return this.passportField;
            }
            set
            {
                this.passportField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class GetPassportRequest
    {

        private UniqueID nameIDField;

        private GovernmentID passportField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }

        /// <remarks/>
        public GovernmentID Passport
        {
            get
            {
                return this.passportField;
            }
            set
            {
                this.passportField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class DeleteCreditCardResponse
    {

        private ResultStatus resultField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class DeleteCreditCardRequest
    {

        private UniqueID creditCardIDField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public UniqueID CreditCardID
        {
            get
            {
                return this.creditCardIDField;
            }
            set
            {
                this.creditCardIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class UpdateCreditCardResponse
    {

        private ResultStatus resultField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class UpdateCreditCardRequest
    {

        private NameCreditCard nameCreditCardField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public NameCreditCard NameCreditCard
        {
            get
            {
                return this.nameCreditCardField;
            }
            set
            {
                this.nameCreditCardField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class InsertCreditCardResponse
    {

        private ResultStatus resultField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class InsertCreditCardRequest
    {

        private UniqueID nameIDField;

        private NameCreditCard nameCreditCardField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }

        /// <remarks/>
        public NameCreditCard NameCreditCard
        {
            get
            {
                return this.nameCreditCardField;
            }
            set
            {
                this.nameCreditCardField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class FetchCreditCardListResponse
    {

        private ResultStatus resultField;

        private NameCreditCard[] creditCardListField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/", IsNullable = false)]
        public NameCreditCard[] CreditCardList
        {
            get
            {
                return this.creditCardListField;
            }
            set
            {
                this.creditCardListField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class FetchCreditCardListRequest
    {

        private UniqueID nameIDField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class DeleteEmailResponse
    {

        private ResultStatus resultField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class DeleteEmailRequest
    {

        private UniqueID emailIDField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public UniqueID EmailID
        {
            get
            {
                return this.emailIDField;
            }
            set
            {
                this.emailIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class UpdateEmailResponse
    {

        private ResultStatus resultField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class UpdateEmailRequest
    {

        private NameEmail nameEmailField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public NameEmail NameEmail
        {
            get
            {
                return this.nameEmailField;
            }
            set
            {
                this.nameEmailField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class InsertEmailResponse
    {

        private ResultStatus resultField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class InsertEmailRequest
    {

        private UniqueID nameIDField;

        private NameEmail nameEmailField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }

        /// <remarks/>
        public NameEmail NameEmail
        {
            get
            {
                return this.nameEmailField;
            }
            set
            {
                this.nameEmailField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class FetchEmailListResponse
    {

        private ResultStatus resultField;

        private NameEmail[] nameEmailListField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/", IsNullable = false)]
        public NameEmail[] NameEmailList
        {
            get
            {
                return this.nameEmailListField;
            }
            set
            {
                this.nameEmailListField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class FetchEmailListRequest
    {

        private UniqueID nameIDField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class DeletePhoneResponse
    {

        private ResultStatus resultField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class DeletePhoneRequest
    {

        private UniqueID phoneIDField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public UniqueID PhoneID
        {
            get
            {
                return this.phoneIDField;
            }
            set
            {
                this.phoneIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class UpdatePhoneResponse
    {

        private ResultStatus resultField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class UpdatePhoneRequest
    {

        private NamePhone namePhoneField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public NamePhone NamePhone
        {
            get
            {
                return this.namePhoneField;
            }
            set
            {
                this.namePhoneField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class InsertPhoneResponse
    {

        private ResultStatus resultField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class InsertPhoneRequest
    {

        private UniqueID nameIDField;

        private NamePhone namePhoneField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }

        /// <remarks/>
        public NamePhone NamePhone
        {
            get
            {
                return this.namePhoneField;
            }
            set
            {
                this.namePhoneField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class FetchPhoneListResponse
    {

        private ResultStatus resultField;

        private NamePhone[] namePhoneListField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/", IsNullable = false)]
        public NamePhone[] NamePhoneList
        {
            get
            {
                return this.namePhoneListField;
            }
            set
            {
                this.namePhoneListField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class FetchPhoneListRequest
    {

        private UniqueID nameIDField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class DeleteAddressResponse
    {

        private ResultStatus resultField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class DeleteAddressRequest
    {

        private UniqueID addressIDField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public UniqueID AddressID
        {
            get
            {
                return this.addressIDField;
            }
            set
            {
                this.addressIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class UpdateAddressResponse
    {

        private ResultStatus resultField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class UpdateAddressRequest
    {

        private NameAddress nameAddressField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public NameAddress NameAddress
        {
            get
            {
                return this.nameAddressField;
            }
            set
            {
                this.nameAddressField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class InsertAddressResponse
    {

        private ResultStatus resultField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class InsertAddressRequest
    {

        private UniqueID nameIDField;

        private NameAddress nameAddressField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }

        /// <remarks/>
        public NameAddress NameAddress
        {
            get
            {
                return this.nameAddressField;
            }
            set
            {
                this.nameAddressField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class FetchAddressListResponse
    {

        private ResultStatus resultField;

        private NameAddress[] nameAddressListField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(Namespace = "http://webservices.micros.com/og/4.3/Name/", IsNullable = false)]
        public NameAddress[] NameAddressList
        {
            get
            {
                return this.nameAddressListField;
            }
            set
            {
                this.nameAddressListField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class FetchAddressListRequest
    {

        private UniqueID nameIDField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class UpdateNameResponse
    {

        private ResultStatus resultField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class UpdateNameRequest
    {

        private UniqueID nameIDField;

        private PersonName personNameField;

        private NativeName nativeNameField;

        private System.DateTime birthdateField;

        private bool birthdateFieldSpecified;

        private Gender genderField;

        private bool genderFieldSpecified;

        private string nationalityField;

        private string vipCodeField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }

        /// <remarks/>
        public PersonName PersonName
        {
            get
            {
                return this.personNameField;
            }
            set
            {
                this.personNameField = value;
            }
        }

        /// <remarks/>
        public NativeName NativeName
        {
            get
            {
                return this.nativeNameField;
            }
            set
            {
                this.nativeNameField = value;
            }
        }

        /// <remarks/>
        public System.DateTime Birthdate
        {
            get
            {
                return this.birthdateField;
            }
            set
            {
                this.birthdateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool BirthdateSpecified
        {
            get
            {
                return this.birthdateFieldSpecified;
            }
            set
            {
                this.birthdateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public Gender Gender
        {
            get
            {
                return this.genderField;
            }
            set
            {
                this.genderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool GenderSpecified
        {
            get
            {
                return this.genderFieldSpecified;
            }
            set
            {
                this.genderFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string nationality
        {
            get
            {
                return this.nationalityField;
            }
            set
            {
                this.nationalityField = value;
            }
        }

        /// <remarks/>
        public string vipCode
        {
            get
            {
                return this.vipCodeField;
            }
            set
            {
                this.vipCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class FetchNameResponse
    {

        private ResultStatus resultField;

        private PersonName personNameField;

        private NativeName nativeNameField;

        private System.DateTime birthdateField;

        private bool birthdateFieldSpecified;

        private Gender genderField;

        private bool genderFieldSpecified;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        public PersonName PersonName
        {
            get
            {
                return this.personNameField;
            }
            set
            {
                this.personNameField = value;
            }
        }

        /// <remarks/>
        public NativeName NativeName
        {
            get
            {
                return this.nativeNameField;
            }
            set
            {
                this.nativeNameField = value;
            }
        }

        /// <remarks/>
        public System.DateTime Birthdate
        {
            get
            {
                return this.birthdateField;
            }
            set
            {
                this.birthdateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool BirthdateSpecified
        {
            get
            {
                return this.birthdateFieldSpecified;
            }
            set
            {
                this.birthdateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public Gender Gender
        {
            get
            {
                return this.genderField;
            }
            set
            {
                this.genderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool GenderSpecified
        {
            get
            {
                return this.genderFieldSpecified;
            }
            set
            {
                this.genderFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class FetchNameRequest
    {

        private UniqueID nameIDField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public UniqueID NameID
        {
            get
            {
                return this.nameIDField;
            }
            set
            {
                this.nameIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class RegisterNameResponse
    {

        private ResultStatus resultField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class RegisterNameRequest
    {

        private PersonName personNameField;

        private NativeName nativeNameField;

        private System.DateTime birthdateField;

        private bool birthdateFieldSpecified;

        private Gender genderField;

        private bool genderFieldSpecified;

        private Address addressField;

        private Phone phoneField;

        private GovernmentID passportField;

        private string nationalityField;

        private string vipCodeField;

        private string loginNameField;

        private string passwordField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public PersonName PersonName
        {
            get
            {
                return this.personNameField;
            }
            set
            {
                this.personNameField = value;
            }
        }

        /// <remarks/>
        public NativeName NativeName
        {
            get
            {
                return this.nativeNameField;
            }
            set
            {
                this.nativeNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime Birthdate
        {
            get
            {
                return this.birthdateField;
            }
            set
            {
                this.birthdateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool BirthdateSpecified
        {
            get
            {
                return this.birthdateFieldSpecified;
            }
            set
            {
                this.birthdateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public Gender Gender
        {
            get
            {
                return this.genderField;
            }
            set
            {
                this.genderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool GenderSpecified
        {
            get
            {
                return this.genderFieldSpecified;
            }
            set
            {
                this.genderFieldSpecified = value;
            }
        }

        /// <remarks/>
        public Address Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        public Phone Phone
        {
            get
            {
                return this.phoneField;
            }
            set
            {
                this.phoneField = value;
            }
        }

        /// <remarks/>
        public GovernmentID Passport
        {
            get
            {
                return this.passportField;
            }
            set
            {
                this.passportField = value;
            }
        }

        /// <remarks/>
        public string nationality
        {
            get
            {
                return this.nationalityField;
            }
            set
            {
                this.nationalityField = value;
            }
        }

        /// <remarks/>
        public string vipCode
        {
            get
            {
                return this.vipCodeField;
            }
            set
            {
                this.vipCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string loginName
        {
            get
            {
                return this.loginNameField;
            }
            set
            {
                this.loginNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Core/")]
    public partial class OGHeaderAuthenticationLicence
    {

        private string keyField;

        /// <remarks/>
        public string Key
        {
            get
            {
                return this.keyField;
            }
            set
            {
                this.keyField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Core/")]
    public partial class OGHeaderAuthenticationUserCredentials
    {

        private string userNameField;

        private string userPasswordField;

        private string domainField;

        private string securityIdField;

        /// <remarks/>
        public string UserName
        {
            get
            {
                return this.userNameField;
            }
            set
            {
                this.userNameField = value;
            }
        }

        /// <remarks/>
        public string UserPassword
        {
            get
            {
                return this.userPasswordField;
            }
            set
            {
                this.userPasswordField = value;
            }
        }

        /// <remarks/>
        public string Domain
        {
            get
            {
                return this.domainField;
            }
            set
            {
                this.domainField = value;
            }
        }

        /// <remarks/>
        public string SecurityId
        {
            get
            {
                return this.securityIdField;
            }
            set
            {
                this.securityIdField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://webservices.micros.com/og/4.3/Core/")]
    public partial class OGHeaderAuthentication
    {

        private OGHeaderAuthenticationUserCredentials userCredentialsField;

        private OGHeaderAuthenticationLicence licenceField;

        /// <remarks/>
        public OGHeaderAuthenticationUserCredentials UserCredentials
        {
            get
            {
                return this.userCredentialsField;
            }
            set
            {
                this.userCredentialsField = value;
            }
        }

        /// <remarks/>
        public OGHeaderAuthenticationLicence Licence
        {
            get
            {
                return this.licenceField;
            }
            set
            {
                this.licenceField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class FetchSubscriptionResponse
    {

        private ResultStatus resultField;

        private UniqueID[] nameIDsField;

        private System.Xml.Serialization.XmlSerializerNamespaces xsnField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(Namespace = "http://webservices.micros.com/og/4.3/Common/", IsNullable = false)]
        public UniqueID[] NameIDs
        {
            get
            {
                return this.nameIDsField;
            }
            set
            {
                this.nameIDsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlNamespaceDeclarationsAttribute()]
        public System.Xml.Serialization.XmlSerializerNamespaces xsn
        {
            get
            {
                return this.xsnField;
            }
            set
            {
                this.xsnField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://webservices.micros.com/ows/5.1/Name.wsdl")]
    public partial class FetchProfileBenefitsResponse
    {

        private ResultStatus resultField;

        private ProfilePromotion[] promotionListField;

        private ECertificate[] eCertificateListField;

        /// <remarks/>
        public ResultStatus Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(Namespace = "http://webservices.micros.com/og/4.3/Membership/", IsNullable = false)]
        public ProfilePromotion[] PromotionList
        {
            get
            {
                return this.promotionListField;
            }
            set
            {
                this.promotionListField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("ECertificateInfo", Namespace = "http://webservices.micros.com/og/4.3/Membership/", IsNullable = false)]
        public ECertificate[] ECertificateList
        {
            get
            {
                return this.eCertificateListField;
            }
            set
            {
                this.eCertificateListField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void RegisterNameCompletedEventHandler(object sender, RegisterNameCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class RegisterNameCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal RegisterNameCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public RegisterNameResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((RegisterNameResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void FetchNameCompletedEventHandler(object sender, FetchNameCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class FetchNameCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal FetchNameCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public FetchNameResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((FetchNameResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void UpdateNameCompletedEventHandler(object sender, UpdateNameCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class UpdateNameCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal UpdateNameCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public UpdateNameResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((UpdateNameResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void FetchAddressListCompletedEventHandler(object sender, FetchAddressListCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class FetchAddressListCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal FetchAddressListCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public FetchAddressListResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((FetchAddressListResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void InsertAddressCompletedEventHandler(object sender, InsertAddressCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class InsertAddressCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal InsertAddressCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public InsertAddressResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((InsertAddressResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void UpdateAddressCompletedEventHandler(object sender, UpdateAddressCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class UpdateAddressCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal UpdateAddressCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public UpdateAddressResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((UpdateAddressResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void DeleteAddressCompletedEventHandler(object sender, DeleteAddressCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class DeleteAddressCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal DeleteAddressCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public DeleteAddressResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((DeleteAddressResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void FetchPhoneListCompletedEventHandler(object sender, FetchPhoneListCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class FetchPhoneListCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal FetchPhoneListCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public FetchPhoneListResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((FetchPhoneListResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void InsertPhoneCompletedEventHandler(object sender, InsertPhoneCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class InsertPhoneCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal InsertPhoneCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public InsertPhoneResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((InsertPhoneResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void UpdatePhoneCompletedEventHandler(object sender, UpdatePhoneCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class UpdatePhoneCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal UpdatePhoneCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public UpdatePhoneResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((UpdatePhoneResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void DeletePhoneCompletedEventHandler(object sender, DeletePhoneCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class DeletePhoneCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal DeletePhoneCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public DeletePhoneResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((DeletePhoneResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void FetchEmailListCompletedEventHandler(object sender, FetchEmailListCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class FetchEmailListCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal FetchEmailListCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public FetchEmailListResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((FetchEmailListResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void InsertEmailCompletedEventHandler(object sender, InsertEmailCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class InsertEmailCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal InsertEmailCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public InsertEmailResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((InsertEmailResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void UpdateEmailCompletedEventHandler(object sender, UpdateEmailCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class UpdateEmailCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal UpdateEmailCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public UpdateEmailResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((UpdateEmailResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void DeleteEmailCompletedEventHandler(object sender, DeleteEmailCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class DeleteEmailCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal DeleteEmailCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public DeleteEmailResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((DeleteEmailResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void FetchCreditCardListCompletedEventHandler(object sender, FetchCreditCardListCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class FetchCreditCardListCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal FetchCreditCardListCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public FetchCreditCardListResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((FetchCreditCardListResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void InsertCreditCardCompletedEventHandler(object sender, InsertCreditCardCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class InsertCreditCardCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal InsertCreditCardCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public InsertCreditCardResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((InsertCreditCardResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void UpdateCreditCardCompletedEventHandler(object sender, UpdateCreditCardCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class UpdateCreditCardCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal UpdateCreditCardCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public UpdateCreditCardResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((UpdateCreditCardResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void DeleteCreditCardCompletedEventHandler(object sender, DeleteCreditCardCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class DeleteCreditCardCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal DeleteCreditCardCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public DeleteCreditCardResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((DeleteCreditCardResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void GetPassportCompletedEventHandler(object sender, GetPassportCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetPassportCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal GetPassportCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public GetPassportResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((GetPassportResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void UpdatePassportCompletedEventHandler(object sender, UpdatePassportCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class UpdatePassportCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal UpdatePassportCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public UpdatePassportResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((UpdatePassportResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void DeletePassportCompletedEventHandler(object sender, DeletePassportCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class DeletePassportCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal DeletePassportCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public DeletePassportResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((DeletePassportResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void FetchGuestCardListCompletedEventHandler(object sender, FetchGuestCardListCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class FetchGuestCardListCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal FetchGuestCardListCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public FetchGuestCardListResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((FetchGuestCardListResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void InsertGuestCardCompletedEventHandler(object sender, InsertGuestCardCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class InsertGuestCardCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal InsertGuestCardCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public InsertGuestCardResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((InsertGuestCardResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void UpdateGuestCardCompletedEventHandler(object sender, UpdateGuestCardCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class UpdateGuestCardCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal UpdateGuestCardCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public UpdateGuestCardResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((UpdateGuestCardResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void DeleteGuestCardCompletedEventHandler(object sender, DeleteGuestCardCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class DeleteGuestCardCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal DeleteGuestCardCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public DeleteGuestCardResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((DeleteGuestCardResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void FetchPreferenceListCompletedEventHandler(object sender, FetchPreferenceListCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class FetchPreferenceListCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal FetchPreferenceListCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public FetchPreferenceListResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((FetchPreferenceListResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void InsertPreferenceCompletedEventHandler(object sender, InsertPreferenceCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class InsertPreferenceCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal InsertPreferenceCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public InsertPreferenceResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((InsertPreferenceResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void DeletePreferenceCompletedEventHandler(object sender, DeletePreferenceCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class DeletePreferenceCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal DeletePreferenceCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public DeletePreferenceResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((DeletePreferenceResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void FetchCommentListCompletedEventHandler(object sender, FetchCommentListCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class FetchCommentListCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal FetchCommentListCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public FetchCommentListResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((FetchCommentListResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void InsertCommentCompletedEventHandler(object sender, InsertCommentCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class InsertCommentCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal InsertCommentCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public InsertCommentResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((InsertCommentResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void UpdateCommentCompletedEventHandler(object sender, UpdateCommentCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class UpdateCommentCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal UpdateCommentCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public UpdateCommentResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((UpdateCommentResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void DeleteCommentCompletedEventHandler(object sender, DeleteCommentCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class DeleteCommentCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal DeleteCommentCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public DeleteCommentResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((DeleteCommentResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void FetchPrivacyOptionCompletedEventHandler(object sender, FetchPrivacyOptionCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class FetchPrivacyOptionCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal FetchPrivacyOptionCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public FetchPrivacyOptionResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((FetchPrivacyOptionResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void InsertUpdatePrivacyOptionCompletedEventHandler(object sender, InsertUpdatePrivacyOptionCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class InsertUpdatePrivacyOptionCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal InsertUpdatePrivacyOptionCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public InsertUpdatePrivacyOptionResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((InsertUpdatePrivacyOptionResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void DeletePrivacyOptionCompletedEventHandler(object sender, DeletePrivacyOptionCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class DeletePrivacyOptionCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal DeletePrivacyOptionCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public DeletePrivacyOptionResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((DeletePrivacyOptionResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void NameLookupCompletedEventHandler(object sender, NameLookupCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class NameLookupCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal NameLookupCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public NameLookupResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((NameLookupResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void FetchNameUDFsCompletedEventHandler(object sender, FetchNameUDFsCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class FetchNameUDFsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal FetchNameUDFsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public FetchNameUDFsResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((FetchNameUDFsResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void InsertUpdateNameUDFsCompletedEventHandler(object sender, InsertUpdateNameUDFsCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class InsertUpdateNameUDFsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal InsertUpdateNameUDFsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public InsertUpdateNameUDFsResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((InsertUpdateNameUDFsResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void TravelAgentLookupCompletedEventHandler(object sender, TravelAgentLookupCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class TravelAgentLookupCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal TravelAgentLookupCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public TravelAgentLookupResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((TravelAgentLookupResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void FetchProfileCompletedEventHandler(object sender, FetchProfileCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class FetchProfileCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal FetchProfileCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public FetchProfileResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((FetchProfileResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void InsertClaimCompletedEventHandler(object sender, InsertClaimCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class InsertClaimCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal InsertClaimCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public InsertClaimResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((InsertClaimResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void FetchClaimsStatusCompletedEventHandler(object sender, FetchClaimsStatusCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class FetchClaimsStatusCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal FetchClaimsStatusCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public FetchClaimsStatusResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((FetchClaimsStatusResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void UpdateClaimCompletedEventHandler(object sender, UpdateClaimCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class UpdateClaimCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal UpdateClaimCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public UpdateClaimResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((UpdateClaimResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void FetchSubscriptionCompletedEventHandler(object sender, FetchSubscriptionCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class FetchSubscriptionCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal FetchSubscriptionCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public FetchSubscriptionResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((FetchSubscriptionResponse)(this.results[0]));
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    public delegate void FetchProfileBenefitsCompletedEventHandler(object sender, FetchProfileBenefitsCompletedEventArgs e);

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("wsdl", "2.0.50727.42")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class FetchProfileBenefitsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal FetchProfileBenefitsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState)
            :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public FetchProfileBenefitsResponse Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((FetchProfileBenefitsResponse)(this.results[0]));
            }
        }
    }
}
