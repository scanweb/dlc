(function($, window){
	console.log('welcome');


var reset = function(){
	$("header").attr("style","");
	$("section.main").attr("style","");
	$("section.main").attr("style","");
	$("section.bottom").attr("style","");
	$("article.card").attr("style","");
	$("header h2.logo").attr("style","");
	$("header h2.subTitle").attr("style","");
	$("header div.line").attr("style","");
	$(".membership-number").attr("style","");
	$(".membership-details .info").attr("style","");
	$(".membership-details").attr("style","");
	$(".bottom div.line").attr("style","");
	$(".bottom p").attr("style","");
};

var renderPortrait = function(){
	reset();
/*
Portrait:
64
446
60
= 
568

=
11%
78%
11%
*/
	// First calculate the numbers
	var viewportWidth = $(window).width(),
		viewportHeight = $(window).height(),
		scale = viewportHeight / 568.0,
		doScale = function(input){ return Math.round(scale * input);},
		topHeight = Math.round(scale*64.0),
		bottomHeight = Math.round(scale*60.0),
		contentHeight = viewportHeight - topHeight - bottomHeight,
	$header = $("header"),
	$content = $("section.main"),
	$bottom = $("section.bottom");
	
	$personalGreeting = $("header h3.personal-greeting");
	
	$bottom.remove();
	$header.remove();
	$personalGreeting.after($content);
	$content.before($header);
	$content.after($bottom);

	console.log(topHeight, contentHeight, bottomHeight);

	$header.css({height: topHeight + "px"});
	$content.css({height:  contentHeight + "px"});
	$bottom.css({height: bottomHeight + "px"});

	// Now calculate the card size

	// Portrait size = 748x1157
	var cardRatio = 748.0 / 1157.0,
		windowRatio = viewportWidth / contentHeight;
	var cardHeight, cardWidth;

	if(cardRatio <= windowRatio){
		cardHeight = contentHeight;
		cardWidth = cardHeight * cardRatio;
	}else{
		cardWidth = Math.round(0.9*viewportWidth);
		cardHeight = cardWidth / cardRatio;
	}

	$card = $("article.card");
	$card.css({width:cardWidth + "px", height: cardHeight + "px"});

	// Now the title
	// logo 374x80
	// 29/568 = 5%
	var logoHeight = Math.round(scale * 29.0),
		logoWidth = Math.round(logoHeight * 374.0 / 80.0),
		logoLeftMargin = Math.round((viewportWidth - cardWidth + 8)*0.5),
		logoTopMargin = Math.round(scale * 15.0),
		$logo = $("header h2.logo");

	$logo.css({ width: logoWidth + "px", height:logoHeight + "px", marginLeft: logoLeftMargin + "px", marginTop:logoTopMargin + "px"});

	// Now subTitle ("Medlemskort")
	var subTitleFontSize = Math.round(scale * 24.0),
		subTitleLineHeight = Math.round(scale * 19.0),
		$subTitle = $("header h2.subTitle");

	$subTitle.css({fontSize: subTitleFontSize + "px", lineHeight: subTitleLineHeight + "px"});
	var subTitleHeight = $subTitle.height(),
		subTitleTopMargin = logoTopMargin + logoHeight - subTitleHeight + Math.round(viewportHeight*2.0/568.0);

	$subTitle.css({marginTop: subTitleTopMargin + "px", marginRight: logoLeftMargin + "px"});

	// The header line

	var $line = $("header div.line"),
		lineMarginTop = Math.round(scale * 5.0),
		lineWidth = Math.round(viewportWidth - 2*logoLeftMargin);

		$subTitle.after($line);

	$line.css({marginTop: lineMarginTop + "px", marginLeft: logoLeftMargin + "px", width:lineWidth + "px"})

	// Now the membership details:
	var membershipDetailsFontSize1 = Math.round(scale * 21.0),
		membershipDetailsFontSize2 = Math.round(scale * 11.0),
		membershipDetailsLineHeight1 = Math.round(scale * 18.0),
		membershipDetailsLineHeight2 = Math.round(scale * 16.0),
		membershipDetailsWidth = Math.round(scale * 210.0),
		membershipDetailsTop = Math.round(scale * 195.0),
		membershipDetailsLeft = Math.round(scale * 205.0) - membershipDetailsWidth,
		$membershipNumber = $(".membership-number"),
		$membershipInfo = $(".membership-details .info"),
	$membershipDetails = $(".membership-details");
	$membershipDetails.css({width:membershipDetailsWidth + "px"})		

	$membershipNumber.css({fontSize:membershipDetailsFontSize1 + "px", lineHeight: membershipDetailsLineHeight1+"px"});
	$membershipInfo.css({fontSize:membershipDetailsFontSize2 + "px", lineHeight: membershipDetailsLineHeight2+"px"});
	var membershipDetailsHeight = $membershipDetails.height();
	console.log(membershipDetailsTop);
	$membershipDetails.css({left:membershipDetailsLeft + "px", top: membershipDetailsTop + "px"});

		// Finally, the bottom
	$line = $(".bottom div.line");
	lineMarginTop = doScale(12);
	$line.css({marginTop: lineMarginTop + "px", marginLeft: logoLeftMargin + "px", width:lineWidth + "px"})

	var footerLineHeight = doScale(11),
		footerFontSize = doScale(9),
		footerMarginTop = doScale(7),
		$footerText = $(".bottom p");

	$footerText.css({fontSize:footerFontSize + "px", lineHeight: footerLineHeight + "px", marginTop: footerMarginTop + "px",
		marginLeft: logoLeftMargin + "px", marginRight: logoLeftMargin + "px"
	});
};



var renderLandscape = function(){
	reset();
/*
25
454
65

748/292
*/
	// First calculate the numbers
	var viewportWidth = $(window).width(),
		viewportHeight = $(window).height(),
		scale = viewportHeight / 320.0,
		doScale = function(input){ return Math.round(scale * input);},
		topWidth = doScale(64),
		bottomWidth = doScale(25),
		contentWidth = viewportWidth - topWidth - bottomWidth,
	$header = $("header"),
	$content = $("section.main"),
	$bottom = $("section.bottom");

	$header.css({width: topWidth + "px"});
	$content.css({width:  contentWidth + "px"});
	$bottom.css({width: bottomWidth + "px"});

	$bottom.remove();
	$header.remove();
	$content.before($bottom);
	$content.after($header);

	// Now calculate the card size

	// Portrait size = 748x1157
	var cardRatio = 1157.0 / 748.0,
		windowRatio = contentWidth / viewportHeight;
	var cardHeight, cardWidth;

	if(cardRatio <= windowRatio){
		cardWidth = contentWidth;
		cardHeight = Math.round(cardHeight / cardRatio);
	}else{
		cardWidth = Math.round(0.9*contentWidth);
		cardHeight = cardWidth / cardRatio;
	}
	var cardMarginTop = Math.round((viewportHeight - cardHeight)*0.5);
	var cardMarginLeft = Math.round((contentWidth - cardWidth)*0.5);

	$card = $("article.card");
	$card.css({width:cardWidth + "px", height: cardHeight + "px", 
		marginTop:cardMarginTop + "px", marginLeft: cardMarginLeft + "px"});


	var contentSpaceRight = (contentWidth - cardWidth)*0.5;

	// Now the title
	// logo 374x80
	// 29/568 = 5%
	var logoHeight = Math.round(scale * 25.0),
		logoWidth = Math.round(logoHeight * 374.0 / 80.0),
		logoTop = Math.round((viewportHeight - cardHeight + 8)*0.5),		
		$logo = $("header h2.logo"),
		lineLeft;
		if(contentSpaceRight > 0){
			if(doScale(20) > contentSpaceRight){
				lineLeft = doScale(20) - contentSpaceRight;
			}else{
				lineLeft = 0;
			}
		}else{
			lineLeft = doScale(20);
		}
		 
		var logoLeft = Math.round(scale * 6.0) + lineLeft + 1 + logoHeight,
			$line = $("header div.line"),
			lineTop = logoTop,		
			lineHeight = Math.round(viewportHeight - 2*logoTop);
			
			console.log(logoLeft, lineLeft);
		$logo.before($line);
		$line.css({top: lineTop + "px", left: lineLeft + "px", height:lineHeight + "px"});
		$logo.css({ width: logoWidth + "px", height:logoHeight + "px", top:logoTop, left:logoLeft});



	// Now subTitle ("Medlemskort")
	var subTitleFontSize = Math.round(scale * 16.0),
		subTitleLineHeight = Math.round(scale * 12.0),		
		subTitleLeft = logoLeft - doScale(2) - subTitleLineHeight;
		$subTitle = $("header h2.subTitle");

	$subTitle.css({fontSize: subTitleFontSize + "px", lineHeight: subTitleLineHeight + "px", left:subTitleLeft});
	
	var subTitleTop = $subTitle.width();
	$subTitle.css({top: subTitleTop + "px"});

	// The header line


	// Now the membership details:
	var membershipDetailsFontSize1 = Math.round(scale * 21.0),
		membershipDetailsFontSize2 = Math.round(scale * 11.0),
		membershipDetailsLineHeight1 = Math.round(scale * 18.0),
		membershipDetailsLineHeight2 = Math.round(scale * 16.0);
		
		$membershipNumber = $(".membership-number"),
		$membershipInfo = $(".membership-details .info"),
	$membershipDetails = $(".membership-details");

	$membershipNumber.css({fontSize:membershipDetailsFontSize1 + "px", lineHeight: membershipDetailsLineHeight1+"px"});
	$membershipInfo.css({fontSize:membershipDetailsFontSize2 + "px", lineHeight: membershipDetailsLineHeight2+"px"});

	var membershipDetailsTop = Math.round(scale * 196.0) - $membershipDetails.height(),
		membershipDetailsLeft = Math.round(scale * 44.0);

	$membershipDetails.css({left:membershipDetailsLeft + "px", top: membershipDetailsTop + "px"});
};

var doOnOrientationChange = function()
  {

    switch(window.orientation) 
    {  
      case -90:
      case 90:
		renderLandscape();
        break; 
      default:
        renderPortrait();
        break; 
    }
  };

  var windowResize = function(){
  	var viewportWidth = $(window).width(),
		viewportHeight = $(window).height();
	if(viewportWidth > viewportHeight){
		renderLandscape();
	}else{
		renderPortrait();
	}
	
  }
console.log($(window).width(), "width")
if($(window).width() < 768) {
  window.addEventListener('orientationchange', doOnOrientationChange);
  window.addEventListener('resize', windowResize);
  windowResize();	
}








})(jQuery, window);